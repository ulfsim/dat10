double calcDistance(double* instance, double* centroid)
{
    double result = 0.0;
    for(int i = 0; i < CENTROIDLENGTH; i++)
    {
        double cFeature = centroid[i];
        double iFeature = instance[i];
        double toPow = iFeature - cFeature;
        double tempResult = pow(toPow,2);
        result += sqrt(tempResult);
    }
    return result;
}

kernel void kmeans(global const double* dataMem, global const double* centroidMem, global double* answerMem, const int instances) {
    unsigned int xid = get_global_id(0);
    int datLength = CENTROIDLENGTH;
    int range = instances;
    double kernelCentroid[CENTROIDLENGTH];
    int centroidStart = xid*CENTROIDLENGTH;
    double currentInstance[CENTROIDLENGTH];
    for(int i = 0; i < CENTROIDLENGTH; i++)
    {
        kernelCentroid[i] = centroidMem[centroidStart+i];
    }

    for(int i = 0; i < range; i++)
    {
        for(int j = 0; j < CENTROIDLENGTH; j++)
        {
            currentInstance[j] = dataMem[(xid*range)+(i*datLength)+j];
        }
        int updateLocation = xid*range + i;
        double temp = calcDistance(currentInstance,kernelCentroid);
        answerMem[updateLocation] = temp;
    }
}

kernel void update(global const double* centroidMem, global double* answerMem, const int instances)
{
    unsigned int xid = get_global_id(0);
    int range = instances;

    double centroids[CENTROIDLENGTH];

}
