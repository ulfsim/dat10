package Ulf.opencl;

/**
 * Created by Ulf on 27/12/16.
 */
public class MemoryObject {
    long iterations;
    long kernelAmount;
    long instancesPrKernel;

    public long getIterations() {
        return iterations;
    }

    public void setIterations(int iterations) {
        this.iterations = iterations;
    }

    public long getInstancesPrKernel() {
        return instancesPrKernel;
    }

    public void setInstancesPrKernel(int instancesPrKernel) {
        this.instancesPrKernel = instancesPrKernel;
    }

    public long getKernelAmount() {
        return kernelAmount;
    }

    public void setKernelAmount(long kernelAmount) {
        this.kernelAmount = kernelAmount;
    }

    public MemoryObject() {
    }

    public MemoryObject(int iterations, int kernelAmount,int instancesPrKernel) {
        this.iterations = iterations;
        this.kernelAmount = kernelAmount;
        this.instancesPrKernel = instancesPrKernel;
    }

    public MemoryObject(long iterations, long kernelAmount, long instancesPrKernel) {
        this.iterations = iterations;
        this.kernelAmount = kernelAmount;
        this.instancesPrKernel = instancesPrKernel;
    }
}
