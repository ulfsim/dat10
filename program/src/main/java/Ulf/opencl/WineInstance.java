package Ulf.opencl;

/**
 * Created by sygob on 12-01-2017.
 */
public class WineInstance {
    double[] features;
    int cluster;

    public WineInstance(double[] features)
    {
        this.features = features;
        this.cluster = 0;
    }

    public int getCluster() {
        return cluster;
    }

    public void setCluster(int cluster) {
        this.cluster = cluster;
    }
}
