package Ulf.opencl;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;

/**
 * Created by sygob on 08-01-2017.
 */
public class FileSplitter {
    public static void main(String[] args) throws Exception {
        FileReader FR = new FileReader("FixedData.csv");
        BufferedReader BR = new BufferedReader(FR);
        int currentFile = 1;
        int count = 0;
        String line = null;

        while(currentFile <= 8) {
            FileWriter FW = new FileWriter("FixedData" + currentFile +".csv");
            BufferedWriter BW = new BufferedWriter(FW);
            while ((line = BR.readLine()) != null) {
                BW.write(line);
                BW.newLine();
                count++;
                if((count % 1000000 == 0) && count != 0)
                {
                    currentFile++;
                    BW.close();
                    FW.close();
                    break;
                }
            }
        }
    }
}
