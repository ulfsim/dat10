package Ulf.opencl;

/**
 * Created by sygob on 09-03-2016.
 */
public class HelloNonOpenCL {
    static final float[] a = new float[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10,11,12,13};
    static final float[] b = new float[]{9, 8, 7, 6, 5, 4, 3, 2, 1, 0,-1,-2,-3};

    public static void main(String[] args) throws Exception {
        int bound = 1000;
        HelloNonOpenCL main = new HelloNonOpenCL();
        for (int i = 0; i < bound; i++)
        {
            main.print(main.vectorAdd(a,b));
        }
    }

    private float[] vectorAdd(float[] a, float[] b)
    {
        float[] returnArray = new float[a.length];
        if(a.length == b.length) {
            for (int i = 0; i < a.length; i++)
            {
                returnArray[i] = a[i] + b[i];
            }
        }
        else
        {
            System.err.println("The two vectors are not addable");
            return null;
        }
        return returnArray;
    }
    private void print(float[] printArray)
    {
        for (float printee: printArray)
        {
            System.out.print(printee + " ");
        }
        System.out.println();
    }
}
