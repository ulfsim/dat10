/*
 * Copyright 2008-2013 Heaton Research, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * For more information on Heaton Research copyrights, licenses
 * and trademarks visit:
 * http://www.heatonresearch.com/copyright
 */
package Ulf.opencl;

import org.lwjgl.opencl.*;


import java.util.List;
import java.util.Locale;
import static org.lwjgl.opencl.CL10.*;

import org.apache.flink.api.java.ExecutionEnvironment;




public class OpenClProgram {


    public static void displayInfo() {


        for (int platformIndex = 0; platformIndex < CLPlatform.getPlatforms().size(); platformIndex++) {
            CLPlatform platform = CLPlatform.getPlatforms().get(platformIndex);
            System.out.println("Platform #" + platformIndex + ":" + platform.getInfoString(CL_PLATFORM_NAME));
            List<CLDevice> devices = platform.getDevices(CL_DEVICE_TYPE_ALL);
            for (int deviceIndex = 0; deviceIndex < devices.size(); deviceIndex++) {
                CLDevice device = devices.get(deviceIndex);
                System.out.printf(Locale.ENGLISH, "Device #%d(%s):%s\n",
                        deviceIndex,
                        UtilCL.getDeviceType(device.getInfoInt(CL_DEVICE_TYPE)),
                        device.getInfoString(CL_DEVICE_NAME));
                System.out.printf(Locale.ENGLISH, "\tCompute Units: %d @ %d mghtz\n",
                        device.getInfoInt(CL_DEVICE_MAX_COMPUTE_UNITS), device.getInfoInt(CL_DEVICE_MAX_CLOCK_FREQUENCY));
                System.out.printf(Locale.ENGLISH, "\tLocal memory: %s\n",
                        UtilCL.formatMemory(device.getInfoLong(CL_DEVICE_LOCAL_MEM_SIZE)));
                System.out.printf(Locale.ENGLISH, "\tGlobal memory: %s\n",
                        UtilCL.formatMemory(device.getInfoLong(CL_DEVICE_GLOBAL_MEM_SIZE)));
                System.out.println();
            }
        }
    }

    public static void main(String[] args) throws Exception {
        final ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();
        System.out.println("Starting... ");
        System.out.println("Running with " + Integer.parseInt(args[0]) + " iterations");
        int weightSize = 13;
        int dataSize = 14;
        int iterations = 0;
        String dataPath = null; //FOR TEST D:\Datasets\train.txt

        if(args.length == 1) {
            try {
                iterations = Integer.parseInt(args[0]);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        Logistic log = new Logistic(weightSize);
        WineDataWrapper wdw = Kmeans.getWineInstances("winedata.csv");
        int features = wdw.instances.get(0).features.length;
        double[] wineData = new double[wdw.instances.size()*features];
        for(int i = 0; i < wdw.instances.size(); i++)
        {
            WineInstance instance = wdw.instances.get(i);
            for (int j = 0; j < features; j++)
            {
                wineData[i*features+j] = instance.features[j];
            }
        }

        double[] first = Logistic.readCSVfile("FixedData1.csv",",",1000000,14);

        OpenCLObject object = new OpenCLObject(first,dataSize,weightSize);
        OpenCLObject kmeans = new OpenCLObject(wineData,4896,features);
        //object.runLogistic("cl/functions.txt", "logistic",iterations);
        kmeans.runKMeans("cl/kmeans.txt","kmeans",iterations,50,wdw.minValues,wdw.maxValues);


    }
    public static double[] convertFloatsToDoubles(float[] input)
    {
        if (input == null)
        {
            return null; // Or throw an exception - your choice
        }
        double[] output = new double[input.length];
        for (int i = 0; i < input.length; i++)
        {
            output[i] = input[i];
        }
        return output;
    }
}