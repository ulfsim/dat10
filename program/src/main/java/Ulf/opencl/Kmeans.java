package Ulf.opencl;

import akka.japi.Util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;

public class Kmeans
{
    private static HashMap<Integer,List<WineInstance>> kMeans(WineDataWrapper wdw, List<Centroid> centroids, int iterations)
    {
        final double bigNumber = Double.MAX_VALUE;
        double minimum;
        double distance = 0.0;

        HashMap<Integer,List<WineInstance>> centroidMap = new HashMap<>();

        for(int it = 0; it < iterations; it++) {
            centroidMap.clear();
            for (int i = 0; i < centroids.size(); i++)
            {
                centroidMap.put(i,new ArrayList<>());
            }
            for (int i = 0; i < wdw.instances.size(); i++) {
                WineInstance instance = wdw.instances.get(i);
                minimum = bigNumber;
                for (int j = 0; j < centroids.size(); j++) {
                    distance = distance(instance, centroids.get(j));
                    if (minimum > distance) {
                        minimum = distance;
                        instance.setCluster(j);
                    }
                }
                centroidMap.get(instance.cluster).add(instance);
            }
            for (int i = 0; i < centroidMap.size(); i++) {
                double[] newPostion = new double[centroids.get(i).features.length];
                for (int j = 0; j < centroidMap.get(i).size(); j++) {
                    double[] tempFeatures = centroidMap.get(i).get(j).features;
                    for (int k = 0; k < newPostion.length; k++) {
                        newPostion[k] += tempFeatures[k];
                    }
                }
                for (int j = 0; j < newPostion.length; j++) {
                    newPostion[j] /= centroidMap.get(i).size();
                }
            }
        }
        return centroidMap;
    }

    private static double distance(WineInstance instance, Centroid c)
    {
        double result = 0;
        for (int i = 0; i < instance.features.length; i++)
        {
            result += Math.sqrt(Math.pow(instance.features[i] - c.features[i],2));
        }
        return result;
    }


    public static class Centroid
    {

        public double[] features;

        public Centroid(int dimensions, double[] minvalues, double[] maxvalues,UtilCL utilCl)
        {
            features = new double[dimensions];
            for (int i = 0; i < dimensions; i++)
            {
                this.features[i] = utilCl.getRandomDoubleInRange(minvalues[i],maxvalues[i]);
            }
        }
    }

    public static void main(String[] args)
    {
        long startTime = System.currentTimeMillis();
        int numberOfClusters = 50;

        WineDataWrapper WDW = getWineInstances("winedata.csv");
        List<Centroid> centroidList = new ArrayList<>();
        UtilCL utilCL = new UtilCL();
        for(int i = 0; i < numberOfClusters; i++)
        {
            centroidList.add(new Centroid(WDW.instances.get(0).features.length,WDW.minValues,WDW.maxValues,utilCL));
        }

        HashMap<Integer,List<WineInstance>> result = kMeans(WDW,centroidList,10);
        long endTime = System.currentTimeMillis();
        System.out.println("Run took at total of " + ((endTime-startTime)) + " ms");
    }
    public static WineDataWrapper getWineInstances(String filePath)
    {
        String line = "";
        List<WineInstance> instances = new ArrayList<>();
        List<Double> minValues = new ArrayList<>();
        List<Double> maxValues = new ArrayList<>();
        try {
            BufferedReader br = new BufferedReader(new FileReader(filePath));
            while((line = br.readLine()) != null)
            {
                String[] splittedLine = line.split(";");
                double[] features = new double[splittedLine.length];
                for(int i = 0; i < splittedLine.length; i++)
                {
                    features[i] = Double.parseDouble(splittedLine[i]);
                }
                WineInstance wineInstance = new WineInstance(features);
                if(instances.size() == 0)
                {
                    minValues = DoubleStream.of(features).mapToObj(Double::valueOf).collect(Collectors.toCollection(ArrayList::new));
                    maxValues = DoubleStream.of(features).mapToObj(Double::valueOf).collect(Collectors.toCollection(ArrayList::new));
                }
                else
                {
                    for(int i = 0; i < features.length; i++)
                    {
                        if(minValues.get(i) > features[i])
                            minValues.set(i,features[i]);
                        else if(maxValues.get(i) < features[i])
                            maxValues.set(i,features[i]);
                    }
                }
                instances.add(wineInstance);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        double[] minArray = new double[minValues.size()];
        double[] maxArray = new double[maxValues.size()];
        for (int i = 0; i < minArray.length; i++)
        {
            minArray[i] = minValues.get(i);
            maxArray[i] = maxValues.get(i);
        }
        return new WineDataWrapper(instances,minArray,maxArray);
    }
}