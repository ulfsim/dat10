package Ulf.opencl;

import java.util.List;

/**
 * Created by sygob on 12-01-2017.
 */
public class WineDataWrapper {
    List<WineInstance> instances;
    double[] minValues;
    double[] maxValues;

    public WineDataWrapper(List<WineInstance> instances, double[] minValues, double[] maxValues)
    {
        this.instances = instances;
        this.minValues = minValues;
        this.maxValues = maxValues;
    }
}
