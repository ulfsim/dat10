/*
 * Copyright 2008-2013 Heaton Research, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * For more information on Heaton Research copyrights, licenses
 * and trademarks visit:
 * http://www.heatonresearch.com/copyright
 */

/*
 * Modified by Ulf Gaarde Simonsen 21/11-16
 */
package Ulf.opencl;

import org.lwjgl.BufferUtils;
import program.Utility;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import java.text.DecimalFormat;

import static org.lwjgl.opencl.CL10.*;

/**
 * Some utilities for OpenCL
 */
public class UtilCL {

    private Random random;

    public UtilCL()
    {
        random = new Random();
        random.setSeed(System.currentTimeMillis());
    }

    /**
     * Format a number to a memory size.
     * @param size The size.
     * @return The formatted size!
     */
    public static String formatMemory(long size) {
        if(size <= 0) return "0";
        final String[] units = new String[] { "B", "KB", "MB", "GB", "TB" };
        int digitGroups = (int) (Math.log10(size)/Math.log10(1024));
        return new DecimalFormat("#,##0.#").format(size/Math.pow(1024, digitGroups)) + " " + units[digitGroups];
    }

    /**
     * Get the device type.
     * @param i The device type id.
     * @return The device type.
     */
    public static String getDeviceType(int i) {
        switch(i) {
            case CL_DEVICE_TYPE_DEFAULT: return "DEFAULT";
            case CL_DEVICE_TYPE_CPU: return "CPU";
            case CL_DEVICE_TYPE_GPU: return "GPU";
            case CL_DEVICE_TYPE_ACCELERATOR: return "ACCELERATOR";
        }
        return "?";
    }

    /**
     * Utility method to convert float array to float buffer
     *
     * @param floats - the float array to convert
     * @return a float buffer containing the input float array
     */
    public static FloatBuffer toFloatBuffer(float[] floats) {
        FloatBuffer buf = BufferUtils.createFloatBuffer(floats.length).put(floats);
        buf.rewind();
        return buf;
    }
    public static DoubleBuffer toDoubleBuffer(double[] doubles) {
        DoubleBuffer buf = BufferUtils.createDoubleBuffer(doubles.length).put(doubles);
        buf.rewind();
        return buf;
    }

    /**
     * Utility method to convert 2d float array to float buffer
     *
     * @param floats - the 2d float array to convert
     * @return a single dimension float buffer containing the 2d float array
     */
    public static FloatBuffer from2DToFloatBuffer(float[][] floats)
    {
        int size = floats.length * floats[0].length;
        float[] singleArray = new float[size];
        for (int i = 0; i < floats.length; i++)
        {
            for(int j = 0; j < floats[0].length; j++)
            {
                singleArray[j+i] = floats[i][j];
            }
        }
        FloatBuffer buf = BufferUtils.createFloatBuffer(singleArray.length).put(singleArray);
        buf.rewind();
        return buf;
    }


    /**
     * Utility method to print a float buffer
     *
     * @param buffer - the float buffer to print to System.out
     */
    public static void print(FloatBuffer buffer) {
        for (int i = 0; i < buffer.capacity(); i++) {
            System.out.print(buffer.get(i) + " ");
        }
        System.out.println("");
    }
    /**
     * Utility method to print a double buffer
     *
     * @param buffer - the double buffer to print to System.out
     */
    public static void print(DoubleBuffer buffer) {
        for (int i = 0; i < buffer.capacity(); i++) {
            System.out.print(buffer.get(i) + " ");
        }
        System.out.println("");
    }


    /**
     * Read a resource into a string.
     * @param filePath The resource to read.
     * @return The resource as a string.
     * @throws java.io.IOException
     */
    public static String getResourceAsString(String filePath) throws IOException {
        InputStream is = UtilCL.class.getClassLoader().getResourceAsStream(filePath);
        if (is == null) {
            throw new IOException("Can't find resource: " + filePath);
        }
        BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = br.readLine()) != null) {
            sb.append(line);
        }
        return sb.toString();
    }
    //Returns array with space for results of all champion/item combinations
    public static float[] getResultArray()
    {
        int champAmount = 128;
        int itemAmount = 250;

        float[] resultArray = new float[champAmount*itemAmount];

        return resultArray;
    }
    //Array implementation of a simple map
    public static float[] getChampionIdArray(int maxChampId)
    {
        float[] champIdArray = new float[maxChampId];
        float[] champArray = Utility.getChampionIdArray(128);
        for(int i = 0; i < champArray.length; i++)
        {
            float temp = champArray[i];
            champIdArray[(int)temp] = i;
        }
        return champIdArray;
    }
    //Array implementation of a simple map
    public static float[] getItemIdArray(int maxItemId)
    {
        float[] itemIIdArray = new float[maxItemId];
        float[] itemArray = Utility.getItemIdArray(250);
        for (int i = 0; i < itemArray.length; i++)
        {
            float temp = itemArray[i];
            itemIIdArray[(int)temp] = i;
        }
        return itemIIdArray;
    }
    //Input: Max and min number as double
    //Output: A random number in the range of max and min
    public double getRandomDoubleInRange(double min, double max)
    {
        double randomValue = min + (max - min) * random.nextDouble();
        return randomValue;
    }
}
