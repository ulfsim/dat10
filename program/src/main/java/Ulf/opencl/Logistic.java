package Ulf.opencl;

import org.apache.flink.api.java.tuple.Tuple14;
import org.lwjgl.Sys;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;


/**
 * Performs simple logistic regression.
 * User: tpeng
 * Date: 6/22/12
 * Time: 11:01 PM
 *
 * @author tpeng
 * @author Matthieu Labas
 */
public class Logistic {

    /** the learning rate */
    private double rate;

    /** the weight to learn */
    private double[] weights;

    /** the number of iterations */
    private int ITERATIONS = 3000;

    public float[] getWeightsAsFloats() {
        float[] floatArray = new float[weights.length];
        for(int i = 0; i < weights.length; i++)
        {
            floatArray[i] = (float)weights[i];
        }
        return floatArray;
    }

    public void setWeights(float[] input) {
        for(int i = 0; i < weights.length; i++)
        {
            weights[i] = input[i];
        }
    }

    public Logistic(int n) {
        this.rate = 0.0001;
        weights = new double[n];
    }

    private static double sigmoid(double z) {
        return 1.0 / (1.0 + Math.exp(-z));
    }

    public void train(List<Instance> instances) {
        for (int n=0; n<ITERATIONS; n++) {
            double lik = 0.0;
            for (int i=0; i<instances.size(); i++) {
                int[] x = instances.get(i).x;
                double predicted = classify(x);
                int label = instances.get(i).label;
                for (int j=0; j<weights.length; j++) {
                    weights[j] = weights[j] + rate * (label - predicted) * x[j];
                }
                // not necessary for learning
                lik += label * Math.log(classify(x)) + (1-label) * Math.log(1- classify(x));
            }
            System.out.println("iteration: " + n + " " + Arrays.toString(weights) + " mle: " + lik);
        }
    }

    public void bigTrain(bigInstance bi)
    {
        int[] features = bi.features;
        int label = bi.label;
        double predicted = classify(features);

        for (int j=0; j<weights.length; j++) {
            weights[j] = weights[j] + rate * (label - predicted) * features[j];
        }
    }

    private double classify(int[] x) {
        double logit = .0;
        for (int i=0; i<weights.length;i++)  {
            logit += weights[i] * x[i];
        }
        return sigmoid(logit);
    }
    private double classify(double[] features,double[]weights) {
        double logit = .0;
        for (int i=0; i<weights.length;i++)  {
            logit += weights[i] * features[i];
        }
        return sigmoid(logit);
    }

    public static class Instance {
        public int label;
        public int[] x;

        public Instance(int label, int[] x) {
            this.label = label;
            this.x = x;
        }
    }

    public static class bigInstance
    {
        public int label;
        public int[] features;

        public bigInstance(int label, int[] features)
        {
            this.label = label;
            this.features = features;
        }
    }

    public static List<Instance> readDataSet(String file) throws FileNotFoundException {
        List<Instance> dataset = new ArrayList<Instance>();
        Scanner scanner = null;
        try {
            scanner = new Scanner(new File(file));
            while(scanner.hasNextLine()) {
                String line = scanner.nextLine();
                if (line.startsWith("#")) {
                    continue;
                }
                String[] columns = line.split("\\s+");

                // skip first column and last column is the label
                int i = 1;
                int[] data = new int[columns.length-2];
                for (i=1; i<columns.length-1; i++) {
                    data[i-1] = Integer.parseInt(columns[i]);
                }
                int label = Integer.parseInt(columns[i]);
                Instance instance = new Instance(label, data);
                dataset.add(instance);
            }
        } finally {
            if (scanner != null)
                scanner.close();
        }
        return dataset;
    }

    public static List<bigInstance> readCriteoData(int readAmount, String filename)
    {
        List<bigInstance> instances = new ArrayList<>();
        int[] currentFeatures = null;
        int currentLabel = 0;

        try(BufferedReader br = new BufferedReader(new FileReader(filename))) {
            int lineLimit = readAmount;
            for (String line; (line = br.readLine()) != null; ) {
                String substring = line;
                int currentFeature = 0;
                currentFeatures = new int[13];

                currentLabel = Character.getNumericValue(line.charAt(0));
                substring = substring.substring(2, substring.length());
                while (substring != "") {
                    int tab = substring.indexOf("\t");
                    if (tab == 0) {
                        currentFeatures[currentFeature] = 0;
                        currentFeature++;
                        substring = substring.substring(1, substring.length());
                        if (currentFeature > 12)
                            break;
                        continue;
                    } else {
                        String value = substring.substring(0, tab);
                        currentFeatures[currentFeature] = Integer.parseInt(value);
                        currentFeature++;
                        substring = substring.substring(tab + 1, substring.length());
                        if (currentFeature > 12)
                            break;
                    }
                }
                bigInstance instance = new bigInstance(currentLabel, currentFeatures);
                instances.add(instance);
                lineLimit--;
                if (lineLimit <= 0)
                    break;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return instances;
    }

    public static bigInstance tupleToBigInstance(Tuple14<Integer,Integer,Integer,Integer,Integer,Integer,Integer,Integer,Integer,Integer,Integer,Integer,Integer,Integer> t14)
    {
        int label = t14.f0;
        int[] features = new int[13];
        for (int i = 1;i < 14; i++)
        {
            features[i-1] = t14.getField(i);
        }
        return new bigInstance(label,features);
    }
    public static double[] readCSVfile(String filePath,String lineSplitter,int instances, int sizeOfInstance)
    {
        double[] instanceArray = new double[instances * sizeOfInstance];
        String line = "";
        int count = 0;

        try {
            BufferedReader br = new BufferedReader(new FileReader(filePath));
            while((line = br.readLine()) != null)
            {
                String[] instance = line.split(lineSplitter);
                for(int i = 0; i < instance.length; i++)
                {
                    instanceArray[count] = Double.parseDouble(instance[i]);
                    count++;
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return instanceArray;
        }
        return instanceArray;
    }

    public static void FixData(List<Logistic.bigInstance> data, String FileName)
    {
        FileWriter fw = null;
        BufferedWriter bw = null;
        String content = "";
        try {
            fw = new FileWriter(FileName);
            bw = new BufferedWriter(fw);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        for(bigInstance l : data) {
            try {
                content = content + l.label + ",";
                for(int i : l.features)
                {
                    content = content + i + ",";
                }

                bw.write(content);
                bw.newLine();
                content = "";
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        try{
            if(bw != null)
                bw.close();
            if(fw != null)
                fw.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public static Logistic trainCPU(String file, int instances ,int iterations) throws FileNotFoundException{
        Logistic log = new Logistic(13);
        int[] currentFeatures = null;
        int currentLabel = 0;

        for(int it = 0; it < iterations; it++) {
            try (BufferedReader br = new BufferedReader(new FileReader(file))) {
                int lineLimit = instances;
                for (String line; (line = br.readLine()) != null; ) {
                    String substring = line;
                    int currentFeature = 0;
                    currentFeatures = new int[13];
                    int count = 0;

                    currentLabel = Character.getNumericValue(line.charAt(0));
                    substring = substring.substring(2, substring.length());
                    while (substring != "") {
                        int tab = substring.indexOf("\t");
                        if (tab == 0) {
                            currentFeatures[currentFeature] = 0;
                            currentFeature++;
                            substring = substring.substring(1, substring.length());
                            if (currentFeature > 12)
                                break;
                            continue;
                        } else {
                            String value = substring.substring(0, tab);
                            currentFeatures[currentFeature] = Integer.parseInt(value);
                            currentFeature++;
                            substring = substring.substring(tab + 1, substring.length());
                            if (currentFeature > 12)
                                break;
                        }
                    }
                    bigInstance instance = new bigInstance(currentLabel, currentFeatures);
                    log.bigTrain(instance);
                    lineLimit--;
                    if (lineLimit <= 0)
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return log;
    }

    public float[] transformToSingleArray(List<bigInstance> instances)
    {
        float[] arrayOfInstances = new float[instances.size()*14];
        int i = 0;
        for(bigInstance bi: instances)
        {
            arrayOfInstances[i*14] = bi.label;
            for (int j = 1; j < bi.features.length; j++)
            {
                arrayOfInstances[i*14+j] = bi.features[j];
            }
            i++;
        }
        return arrayOfInstances;
    }

    public static void main(String... args) throws FileNotFoundException {
        long startTime = System.currentTimeMillis();
        //List<Instance> instances = readDataSet("dataset.txt");
        Logistic logistic = trainCPU("D:\\Datasets\\train.txt",10000,10000);

        int[] x = {0,29,60,6,7260,437,1,4,14,0,1,0,6};
        System.out.println("prob(1|x) = " + logistic.classify(x));

        int[] x2 = {27,17,45,28,2,28,27,29,28,1,1,0,23};
        System.out.println("prob(1|x2) = " + logistic.classify(x2));
        int[] x3 = {0,1,20,16,1548,93,42,32,912,0,15,1,16};
        System.out.println("prob 1|x3 = " + logistic.classify(x3));
        int[] x4 = {1,1,5,0,1382,4,15,2,181,1,2,0,2};
        System.out.println("prob (1|x4) = " + logistic.classify(x4));
        long endTime   = System.currentTimeMillis();
        long totalTime = endTime - startTime;
        System.out.println(totalTime);
    }

}