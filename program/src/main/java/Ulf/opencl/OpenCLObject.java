package Ulf.opencl;

/**
 * Created by ulf on 22-11-2016.
 */

import org.lwjgl.BufferUtils;
import org.lwjgl.PointerBuffer;
import org.lwjgl.Sys;
import org.lwjgl.opencl.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.DoubleBuffer;
import java.util.*;

import static org.lwjgl.opencl.CL10.*;



public class OpenCLObject {
    CLPlatform platform;
    List<CLDevice> devices;
    CLContext context;
    CLCommandQueue queue;

    int instances;
    int dataSize;
    int weightSize;

    double[] dataArray;

    DoubleBuffer dataBuffer;
    DoubleBuffer weightBuffer;
    DoubleBuffer answerBuffer;

    CLMem answerMemory;
    CLMem weightMemory;
    CLMem dataMemory;

    CLProgram program;
    CLKernel kernel;

    MemoryObject memObject;

    public OpenCLObject(double[] dataArray,int instances, int weightSize) throws Exception {
        CL.create();
        displayInfo();
        dataSize = 0;
        platform = CLPlatform.getPlatforms().get(0);
        devices = platform.getDevices(CL_DEVICE_TYPE_GPU);
        context = CLContext.create(platform, devices, null, null, null);
        if(!devices.isEmpty()) {
            System.out.println("Creating for GPU...");
            queue = clCreateCommandQueue(context, devices.get(0), CL_QUEUE_PROFILING_ENABLE, null);
        }
        else {
            platform = CLPlatform.getPlatforms().get(1);
            devices = platform.getDevices(CL_DEVICE_TYPE_CPU);
            queue = clCreateCommandQueue(context,devices.get(0),CL_QUEUE_PROFILING_ENABLE, null);
            System.out.println("No GPU present, creating for CPU...");
        }

        memObject = calculateMemoryAllocation(devices.get(0), dataArray, instances, weightSize);

        this.dataArray = dataArray;
        this.instances = instances;
        this.weightSize = weightSize;
    }

    public double[] runLogistic (String sourceName, String funcName, int iterations)throws Exception{
        long startTime = System.currentTimeMillis();

        int localIterations = (int)memObject.getIterations();
        int kernelAmount = (int)memObject.getKernelAmount();
        int instancesPrKernel = (int)memObject.getInstancesPrKernel();

        double[] weightsArray = new double[weightSize];
        double[] answerArray = new double[weightSize*kernelAmount];
        double[] tempAnswerArray = new double[weightSize];

        String pre1Program = getResourceAsString(sourceName);
        String pre2Program = pre1Program.replaceAll("DATLENGTHVALUE",Integer.toString(dataSize));
        String pre3Program = pre2Program.replaceAll("WEIGHTLENGTHVALUE",Integer.toString(weightSize));

        program = clCreateProgramWithSource(context, pre3Program, null);

        clBuildProgram(program, devices.get(0), "", null); // Prints out error messages from the OpenCL kernel compilation
        System.out.println(program.getBuildInfoString(devices.get(0), CL_PROGRAM_BUILD_LOG));

        kernel = clCreateKernel(program,funcName,null);
        for(int totalIt = 0; totalIt < iterations; totalIt++){
            for(int i = 0; i < localIterations; i++) {
                if (localIterations == 1) {
                    dataBuffer = UtilCL.toDoubleBuffer(dataArray);
                } else {
                    if(i == localIterations -1)
                    {
                        double[] temp = Arrays.copyOfRange(dataArray,i*instancesPrKernel,dataArray.length-1);
                        dataBuffer = UtilCL.toDoubleBuffer(temp);
                    }
                    else
                    {
                        double[] temp = Arrays.copyOfRange(dataArray,i*instancesPrKernel,i+1*(instancesPrKernel-1));
                        dataBuffer = UtilCL.toDoubleBuffer(temp);
                    }
                }
                weightBuffer = UtilCL.toDoubleBuffer(weightsArray);
                answerBuffer = UtilCL.toDoubleBuffer(answerArray);

                dataMemory = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,dataBuffer, null);
                weightMemory = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, weightBuffer, null);
                answerMemory = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, answerBuffer, null);
                clEnqueueWriteBuffer(queue, dataMemory, 1, 0, dataBuffer, null, null);
                clEnqueueWriteBuffer(queue, weightMemory, 1, 0, weightBuffer, null, null);

                clFinish(queue);

                PointerBuffer kernel1DGlobalWorkSize = BufferUtils.createPointerBuffer(1);
                kernel1DGlobalWorkSize.put(0, kernelAmount);
                kernel.setArg(0,dataMemory);
                kernel.setArg(1,weightMemory);
                kernel.setArg(2,answerMemory);
                kernel.setArg(3,instancesPrKernel);
                clEnqueueNDRangeKernel(queue, kernel, 1, null, kernel1DGlobalWorkSize, null, null, null);
                clEnqueueReadBuffer(queue,answerMemory,1,0,answerBuffer,null,null);
                clFinish(queue);

                for(int j = 0; j < weightSize*kernelAmount; j++) {
                    tempAnswerArray[j%weightSize] += answerBuffer.get(j)/instancesPrKernel*kernelAmount;
                }
                /*for(int j = 0; j < weightSize; j++)
                {
                    tempAnswerArray[j] /= instancesPrKernel*kernelAmount;
                }*/

                weightsArray = tempAnswerArray;
                clReleaseMemObject(dataMemory);
                clReleaseMemObject(weightMemory);
                clReleaseMemObject(answerMemory);
            }
            if(totalIt % 100 == 0)
                System.out.println(totalIt);
        }
        System.out.println();
        clReleaseKernel(kernel);
        clReleaseProgram(program);
        clReleaseCommandQueue(queue);
        clReleaseContext(context);
        CL.destroy();

        long endTime   = System.currentTimeMillis();
        long totalTime = endTime - startTime;
        System.out.println("Run took a total of " + totalTime + " milliseconds");
        return weightsArray;
    }

    public double[] runKMeans (String sourceName, String funcName, int iterations, int centeroids, double[] minvalues, double[] maxvalues) throws Exception
    {
        long startTime = System.currentTimeMillis();

        int localIterations = (int)memObject.getIterations();
        int kernelAmount = (int)memObject.getKernelAmount();
        int instancesPrKernel = (int)memObject.getInstancesPrKernel();

        double[] centroidArray = new double[weightSize*centeroids];
        double[] answerArray = new double[instancesPrKernel*kernelAmount];
        double[] tempAnswerArray = new double[answerArray.length];

        UtilCL utilCL = new UtilCL();
        List<Kmeans.Centroid> centroidList = new ArrayList<>();
        for(int i = 0; i < centeroids; i++)
        {
            centroidList.add(new Kmeans.Centroid(weightSize,minvalues,maxvalues,utilCL));
        }
        for(int i = 0; i < centeroids; i++)
        {
            double[] temp = centroidList.get(i).features;
            for (int j = 0; j < weightSize; j++)
            {
                centroidArray[i*weightSize+j] = temp[j];
            }
        }

        String pre1Program = getResourceAsString(sourceName);
        String pre2Program = pre1Program.replaceAll("CENTROIDLENGTH",Integer.toString(weightSize));

        program = clCreateProgramWithSource(context, pre2Program, null);

        clBuildProgram(program, devices.get(0), "", null); // Prints out error messages from the OpenCL kernel compilation
        System.out.println(program.getBuildInfoString(devices.get(0), CL_PROGRAM_BUILD_LOG));

        kernel = clCreateKernel(program,funcName,null);
        CLKernel kernel2 = clCreateKernel(program,"update",null);
            for(int i = 0; i < localIterations; i++) {
                if (localIterations == 1) {
                    dataBuffer = UtilCL.toDoubleBuffer(dataArray);
                } else {
                    if(i == localIterations -1) //If last iterations, copy the rest of the data
                    {
                        double[] temp = Arrays.copyOfRange(dataArray,i*instancesPrKernel,dataArray.length-1);
                        dataBuffer = UtilCL.toDoubleBuffer(temp);
                    }
                    else
                    {
                        double[] temp = Arrays.copyOfRange(dataArray,i*instancesPrKernel,i+1*(instancesPrKernel-1));
                        dataBuffer = UtilCL.toDoubleBuffer(temp);
                    }
                }


                weightBuffer = UtilCL.toDoubleBuffer(centroidArray);
                answerBuffer = UtilCL.toDoubleBuffer(answerArray);

                dataMemory = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,dataBuffer, null);
                weightMemory = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, weightBuffer, null);
                answerMemory = clCreateBuffer(context, CL_MEM_WRITE_ONLY | CL_MEM_COPY_HOST_PTR, answerBuffer, null);
                clEnqueueWriteBuffer(queue, dataMemory, 1, 0, dataBuffer, null, null);
                clEnqueueWriteBuffer(queue, weightMemory, 1, 0, weightBuffer, null, null);

                clFinish(queue);

                PointerBuffer kernel1DGlobalWorkSize = BufferUtils.createPointerBuffer(1);
                kernel1DGlobalWorkSize.put(0, kernelAmount);
                kernel.setArg(0,dataMemory);
                kernel.setArg(1,weightMemory);
                kernel.setArg(2,answerMemory);
                kernel.setArg(3,instancesPrKernel);
                kernel2.setArg(0,weightMemory);
                kernel2.setArg(1,answerMemory);
                kernel2.setArg(2,instancesPrKernel);
                clEnqueueNDRangeKernel(queue, kernel, 1, null, kernel1DGlobalWorkSize, null, null, null);
                for(int j = 0; j < iterations; j++)
                {
                    clEnqueueReadBuffer(queue, answerMemory, 1, 0, answerBuffer, null, null);
                    clFinish(queue);
                }
                HashMap<Integer,List<double[]>> centroidMap = new HashMap<Integer, List<double[]>>();

                for(int k = 0; k < centeroids; k++)
                {
                    List<double[]> instanceList = new ArrayList<>();
                    centroidMap.put(k,instanceList);
                }

                for (int k = 0; k < answerBuffer.limit();k++)
                {
                    tempAnswerArray[k] = answerBuffer.get(k);
                }
                int currentCluster = 0;
                double currentDistance = Double.MAX_VALUE;
                for(int k = 0; k < instancesPrKernel;k++)
                {
                    for(int j = 0; j < kernelAmount; j++)
                    {
                        double temp = tempAnswerArray[j*instancesPrKernel+k];
                        if(temp < currentDistance)
                        {
                            currentDistance = temp;
                            currentCluster = j;
                        }
                    }
                    double[] currentInstance = Arrays.copyOfRange(dataArray,k*dataSize,k*dataSize+dataSize);
                    centroidMap.get(currentCluster).add(currentInstance);
                    currentDistance = Double.MAX_VALUE;
                    currentCluster = 0;
                }
                for (int k = 0; k < centroidMap.size(); k++)
                {
                    double[] newPosition = new double[weightSize];
                    List<double[]> currentList = centroidMap.get(k);
                    for(int j = 0; j < currentList.size(); j++)
                    {
                        double[] currentArray = currentList.get(j);
                        for(int t = 0; t < weightSize; t++)
                        {
                            newPosition[t] += currentArray[t];
                        }
                    }
                    for(int j = 0; j < weightSize; j++)
                    {
                        int calcPos = k* centroidMap.size()+j;
                        if(currentList.size() != 0)
                            centroidArray[calcPos] = newPosition[j]/currentList.size();
                    }
                }
                clReleaseMemObject(dataMemory);
                clReleaseMemObject(weightMemory);
                clReleaseMemObject(answerMemory);
            }
        System.out.println();
        clReleaseKernel(kernel);
        clReleaseProgram(program);
        clReleaseCommandQueue(queue);
        clReleaseContext(context);
        CL.destroy();
        UtilCL.print(answerBuffer);
        long endTime   = System.currentTimeMillis();
        long totalTime = endTime - startTime;
        System.out.println("Run took a total of " + totalTime + " milliseconds");
        return centroidArray;
    }
    //Function created by Jeff Heaton https://github.com/jeffheaton
    public static String getResourceAsString(String filePath) throws IOException {
        InputStream is = UtilCL.class.getClassLoader().getResourceAsStream(filePath);
        if (is == null) {
            throw new IOException("Can't find resource: " + filePath);
        }
        BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = br.readLine()) != null) {
            sb.append(line);
        }
        return sb.toString();
    }

    //Function created by Jeff Heaton https://github.com/jeffheaton
    public static void displayInfo() {


        for (int platformIndex = 0; platformIndex < CLPlatform.getPlatforms().size(); platformIndex++) {
            CLPlatform platform = CLPlatform.getPlatforms().get(platformIndex);
            System.out.println("Platform #" + platformIndex + ":" + platform.getInfoString(CL_PLATFORM_NAME));
            List<CLDevice> devices = platform.getDevices(CL_DEVICE_TYPE_ALL);
            for (int deviceIndex = 0; deviceIndex < devices.size(); deviceIndex++) {
                CLDevice device = devices.get(deviceIndex);
                System.out.printf(Locale.ENGLISH, "Device #%d(%s):%s\n",
                        deviceIndex,
                        UtilCL.getDeviceType(device.getInfoInt(CL_DEVICE_TYPE)),
                        device.getInfoString(CL_DEVICE_NAME));
                System.out.printf(Locale.ENGLISH, "\tCompute Units: %d @ %d mghtz\n",
                        device.getInfoInt(CL_DEVICE_MAX_COMPUTE_UNITS), device.getInfoInt(CL_DEVICE_MAX_CLOCK_FREQUENCY));
                System.out.printf(Locale.ENGLISH, "\tLocal memory: %s\n",
                        UtilCL.formatMemory(device.getInfoLong(CL_DEVICE_LOCAL_MEM_SIZE)));
                System.out.printf(Locale.ENGLISH, "\tGlobal memory: %s\n",
                        UtilCL.formatMemory(device.getInfoLong(CL_DEVICE_GLOBAL_MEM_SIZE)));
                System.out.println();
            }
        }
    }
    //Input: Current OpenCL Device, data memory, amount of data instances, size of weights and results.
    //Output: MemoryObject containing the number of iterations needed and the number of instances that should be used for each iteration
    private MemoryObject calculateMemoryAllocation(CLDevice currentDevice, double[] dataMemory, int dataInstances, int weightSize)
    {
        long globalMemSize = currentDevice.getInfoLong(CL_DEVICE_GLOBAL_MEM_SIZE);
        long computeUnits = currentDevice.getInfoInt(CL_DEVICE_MAX_COMPUTE_UNITS);

        int sizeOfDouble = Double.BYTES;

        long memPrKernel = globalMemSize/computeUnits;
        long memLeft = memPrKernel;
        memLeft -= (weightSize*2) * sizeOfDouble; //One instance of local and global weights

        int instanceSize = dataMemory.length/dataInstances;
        dataSize = instanceSize;
        int sizeOfInstance = instanceSize * sizeOfDouble;


        long maxInstancesPrKernel = memLeft / sizeOfInstance;
        long instancesPrKernel; //Used to store the final value of instances per kernel
        long iterations; //Used to store the final value of iterations

        if(dataInstances < (maxInstancesPrKernel*computeUnits))
        {
            instancesPrKernel = dataInstances/computeUnits;
            iterations = 1;
            MemoryObject memoryObject = new MemoryObject(iterations,computeUnits,instancesPrKernel); //MemoryObject contains iterations and instancesPrKernel
            return memoryObject;
        }
        else
        {
            instancesPrKernel = maxInstancesPrKernel;
            iterations = dataInstances / instancesPrKernel;
        }

        int dataTotalBytes = dataInstances * sizeOfInstance;

        if((dataTotalBytes != 0) && !(dataTotalBytes % instancesPrKernel == 0)) // Handle case of perfect memory fit
            iterations += 1;
        MemoryObject memoryObject = new MemoryObject(iterations,computeUnits,instancesPrKernel); //MemoryObject contains iterations and instancesPrKernel

        return memoryObject;
    }
}
