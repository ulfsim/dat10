package program;

import program.classes.MatchDetail;

import java.io.*;
import java.util.*;

/**
 * Created by Ulf on 11/02/16.
 */
public class RankingRecommender {
    Long championId = 0L;
    Map<Long,Double> ratingMap = null;

    public RankingRecommender(Long championId)
    {
        this.championId = championId;
        ratingMap = Utility.getChampionIdMap(true);
        Nullify(ratingMap);
    }
    public RankingRecommender() {

    }
    public float[] runGPU()
    {
        String osPath = Utility.GetOSDataPath();
        String filepath = osPath + "na/na1";
        Recommender rm = new Recommender();
        List<MatchDetail> matchDetails;

        matchDetails = this.getMatches(filepath);
        List<float[]> championWeightsArray = rm.GPUArrays();
        float[] champIdArray = this.getChampIdsAsArray();
        for(Float f : champIdArray)
        {
            System.out.print(f + " ");
        }
        return champIdArray;
    }
    public void runNonStochastic()
    {
        String filepath = "D:\\Data\\na\\na";
        Recommender rm = new Recommender();
        List<MatchDetail> matchDetails;
        for(int numofIterations = 0; numofIterations < 100; numofIterations++)
        {
            for(int i = 0; i < 10; i++){
                matchDetails = this.getMatches(filepath + i);
                System.out.println("Using file: " + filepath + i);
                for(int j = 0; j < matchDetails.size()-1;j++)
                {
                    rm.staticMinimize(new Match(matchDetails.get(j)));
                }
            }
        }
        Map<Long,ThetaWeights> weightMap = rm.getTempItemRatings();
        Map<Long,Integer> freqMap = rm.getItemFreqMap();
        Map<Long,ThetaWeights> tempRatingMap = rm.getTempItemRatings();
    }
    public void run()
    {
        String filepath = "D:\\Data\\na\\na";
        Recommender rm = new Recommender();
        List<MatchDetail> matchDetails;

        for(int numOfIterations = 0; numOfIterations < 1; numOfIterations++) {
            int listSize = 30;
            rm.setAlpha(0.1);

            List<Integer> matchList = new ArrayList<>();
            for (int i = 0; i < listSize; i++) {
                matchList.add(i);
            }

            for (int i = 0; i < listSize; i++) {
                int randomNum = Utility.GetRandomInt(listSize - i);
                matchDetails = this.getMatches(filepath + matchList.get(randomNum));
                System.out.println("Using file: " + filepath + randomNum);
                matchList.remove(randomNum);
                int matchListSize = matchDetails.size();

                for (int j = 0; j < matchListSize - 1; j++) {
                    if (j != 0 && j % 100 == 0)
                    {
                        //rm.PrintCostFunction();
                    }
                    int randomInt = Utility.GetRandomInt(matchListSize - j);
                    rm.staticMinimize(new Match(matchDetails.get(randomInt)));
                    matchDetails.remove(randomInt);

                }
                rm.ResetBatchParameter();
                System.out.println("i: " + i);
                rm.setAlpha(rm.getAlpha()*0.7);
            }
            TestMatchData tmd = new TestMatchData();

            int nSize = 20;
            /*
            List<Double> averageAccuracy = new ArrayList<>();
            for (int i = 0; i < nSize ; i++)
            {
                averageAccuracy.add(0.0);
            }

            for(int t = 31 ; t < 34; t++) {
                matchDetails = wc.getMatches("/users/Ulf/data/testData/na/na" + t);
                for (int e = 1; e < nSize + 1; e++) {
                    double averageScore = 0.0;
                    int averageAmount = 0;
                    for (int i = 0; i < matchDetails.size(); i++) {
                        averageScore += tmd.getBestItemBuild(new Match(matchDetails.get(i)), rm, e);
                        averageAmount++;
                    }
                    System.out.println("Average Score is: " + averageScore / averageAmount + " N = " + e);
                    averageAccuracy.set(e-1,averageAccuracy.get(e-1) + (averageScore / averageAmount));
                }
            }
            for (int i = 0; i < nSize; i++)
            {
                averageAccuracy.set(i, averageAccuracy.get(i)/4);
                System.out.println("Average for N=" + (i+1) + ": " + averageAccuracy.get(i));
            }*/
            /*
            List<Double> randomAccuracy = new ArrayList<>();
            for (int i = 0; i < nSize ; i++)
            {
                randomAccuracy.add(0.0);
            }
            for(int t = 31 ; t < 35; t++) {
                matchDetails = this.getMatches("D:\\Data\\na\\na" + t);
                for (int e = 1; e < nSize+1; e++) {
                    double averageScore = 0.0;
                    int averageAmount = 0;
                    for (int i = 0; i < matchDetails.size(); i++) {
                        averageScore += tmd.getRandomItemBuild(new Match(matchDetails.get(i)), rm, e);
                        averageAmount++;
                    }
                    System.out.println("Average Score is: " + averageScore / averageAmount + " N = " + e);
                    randomAccuracy.set(e-1,randomAccuracy.get(e-1) + (averageScore / averageAmount));
                }
            }
            for (int i = 0; i < nSize; i++)
            {
                randomAccuracy.set(i, randomAccuracy.get(i)/4);
                System.out.println("Average for N=" + (i+1) + ": " + randomAccuracy.get(i));
            }*/
        }
        System.out.println("done");
    }
    /*public static void Recommend (Recommender rm, Match m)
    {
        Map<Long,Map<Long,Double>> championMap = new HashMap<>();
        Map<Long,ItemFeatureVector> itemVectors = rm.getItemVectors();

        for(StaticComponent sc : m.getComponentsMap().keySet()) {
            Map<Long, Double> ratingMap;
            if(!championMap.containsKey(sc.getUserChampionId())){
                ratingMap = Utility.getItemIdMap();
                Utility.Nullify(ratingMap);
            }
            else {
                ratingMap = championMap.get(sc.getUserChampionId());
            }

            for (Map.Entry<Long, Double> entry : ratingMap.entrySet()) {
                ThetaWeights tws = itemVectors.get(entry.getKey()).getThetaWeights();
                ratingMap.put(entry.getKey(), rm.staticItemRating(tws, sc));
            }

            championMap.put(sc.getUserChampionId(),ratingMap);
        }
    }*/
    /*public static void Rank(Recommender rm)
    {

        Map<Long,Double> championMap = Utility.getChampionIdMap();
        Map<Long, RankingRecommender> rankMap = new HashMap<>();
        for(Long champId : championMap.keySet())
        {
            rankMap.put(champId,new RankingRecommender(champId));
        }
        for (ItemFeatureVector ifv : rm.getItemVectors().values())
        {
            Map<Long,Double> weightMap = ifv.getThetaWeights().getUserChampionWeights();
            for(Map.Entry<Long,Double> entry : weightMap.entrySet())
            {
                rankMap.get(entry.getKey()).ratingMap.put((long)ifv.getItemId(),entry.getValue());
            }
        }
        for (Map.Entry<Long,RankingRecommender> entry : rankMap.entrySet())
        {
            entry.getValue().printRankToFile();
        }
    }
    private void printRankToFile()
    {
        try {
            File file = new File("/Users/Ulf/dat9/flink-java-project/predictions/eune/" + this.championId + ".txt");
            if (!file.exists()) {
                file.createNewFile();
            }
            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            Map<Long,Double> sortedMap = sortByValue(ratingMap);
            for (Map.Entry<Long,Double> entry : sortedMap.entrySet())
            {
                bw.write(entry.getKey() + " " + entry.getValue());
                bw.newLine();
            }
            bw.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }*/
    private void Nullify(Map<Long,Double> md)
    {
        for(Map.Entry<Long,Double> entry : md.entrySet())
        {
            entry.setValue(0.0);
        }
    }
    public static <K, V extends Comparable<? super V>> Map<K, V>
    sortByValue( Map<K, V> map )
    {
        List<Map.Entry<K, V>> list =
                new LinkedList<Map.Entry<K, V>>( map.entrySet() );
        Collections.sort(list, new Comparator<Map.Entry<K, V>>() {
            public int compare(Map.Entry<K, V> o1, Map.Entry<K, V> o2) {
                return (o1.getValue()).compareTo(o2.getValue());
            }
        });

        Map<K, V> result = new LinkedHashMap<K, V>();
        for (Map.Entry<K, V> entry : list)
        {
            result.put(entry.getKey(), entry.getValue());
        }
        return result;
    }
    public List<MatchDetail> getMatches(String fileLocation)
    {
        List<MatchDetail> returnList= new ArrayList<>();
        try{
            FileReader fr = new FileReader(fileLocation);
            BufferedReader br = new BufferedReader(fr);


            String line;
            while ((line = br.readLine())!= null)
            {
                returnList.add(new MatchDetail(line));
            }
            br.close();
        }
        catch (FileNotFoundException FNFE)
        {
            FNFE.printStackTrace();
            return null;
        }
        catch (IOException IOE)
        {
            IOE.printStackTrace();
            return null;
        }
        return returnList;
    }
    private float[] getChampIdsAsArray()
    {
        Map<Long,Double> championIdMap = Utility.getChampionIdMap(true);
        float[] championIdArray = new float[championIdMap.entrySet().size()];

        int i = 0;
        for(Long champId : championIdMap.keySet())
        {
            float floatChampId = (float)champId;
            championIdArray[i] = floatChampId;
            i++;
        }
        return championIdArray;
    }
    private float[] getItemIdsAsArray()
    {
        Map<Long,Double> itemIdMap = Utility.getItemIdMap(true);
        float[] itemIdArray = new float[itemIdMap.entrySet().size()];

        int i = 0;
        for(Long itemId : itemIdMap.keySet())
        {
            float floatItemId = (float)itemId;
            itemIdArray[i] = floatItemId;
        }
        return itemIdArray;
    }
}
