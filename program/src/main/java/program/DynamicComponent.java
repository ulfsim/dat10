package program;

import program.classes.MatchDetail;
import program.classes.Team;

import java.util.*;

/**
 * Created by Ulf on 21/12/15.
 */
public class DynamicComponent {
    int id;
    int userId;
    List<Long> userItemList;
    Map<Long, List<Long>> enemyItemList;
    double userTurretsDestroyed;
    double enemyTurretsDestroyed;
    double userInhibitorsDestroyed;
    double enemyInhibitorsDestroyed;
    double userDragonsKills;
    double enemyDragonsKills;
    double userBaronKills;
    double enemyBaronKills;
    double userHeraldKills;
    double enemyHeraldKills;

    double divisionWeight = 1;
    private boolean enableVolatileWeights = false;

    //Creates and empty DynamicComponent with given id and all other values to zero or empty
    public DynamicComponent(int id)
    {
        this.id = id;
        userItemList = new ArrayList<>();
        enemyItemList = new HashMap<>();
        userTurretsDestroyed = 0;
        enemyTurretsDestroyed = 0;
        userInhibitorsDestroyed = 0;
        enemyInhibitorsDestroyed = 0;
        userDragonsKills = 0;
        enemyDragonsKills = 0;
        userBaronKills = 0;
        enemyBaronKills = 0;
        userHeraldKills = 0;
        enemyHeraldKills = 0;
    }
    //Extracts the needed data from the Matchdetail using the given userId and assigns it with given id
    public DynamicComponent(int id, MatchDetail md, int userId)
    {
        this.id = id;
        this.userId = userId;
        long userTeamId = md.getParticipants().get(userId).getTeamId();

        int enemyParticipantStart = (userId < 6-1) ? 5-1 : 0;
        userItemList = md.getParticipants().get(userId).getStats().getItems();

        long enemyTeamId = md.getParticipants().get(enemyParticipantStart).getTeamId();

        enemyItemList = new HashMap<>();
        for (int i = enemyParticipantStart; i < enemyParticipantStart + 5; i++)
        {
            long enId = md.getParticipants().get(i).getChampionId();
            List<Long> enItems = md.getParticipants().get(i).getStats().getItems();
            enemyItemList.put(enId,enItems);
        }
        Team userTeam = md.getTeams().get(((int) (long)userTeamId % 99)-1);
        Team enemyTeam = md.getTeams().get(((int)(long)enemyTeamId % 99)-1);
        userTurretsDestroyed = userTeam.getTowerKills()/divisionWeight;
        enemyTurretsDestroyed = enemyTeam.getTowerKills()/divisionWeight;
        userInhibitorsDestroyed = userTeam.getInhibitorKills()/divisionWeight;
        enemyInhibitorsDestroyed = userTeam.getInhibitorKills()/divisionWeight;
        userDragonsKills = userTeam.getDragonKills()/divisionWeight;
        enemyDragonsKills = enemyTeam.getDragonKills()/divisionWeight;
        userBaronKills = userTeam.getBaronKills()/divisionWeight;
        enemyBaronKills = enemyTeam.getBaronKills()/divisionWeight;
        userHeraldKills = userTeam.getRiftHeraldKills()/divisionWeight;
        enemyHeraldKills = enemyTeam.getRiftHeraldKills()/divisionWeight;
    }
    //Creates a DynamicComponent from an existing Dynamic vector and returns a new similar vector with id + 1
    public DynamicComponent(DynamicComponent vector)
    {
        this.id = vector.getId() + 1;
        this.userItemList =  vector.getUserItemList();
        this.enemyItemList = vector.getEnemyItemList();
        this.userTurretsDestroyed = vector.getUserTurretsDestroyed();
        this.enemyTurretsDestroyed = vector.getEnemyTurretsDestroyed();
        this.userInhibitorsDestroyed = vector.getUserInhibitorsDestroyed();
        this.enemyInhibitorsDestroyed = vector.getEnemyInhibitorsDestroyed();
        this.userDragonsKills = vector.getUserDragonsKills();
        this.enemyDragonsKills = vector.getEnemyDragonsKills();
        this.userBaronKills = vector.getUserBaronKills();
        this.enemyBaronKills = vector.getEnemyBaronKills();
        this.userHeraldKills = vector.getUserHeraldKills();
        this.enemyHeraldKills = vector.getEnemyHeraldKills();
    }

    public int getId() {
        return id;
    }

    public List<Long> getUserItemList() {
        return userItemList;
    }


    public Map<Long, List<Long>> getEnemyItemList() {
        return enemyItemList;
    }


    public double getUserTurretsDestroyed() {
        if(enableVolatileWeights)
            return userTurretsDestroyed;
        else
            return 0;
    }


    public double getEnemyTurretsDestroyed() {
        if(enableVolatileWeights)
            return enemyTurretsDestroyed;
        else
            return 0;
    }

    public double getUserInhibitorsDestroyed() {
        if(enableVolatileWeights)
            return userInhibitorsDestroyed;
        else
            return 0;
    }

    public double getEnemyInhibitorsDestroyed() {
        if(enableVolatileWeights)
            return enemyInhibitorsDestroyed;
        else
            return 0;
    }


    public double getUserDragonsKills() {
        if(enableVolatileWeights)
            return userDragonsKills;
        else
            return 0;
    }


    public double getEnemyDragonsKills() {
        if(enableVolatileWeights)
            return enemyDragonsKills;
        else
            return 0;
    }


    public double getUserBaronKills() {
        if(enableVolatileWeights)
            return userBaronKills;
        else
            return 0;
    }


    public double getEnemyBaronKills() {
        if(enableVolatileWeights)
            return enemyBaronKills;
        else
            return 0;
    }


    public double getUserHeraldKills() {
        if(enableVolatileWeights)
            return userHeraldKills;
        else
            return 0;
    }


    public double getEnemyHeraldKills() {
        if(enableVolatileWeights)
            return enemyHeraldKills;
        else
            return 0;
    }


    public int getUserId() { return userId; }

    public void setId(int id) {
        this.id = id;
    }

    //Creates a new DynamicComponent from an existing Dynamic vector
    public DynamicComponent(DynamicComponent vector, int i)
    {
        this.id = vector.getId();
        this.userItemList = new ArrayList<>(vector.getUserItemList());
        List<Long> tmp;
        this.enemyItemList = new HashMap<>(vector.getEnemyItemList());
        for (Map.Entry<Long,List<Long>> ll : enemyItemList.entrySet()){
            tmp = ll.getValue();
            ll.setValue(new ArrayList<>(tmp));
        }
        this.userTurretsDestroyed = vector.getUserTurretsDestroyed();
        this.enemyTurretsDestroyed = vector.getEnemyTurretsDestroyed();
        this.userInhibitorsDestroyed = vector.getUserInhibitorsDestroyed();
        this.enemyInhibitorsDestroyed = vector.getEnemyInhibitorsDestroyed();
        this.userDragonsKills = vector.getUserDragonsKills();
        this.enemyDragonsKills = vector.getEnemyDragonsKills();
        this.userBaronKills = vector.getUserBaronKills();
        this.enemyBaronKills = vector.getEnemyBaronKills();
        this.userHeraldKills = vector.getUserHeraldKills();
        this.enemyHeraldKills = vector.getEnemyHeraldKills();
    }

    public void clearDynamicComponent(){
        userItemList = new ArrayList<>();
        for (Map.Entry<Long,List<Long>> ll : enemyItemList.entrySet()){
            ll.setValue(new ArrayList<Long>());
        }
        userTurretsDestroyed = 0;
        enemyTurretsDestroyed = 0;
        userInhibitorsDestroyed = 0;
        enemyInhibitorsDestroyed = 0;
        userDragonsKills = 0;
        enemyDragonsKills = 0;
        userBaronKills = 0;
        enemyBaronKills = 0;
        userHeraldKills = 0;
        enemyHeraldKills = 0;
    }
}
