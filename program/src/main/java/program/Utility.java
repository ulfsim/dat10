package program;

import program.classes.Event;
import program.classes.Frame;
import program.classes.MatchDetail;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * Created by Ulf on 18/12/15.
 */
public class Utility {


    static Random rand = new Random();
    public static Map<Long,Double> getItemIdMap(boolean randomEnabled)
    {
        HashMap<Long,Double> itemMap = new HashMap<>();
        try {
            String filePath = GetOSIDPath();
            FileReader fr = new FileReader(filePath+"itemIds");
            BufferedReader br = new BufferedReader(fr);
            String line;
            while((line = br.readLine()) !=null)
            {
                if(randomEnabled)
                    itemMap.put(Long.parseLong(line),GetRandomDouble());
                else
                    itemMap.put(Long.parseLong(line),0.0);
            }
            br.close();
        }
        catch (FileNotFoundException FNFE)
        {
            FNFE.printStackTrace();
        }
        catch (IOException IOE)
        {
            IOE.printStackTrace();
        }
        return itemMap;
    }
    public static float[] getItemIdArray(int itemAmount)
    {

        float[] itemArray = new float[itemAmount];
        try {
            String filePath = GetOSIDPath();
            FileReader fr = new FileReader(filePath + "itemIds");
            BufferedReader br = new BufferedReader(fr);
            String line;
            int lineNumber = 0;
            while((line = br.readLine()) !=null)
            {
                itemArray[lineNumber] = Integer.parseInt(line);
                lineNumber++;
            }
            br.close();
        }
        catch (FileNotFoundException FNFE)
        {
            FNFE.printStackTrace();
        }
        catch (IOException IOE)
        {
            IOE.printStackTrace();
        }
        return itemArray;
    }


    public static Map<Long,Double> getChampionIdMap(boolean randomEnabled)
    {
        HashMap<Long,Double> championMap = new HashMap<>();
        try {
            String filePath = GetOSIDPath();
            FileReader fr = new FileReader(filePath + "championIds");
            BufferedReader br = new BufferedReader(fr);
            String line;
            while((line = br.readLine()) !=null)
            {
                if(randomEnabled)
                    championMap.put(Long.parseLong(line),GetRandomDouble());
                else
                    championMap.put(Long.parseLong(line),0.0);
            }
            br.close();
        }
        catch (FileNotFoundException FNFE)
        {
            FNFE.printStackTrace();
        }
        catch (IOException IOE)
        {
            IOE.printStackTrace();
        }
        return championMap;
    }

    public static float[] getChampionIdArray(int champAmount)
    {

        float[] championArray = new float[champAmount];
        try {
            String filePath = GetOSIDPath();
            FileReader fr = new FileReader(filePath + "championIds");
            BufferedReader br = new BufferedReader(fr);
            String line;
            int lineNumber = 0;
            while((line = br.readLine()) !=null)
            {
                championArray[lineNumber] = Integer.parseInt(line);
                lineNumber++;
            }
            br.close();
        }
        catch (FileNotFoundException FNFE)
        {
            FNFE.printStackTrace();
        }
        catch (IOException IOE)
        {
            IOE.printStackTrace();
        }
        return championArray;
    }

//    public static double GetRandomDouble()
//    {
//        return rand.nextDouble()*0.0;
//    }
    public static int GetRandomInt(int range)
    {
        return rand.nextInt(range);
    }

    public static double GetRandomDouble() { return rand.nextDouble();  }
    public static double GetActualRandomDouble() { return rand.nextDouble();  }
    //Interval of Dynamic Components in minutes
    private static int intervalM = 10;

    public static ArrayList<DynamicComponent> getDynamicComponents(MatchDetail matcheDetail,int userId, StaticComponent sc){
        ArrayList<DynamicComponent> dynamicComponents = new ArrayList<>();

        //userId++;

        //Input minutes in ms
        int intervalMS = intervalM * 60000;
        int currentMs = intervalMS;

        //Values for keeping track of stackable items
        int userHealthPotionStack = 0;
        long healthPotionItemId = 2003;
        long biscuitItemId = 2010;
        int userVisionWardStack = 0;
        long visionWardItemId = 2043;

        //No new dynamic components are created after this time
        int maxDynamicComponentTime = 3600000;

        //Values for making sure that the right participantId is chosen when adding items to enemyItemList
        int lastIdOfTeamOne = 5;
        int teamTwoIds = 6;
        int teamOneIds = 1;

        ArrayList<Integer> enemyHealthPotionStacks = new ArrayList<>(Arrays.asList(0,0,0,0,0));
        ArrayList<Integer> enemyVisionWardStacks = new ArrayList<>(Arrays.asList(0,0,0,0,0));

        List<Frame> frames = matcheDetail.getTimeline().getFrames();

        DynamicComponent newDynamicComponent = new DynamicComponent(0, matcheDetail, userId);

        newDynamicComponent.clearDynamicComponent();

        for (Frame f : frames){
                //for (Event e : f.getEvents()){
                    for (int i = 0; i < f.getEvents().size(); i++) {
                        Event e = f.getEvents().get(i);

                        if (currentMs > maxDynamicComponentTime){ //No more dynamic components are made after maxDynamicComponentTime has passed
                            return dynamicComponents;
                        }

                    if (e.getTimestamp() < currentMs){
                        if (e.getEventType().equals("ITEM_PURCHASED")){
                            if (e.getParticipantId() > lastIdOfTeamOne && userId <= lastIdOfTeamOne && newDynamicComponent.getEnemyItemList().containsKey(sc.getEnemyChampionIds().get((int)e.getParticipantId()-teamTwoIds))){
                                if (e.getItemId() == healthPotionItemId || e.getItemId() == biscuitItemId){
                                    enemyHealthPotionStacks.set((int)e.getParticipantId()-teamTwoIds, enemyHealthPotionStacks.get((int)e.getParticipantId()-6)+1);
                                    if (!newDynamicComponent.getEnemyItemList().get(sc.getEnemyChampionIds().get((int)e.getParticipantId()-teamTwoIds)).contains(healthPotionItemId) &&
                                            !newDynamicComponent.getEnemyItemList().get(sc.getEnemyChampionIds().get((int)e.getParticipantId()-teamTwoIds)).contains(biscuitItemId)){
                                        newDynamicComponent.getEnemyItemList().get(sc.getEnemyChampionIds().get((int) e.getParticipantId() - teamTwoIds)).add(e.getItemId());
                                    }
                                }
                                else if (e.getItemId() == visionWardItemId){
                                    enemyVisionWardStacks.set((int)e.getParticipantId()-6, enemyVisionWardStacks.get((int)e.getParticipantId()-6)+1);
                                    if (!newDynamicComponent.getEnemyItemList().get(sc.getEnemyChampionIds().get((int)e.getParticipantId()-teamTwoIds)).contains(visionWardItemId)){
                                        newDynamicComponent.getEnemyItemList().get(sc.getEnemyChampionIds().get((int) e.getParticipantId()-teamTwoIds)).add(e.getItemId());
                                    }
                                }
                                else {
                                    newDynamicComponent.getEnemyItemList().get(sc.getEnemyChampionIds().get((int) e.getParticipantId() - teamTwoIds)).add(e.getItemId());
                                }
                            }
                            else if (e.getParticipantId() != 0 && e.getParticipantId() <= lastIdOfTeamOne && userId > lastIdOfTeamOne && newDynamicComponent.getEnemyItemList().containsKey(sc.getEnemyChampionIds().get((int)e.getParticipantId()-teamOneIds))){
                                if (e.getItemId() == healthPotionItemId  || e.getItemId() == biscuitItemId){ //If it is a health potion, give +1 to number of health potions for that player
                                    enemyHealthPotionStacks.set((int)e.getParticipantId()-1, enemyHealthPotionStacks.get((int)e.getParticipantId()-1)+1);
                                    if (!newDynamicComponent.getEnemyItemList().get(sc.getEnemyChampionIds().get((int)e.getParticipantId()-teamOneIds)).contains(healthPotionItemId) &&
                                            !newDynamicComponent.getEnemyItemList().get(sc.getEnemyChampionIds().get((int)e.getParticipantId()-teamOneIds)).contains(biscuitItemId)){ //Add health potion if it is not on the list
                                        newDynamicComponent.getEnemyItemList().get(sc.getEnemyChampionIds().get((int)e.getParticipantId()-teamOneIds)).add(e.getItemId());
                                    }
                                }
                                else if (e.getItemId() == visionWardItemId){
                                    enemyVisionWardStacks.set((int)e.getParticipantId()-1, enemyVisionWardStacks.get((int)e.getParticipantId()-1)+1);
                                    if (!newDynamicComponent.getEnemyItemList().get(sc.getEnemyChampionIds().get((int)e.getParticipantId()-teamOneIds)).contains(visionWardItemId)){
                                        newDynamicComponent.getEnemyItemList().get(sc.getEnemyChampionIds().get((int) e.getParticipantId()-teamOneIds)).add(e.getItemId());
                                    }
                                }
                                else {
                                    newDynamicComponent.getEnemyItemList().get(sc.getEnemyChampionIds().get((int)e.getParticipantId()-1)).add(e.getItemId());
                                }
                            }
                            else if (e.getParticipantId() == newDynamicComponent.getUserId()+1) {
                                newDynamicComponent.getUserItemList().add((e.getItemId()));
                            }
                        }
                        else if (e.getEventType().equals("ITEM_DESTROYED") ||  e.getEventType().equals("ITEM_SOLD")){

                            if (e.getParticipantId() > lastIdOfTeamOne && userId <= lastIdOfTeamOne && newDynamicComponent.getEnemyItemList().containsKey(sc.getEnemyChampionIds().get((int)e.getParticipantId()-teamTwoIds))){
                                if (e.getItemId() == healthPotionItemId || e.getItemId() == biscuitItemId){
                                    enemyHealthPotionStacks.set((int)e.getParticipantId()-6, enemyHealthPotionStacks.get((int)e.getParticipantId()-6)-1);
                                    if (enemyHealthPotionStacks.get((int)e.getParticipantId()-6) <= 0){
                                        newDynamicComponent.getEnemyItemList().get(sc.getEnemyChampionIds().get((int) e.getParticipantId() - teamTwoIds)).remove(e.getItemId());
                                    }
                                }
                                else if (e.getItemId() == visionWardItemId){
                                    enemyVisionWardStacks.set((int)e.getParticipantId()-6, enemyVisionWardStacks.get((int)e.getParticipantId()-6)-1);
                                    if (enemyVisionWardStacks.get((int)e.getParticipantId()-6) <= 0){
                                        newDynamicComponent.getEnemyItemList().get(sc.getEnemyChampionIds().get((int) e.getParticipantId() - teamTwoIds)).remove(e.getItemId());
                                    }
                                }
                                else {
                                    newDynamicComponent.getEnemyItemList().get(sc.getEnemyChampionIds().get((int) e.getParticipantId() - teamTwoIds)).remove(e.getItemId());
                                }
                            }
                            else if (e.getParticipantId() != 0 && e.getParticipantId() <= lastIdOfTeamOne && userId > lastIdOfTeamOne && newDynamicComponent.getEnemyItemList().containsKey(sc.getEnemyChampionIds().get((int)e.getParticipantId()-teamOneIds))){
                                if (e.getItemId() == healthPotionItemId || e.getItemId() == biscuitItemId){
                                    enemyHealthPotionStacks.set((int)e.getParticipantId()-1, enemyHealthPotionStacks.get((int)e.getParticipantId()-1)-1);
                                    if (enemyHealthPotionStacks.get((int)e.getParticipantId()-1) <= 0){
                                        newDynamicComponent.getEnemyItemList().get(sc.getEnemyChampionIds().get((int) e.getParticipantId()-teamOneIds)).remove(e.getItemId());
                                    }
                                }
                                else if (e.getItemId() == visionWardItemId){
                                    enemyVisionWardStacks.set((int)e.getParticipantId()-1, enemyVisionWardStacks.get((int)e.getParticipantId()-1)-1);
                                    if (enemyVisionWardStacks.get((int)e.getParticipantId()-1) <= 0){
                                        newDynamicComponent.getEnemyItemList().get(sc.getEnemyChampionIds().get((int) e.getParticipantId() - teamOneIds)).remove(e.getItemId());
                                    }
                                }
                                else {
                                    newDynamicComponent.getEnemyItemList().get(sc.getEnemyChampionIds().get((int) e.getParticipantId() - teamOneIds)).remove(e.getItemId());
                                }
                            }
                            else if (e.getParticipantId() == newDynamicComponent.getUserId()+1) {
                                newDynamicComponent.getUserItemList().remove((e.getItemId()));
                            }

                        }
                        else if (e.getEventType().equals("ITEM_UNDO")){
                            if (e.getParticipantId() > lastIdOfTeamOne && userId <= lastIdOfTeamOne && newDynamicComponent.getEnemyItemList().containsKey(sc.getEnemyChampionIds().get((int)e.getParticipantId()-teamTwoIds))){
                                if (e.getItemAfter() != 0) {
                                Collections.replaceAll(newDynamicComponent.getEnemyItemList().get(sc.getEnemyChampionIds().get((int)e.getParticipantId()-teamTwoIds)), e.getItemBefore(),(long) e.getItemAfter());
                                }
                                else {
                                    newDynamicComponent.getEnemyItemList().get(sc.getEnemyChampionIds().get((int)e.getParticipantId()-teamTwoIds)).remove(e.getItemBefore());
                                }
                            }
                            else if (e.getParticipantId() != 0 && e.getParticipantId() <= lastIdOfTeamOne && userId > lastIdOfTeamOne && newDynamicComponent.getEnemyItemList().containsKey(sc.getEnemyChampionIds().get((int)e.getParticipantId()-teamOneIds))){
                                if (e.getItemAfter() != 0) {
                                    Collections.replaceAll(newDynamicComponent.getEnemyItemList().get(sc.getEnemyChampionIds().get((int)e.getParticipantId()-teamOneIds)), e.getItemBefore(),(long) e.getItemAfter());
                                }
                                newDynamicComponent.getEnemyItemList().get(sc.getEnemyChampionIds().get((int)e.getParticipantId()-teamOneIds)).remove(e.getItemBefore());
                            }
                            else if (e.getParticipantId() == newDynamicComponent.getUserId()+1) {
                                if (e.getItemAfter() != 0) {
                                    Collections.replaceAll(newDynamicComponent.getUserItemList(), e.getItemBefore(),e.getItemAfter());
                                }
                                else {
                                    newDynamicComponent.getUserItemList().remove(e.getItemBefore());
                                }
                            }

                        }
                        else if (e.getEventType().equals("ELITE_MONSTER_KILL")) {
                            if ((e.getKillerId() > lastIdOfTeamOne && userId <= lastIdOfTeamOne) || (e.getKillerId() <= lastIdOfTeamOne && userId > lastIdOfTeamOne)) {
                                if(e.getMonsterType().equals("BARON_NASHOR")) {
                                    newDynamicComponent.enemyBaronKills++;
                                }
                                else if (e.getMonsterType().equals("RIFTHERALD")) {
                                    newDynamicComponent.enemyHeraldKills++;
                                }
                                else if (e.getMonsterType().equals("DRAGON")){
                                    newDynamicComponent.enemyDragonsKills++;
                                }
                            }
                            else {
                                if(e.getMonsterType().equals("BARON_NASHOR")) {
                                    newDynamicComponent.userBaronKills++;
                                }
                                else if (e.getMonsterType().equals("RIFTHERALD")) {
                                    newDynamicComponent.userHeraldKills++;
                                }
                                else if (e.getMonsterType().equals("DRAGON")){
                                    newDynamicComponent.userDragonsKills++;
                                }
                            }
                        }
                        else if(e.getEventType().equals("BUILDING_KILL")){
                            if (e.getTowerType().equals("BASE_TURRET") || e.getTowerType().equals("INNER_TURRET") ||
                                    e.getTowerType().equals("NEXUS_TURRET") || e.getTowerType().equals("OUTER_TURRET")){
                                if ((e.getTeamId() == 200 && userId <= lastIdOfTeamOne) || (e.getTeamId() == 100 && userId > lastIdOfTeamOne)) {
                                    newDynamicComponent.userTurretsDestroyed++;
                                }
                                else {
                                    newDynamicComponent.enemyTurretsDestroyed++;
                                }
                            }
                            if (e.getTowerType().equals("UNDEFINED_TURRET")){
                                if ((e.getTeamId() == 200 && userId <= lastIdOfTeamOne) || (e.getTeamId() == 100 && userId > lastIdOfTeamOne)) {
                                    newDynamicComponent.userInhibitorsDestroyed++;
                                }
                                else {
                                    newDynamicComponent.enemyInhibitorsDestroyed++;
                                }
                            }
                        }
                    }
                    else {
                        dynamicComponents.add(new DynamicComponent(newDynamicComponent,0));
                        newDynamicComponent.setId(newDynamicComponent.getId()+1);
                        currentMs = currentMs + intervalMS;
                        i--;
                    }
                }
        }
        dynamicComponents.add(new DynamicComponent(newDynamicComponent,0));
        return dynamicComponents;
    }
    public static Map<Integer,List<Integer>> GetItemRelationMap(String path)
    {
        Map<Integer,List<Integer>> returnMap = new HashMap<>();
        try {
            FileReader fr = new FileReader(path);
            BufferedReader br = new BufferedReader(fr);

            String line;
            while((line = br.readLine()) !=null)
            {
                List<Integer> itemRelationList = new ArrayList<>();
                int itemId;

                Scanner scanner = new Scanner(line);
                scanner.useDelimiter(",");
                itemId = scanner.nextInt();


                while(scanner.hasNextInt())
                {
                    itemRelationList.add(scanner.nextInt());
                }
                returnMap.put(itemId,itemRelationList);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return returnMap;
    }
    public static void Nullify(Map<Long,Double> md)
    {
        for(Map.Entry<Long,Double> entry : md.entrySet())
        {
            entry.setValue(0.0);
        }
    }
    public static String GetOSIDPath()
    {
        String osPath = "";
        String SystemName = System.getProperty("os.name");
        if(SystemName.toLowerCase().contains("mac"))
        {
            osPath = "/Users/Ulf/dat9/flink-java-project/";
        }
        else if(SystemName.toLowerCase().contains("windows"))
        {
            osPath= "C:\\Users\\sygob\\Documents\\dat9\\flink-java-project\\";
        }
        return osPath;
    }
    public static String GetOSDataPath()
    {
        String osPath = "";
        String SystemName = System.getProperty("os.name");
        if(SystemName.toLowerCase().contains("mac"))
        {
            osPath = "/Users/Ulf/data/";
        }
        else if(SystemName.toLowerCase().contains("windows"))
        {
            osPath= "D:\\Data\\";
        }
        return osPath;
    }
}
