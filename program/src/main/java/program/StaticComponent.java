package program;

import program.classes.MatchDetail;
import program.classes.Participant;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Ulf on 23/12/15.
 */
public class StaticComponent {
    private long userChampionId;
    private List<Long> enemyChampionIds;
    private String userRank;
    private List<String> enemyRanks;
    private Boolean userWinner;
    private List<Long> itemList;
    private Map<Long, List<Long>> enemyItemList;

    public float[] getItemArrayWithId()
    {
        float[] itemArray = new float[7];
        itemArray[0] = userChampionId;
        int i = 1;
        for(Long item : itemList)
        {
            float itemFloat = (float)item;
            itemArray[i] = itemFloat;
            i++;
        }
        return itemArray;
    }

    public StaticComponent(StaticComponent sc)
    {
        this.userChampionId = sc.getUserChampionId();
        this.enemyChampionIds = sc.getEnemyChampionIds();
        this.userRank = sc.getUserRank();
        this.enemyRanks = sc.getEnemyRanks();
        this.userWinner = sc.isUserWinner();
        this.itemList = sc.getItemList();
        this.enemyItemList = sc.getEnemyItemList();
    }

    public StaticComponent(MatchDetail md, int userId)
    {
        Participant user = md.getParticipants().get(userId);
        userChampionId = user.getChampionId();
        userRank = user.getHighestAchievedSeasonTier();
        long enemyStartId = (userId < 6-1) ? 5-1 : 0;
        
        enemyChampionIds = new ArrayList<>();
        enemyRanks = new ArrayList<>();

        for (long i = enemyStartId; i < enemyStartId + 5; i++)
        {
            enemyChampionIds.add(md.getParticipants().get((int) (long) i).getChampionId());
            enemyRanks.add(md.getParticipants().get((int)(long)i).getHighestAchievedSeasonTier());
        }

        userWinner = user.getStats().isWinner();
        itemList = new ArrayList<>();
        //OBS: Check for null when using itemList
        for(long item : user.getStats().getItems())
        {
            itemList.add(item);
        }
        itemList.remove(itemList.size()-1);

        enemyItemList = new HashMap<>();
        for (long i = enemyStartId; i < enemyStartId + 5; i++)
        {
            long enId = md.getParticipants().get((int)i).getChampionId();
            List<Long> enItems = md.getParticipants().get((int)i).getStats().getItems();
            enemyItemList.put(enId,enItems);
        }
    }

    public long getUserChampionId() {
        return userChampionId;
    }

    public List<Long> getEnemyChampionIds() {
        return enemyChampionIds;
    }

    public String getUserRank() {
        return userRank;
    }

    public List<String> getEnemyRanks() {
        return enemyRanks;
    }

    public Boolean isUserWinner() {
        return userWinner;
    }

    public List<Long> getItemList() {
        return itemList;
    }

    public void setItemList(List<Long> itemList) {
        this.itemList = itemList;
    }
    public Map<Long, List<Long>> getEnemyItemList() {
        return enemyItemList;
    }
}
