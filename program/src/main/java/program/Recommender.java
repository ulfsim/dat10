package program;

import javafx.beans.binding.MapBinding;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Ulf on 21/12/15.
 */
public class Recommender {

    //Set to true if using stochastic gradient descent
    boolean stochastic;
    Map<Long,ThetaWeights> tempItemRatings;
    Map<Long,Integer> itemFreqMap;

    int itemListSizeWithId = 7;
    double alpha = 0.001;
    double currentCostValue = 0.0;
    int currentCostAmount = 0;
    double totalCost = 0.0;
    double totalAmount = 0.0;
    double prevTotalCalc = 2.0;
    double regularization = 0.001;
    int printNow = 0;

    Map<Long,ItemFeatureVector> itemVectors;

    public Recommender() {
        stochastic = false;
        itemVectors = new HashMap<>();
        Map<Long,Double> itemMap = Utility.getItemIdMap(true);
        tempItemRatings = new HashMap<>();
        itemFreqMap = new HashMap<>();
        //Initialize temp ratings and item frequency map for gradient descent
        for(Long id : itemMap.keySet())
        {
            tempItemRatings.put(id,new ThetaWeights(true));
            itemFreqMap.put(id,0);
        }

        for (Map.Entry<Long, Double> entry : itemMap.entrySet()) {
            Long key = entry.getKey();
            int intKey = (int) (long) key;
            itemVectors.put(key, new ItemFeatureVector(intKey));
        }
    }
    public void minimize(Match match)
    {
        for (Map.Entry<Long,ItemFeatureVector> entry : itemVectors.entrySet())
        {
            updateVector(entry.getValue(), match);
        }
    }
    public List<float[]> GPUArrays()
    {
        ArrayList<float[]> itemRatingArrays = new ArrayList<>();
        for(ItemFeatureVector ifv : itemVectors.values())
        {
            itemRatingArrays.add(ifv.getThetaWeights().getChampionWeightsAsFloatArray());
        }
        return itemRatingArrays;
    }
    public float[] matchAsArray(Match match)
    {
        List<StaticComponent> componentsList = new ArrayList<>();
        for(StaticComponent sc : match.getComponentsMap().keySet())
        {
            componentsList.add(sc);
        }

        float[] matchArray = new float[componentsList.size()*itemListSizeWithId];
        for(int i = 0; i < componentsList.size(); i++)
        {
            float[] tempArray = componentsList.get(i).getItemArrayWithId();
            for(int j = 0; j < tempArray.length; j++)
            {
                matchArray[i*j] = tempArray[j];
            }
        }
        return matchArray;
    }
    public void staticMinimize(Match match)
    {
        Map<StaticComponent,List<DynamicComponent>> componentListMap = match.getComponentsMap();
        List<Long> itemList;

        for (Map.Entry<StaticComponent,List<DynamicComponent>> entry : componentListMap.entrySet()) {
            StaticComponent sc = entry.getKey();
            itemList = sc.getItemList();
            for(Long itemId : itemList)
            {
                if (sc.isUserWinner()) {
                    if(itemId != 0) {
                        staticUpdateVector(itemVectors.get(itemId), sc, 1.0);
                        /*currentCostValue += calcSingleItem(sc, itemId);
                        currentCostAmount++;*/
                    }
                }
                else
                {
                    if(itemId != 0) {
                        staticUpdateVector(itemVectors.get(itemId), sc, 0.0);
                        /*currentCostValue += calcSingleItem(sc, itemId);
                        currentCostAmount++;*/
                    }
                }
            }
        }
        /*for (Map.Entry<Long,ItemFeatureVector> entry : itemVectors.entrySet())
        {
            staticUpdateVector(entry.getValue(), match);
        }*/
    }
    private double updateParam(double currentTheta, double currentRating , double featureWeight, double actualRating)
    {
        double newWeight = currentTheta - ((alpha*(currentRating-actualRating)*featureWeight));
        //if(newWeight < 0)
        //    newWeight = 0.0;
        return newWeight;
    }

    private void updateVector(ItemFeatureVector vector,Match match){
        ThetaWeights weights = vector.getThetaWeights();
        Map<StaticComponent,List<DynamicComponent>> componentListMap = match.getComponentsMap();
        List<Long> itemList;

        for (int i = 0; i < componentListMap.size(); i++)
        {
            for (Map.Entry<StaticComponent,List<DynamicComponent>> entry : componentListMap.entrySet()) {
                StaticComponent sc = entry.getKey();

                for (DynamicComponent dc : entry.getValue()) {
                    itemList = dc.getUserItemList();

                    if (sc.isUserWinner()) {
                        if (itemList.contains((long) vector.getItemId())) {
                            multiUpdate(weights, dc, itemList, sc, 1.0, 1.0);
                        }
                    } else {
                        if (itemList.contains((long) vector.getItemId())) {
                            multiUpdate(weights, dc, itemList, sc, 1.0, 0);
                        }
                    }
                }
            }
        }
    }
    private void staticUpdateVector(ItemFeatureVector vector,StaticComponent sc, double actualRating){
        vector.getItemUsageMap().put(sc.getUserChampionId(),vector.getItemUsageMap().get(sc.getUserChampionId())+1.0);
        staticMultiUpdate(vector,sc,1.0,actualRating);
    }
    private void multiUpdate(ThetaWeights weights, DynamicComponent dc, List<Long> itemList, StaticComponent sc, double featureWeights, double actualRating)
    {
        double currentRating = this.itemRating(weights, sc, dc);
        weights.setUserBaronWeight(updateParam(weights.userBaronWeight,currentRating, dc.userBaronKills, actualRating));
        weights.setEnemyBaronWeight(updateParam(weights.enemyBaronWeight,currentRating,dc.enemyBaronKills, actualRating));
        weights.setUserHeraldWeight(updateParam(weights.userHeraldWeight,currentRating,dc.userHeraldKills, actualRating));
        weights.setEnemyHeraldWeight(updateParam(weights.enemyHeraldWeight,currentRating ,dc.enemyHeraldKills, actualRating));
        weights.setUserTurretWeight(updateParam(weights.userTurretWeight,currentRating ,dc.userTurretsDestroyed, actualRating));
        weights.setEnemyTurretWeight(updateParam(weights.enemyTurretWeight, currentRating,dc.enemyTurretsDestroyed, actualRating));
        weights.setUserInhibitorWeight(updateParam(weights.userInhibitorWeight,currentRating ,dc.userInhibitorsDestroyed, actualRating));
        weights.setEnemyTurretWeight(updateParam(weights.enemyTurretWeight,currentRating,dc.enemyTurretsDestroyed,actualRating));

        for(long item: itemList)
        {
            if(item != 0) {
                weights.userItemWeights.put(item, updateParam(weights.userItemWeights.get(item),currentRating, featureWeights, actualRating));
            }
        }
        long userChampId = sc.getUserChampionId();
        weights.userChampionWeights.put(userChampId, updateParam(weights.userChampionWeights.get(userChampId), currentRating,featureWeights, actualRating));

        List<Long> enemyChampions = sc.getEnemyChampionIds();
        for(long champ : enemyChampions)
        {
            weights.enemyChampionWeights.put(champ,updateParam(weights.enemyChampionWeights.get(champ), currentRating, featureWeights, actualRating));
        }
        Map<Long,Map<Long,Double>> enemyItemWeights = weights.getEnemyItemWeights();
        for (Map.Entry<Long,List<Long>> entry : dc.getEnemyItemList().entrySet()){
            Map<Long,Double> currentMap = enemyItemWeights.get(entry.getKey());
            for (long item : entry.getValue()){
                if (item != 0){
                    currentMap.put(item, updateParam(currentMap.get(item),currentRating,featureWeights,actualRating));
                }
            }
        }
    }
    private void staticMultiUpdate(ItemFeatureVector vector, StaticComponent sc, double featureWeights, double actualRating)
    {
        if(stochastic){
            ThetaWeights weights = vector.getThetaWeights();
            double currentRating = this.staticItemRating(weights, sc);
            /*
            for(long item: sc.getItemList())
            {
                if(item != 0) {
                    weights.userItemWeights.put(item, updateParam(weights.userItemWeights.get(item),currentRating, featureWeights, actualRating));
                }
            }*/
            long userChampId = sc.getUserChampionId();
            weights.userChampionWeights.put(userChampId, updateParam(weights.userChampionWeights.get(userChampId), currentRating,featureWeights, actualRating));
            /*
            List<Long> enemyChampions = sc.getEnemyChampionIds();
            for(long champ : enemyChampions)
            {
                weights.enemyChampionWeights.put(champ,updateParam(weights.enemyChampionWeights.get(champ), currentRating, featureWeights, actualRating));
            }
            Map<Long,Map<Long,Double>> enemyItemWeights = weights.getEnemyItemWeights();
            for (Map.Entry<Long,List<Long>> entry : sc.getEnemyItemList().entrySet()){
                Map<Long,Double> currentMap = enemyItemWeights.get(entry.getKey());
                for (long item : entry.getValue()){
                    if (item != 0){
                        currentMap.put(item, updateParam(currentMap.get(item),currentRating,featureWeights,actualRating));
                    }
                }
            }*/
        }
        else
        {
            long userChampId = sc.getUserChampionId();
            long id = vector.getItemId();
            int currentFreq = itemFreqMap.get(id);
            itemFreqMap.put(id,currentFreq + 1);
            double currentRating = tempItemRatings.get(id).userChampionWeights.get(userChampId);
            tempItemRatings.get(id).userChampionWeights.put(userChampId,++currentRating);
        }

    }
    private double costFunction(double predictedRating, double actualRating)
    {
        return Math.pow(predictedRating - actualRating, 2);
    }
    public double calcCostFunction(StaticComponent sc){
        double currentCost = 0.0;
        double actualRating;
        for (Map.Entry<Long,ItemFeatureVector> itemVector : itemVectors.entrySet()) {
            ItemFeatureVector currentVector = itemVector.getValue();
            double champUsage =  itemVector.getValue().getItemUsageMap().get(sc.getUserChampionId());
            if (sc.isUserWinner() && sc.getItemList().contains((long)currentVector.getItemId()))
            {
                actualRating = 1.0;
                currentCost += costFunction(staticItemRating(currentVector.getThetaWeights(),sc),actualRating);
            }
            else if(sc.getItemList().contains((long)currentVector.getItemId()))
            {
                actualRating = 0.0;
                currentCost += costFunction(staticItemRating(currentVector.getThetaWeights(),sc),actualRating);
            }
        }
        return currentCost;
    }
    private double calcSingleItem(StaticComponent sc, Long itemId)
    {
        double currentCost = 0.0;
        double actualRating;
        ItemFeatureVector currentVector = itemVectors.get(itemId);
        double champUsage =  currentVector.getItemUsageMap().get(sc.getUserChampionId());
        if (sc.isUserWinner() && sc.getItemList().contains((long)currentVector.getItemId()))
        {
            actualRating = 1.0;
            currentCost += costFunction(staticItemRating(currentVector.getThetaWeights(),sc),actualRating);
        }
        else if(sc.getItemList().contains((long)currentVector.getItemId()))
        {
            actualRating = 0.0;
            currentCost += costFunction(staticItemRating(currentVector.getThetaWeights(),sc),actualRating);
        }
        return currentCost;
    }

    private double itemRating(ThetaWeights weights, StaticComponent sc, DynamicComponent dc)
    {
        double rating = 0.0;
        /* Commented out for testing 25/7/16
        rating += weights.getEnemyBaronWeight() *dc.getEnemyBaronKills();
        rating += weights.getEnemyDragonWeight() * dc.getEnemyDragonsKills();
        rating += weights.getEnemyHeraldWeight() * dc.getEnemyHeraldKills();
        rating += weights.getEnemyInhibitorWeight() * dc.getEnemyInhibitorsDestroyed();

        Map<Long,Map<Long,Double>> enemyItemWeights = weights.getEnemyItemWeights();
        for (Map.Entry<Long,List<Long>> entry: dc.getEnemyItemList().entrySet())
        {
            Map<Long,Double> currentMap = enemyItemWeights.get(entry.getKey());
            for (long item : entry.getValue())
            {
                if(item != 0)
                    rating += currentMap.get(item);
            }
        }

        for (long item : dc.getUserItemList()) {
            if(item != 0)
                rating += weights.getUserItemWeights().get(item);
        }
        rating += weights.getUserBaronWeight() * dc.getUserBaronKills();
        rating += weights.getUserDragonWeight() * dc.getUserDragonsKills();
        rating += weights.getUserHeraldWeight() * dc.getUserHeraldKills();
        rating += weights.getUserInhibitorWeight() * dc.getUserInhibitorsDestroyed();

        Map<Long,Double> enemyChampionWeights = weights.getEnemyChampionWeights();
        for (long champion : sc.getEnemyChampionIds())
        {
            rating += enemyChampionWeights.get(champion);
        }*/
        rating += weights.getUserChampionWeights().get(sc.getUserChampionId());

        return rating;
    }
    public double staticItemRating(ThetaWeights weights, StaticComponent sc)
    {
        int addCount = 0;
        double rating = 0.0;

        /* commented out for testing 25/7/16
        Map<Long,Map<Long,Double>> enemyItemWeights = weights.getEnemyItemWeights();
        for (Map.Entry<Long,List<Long>> entry: sc.getEnemyItemList().entrySet())
        {
            Map<Long,Double> currentMap = enemyItemWeights.get(entry.getKey());
            for (long item : entry.getValue())
            {
                if(item != 0) {
                    rating += currentMap.get(item);
                    addCount++;
                }
            }
        }

        for (long item : sc.getItemList()) {
            if(item != 0) {
                rating += weights.getUserItemWeights().get(item);
                addCount++;
            }
        }


        Map<Long,Double> enemyChampionWeights = weights.getEnemyChampionWeights();
        for (long champion : sc.getEnemyChampionIds())
        {
            rating += enemyChampionWeights.get(champion);
            addCount++;
        }*/
        rating += weights.getUserChampionWeights().get(sc.getUserChampionId());
        addCount += 1;

        return rating/addCount;
    }
    public double normalizedStaticItemRating(ThetaWeights weights, StaticComponent sc, double champAmount, double champTotal)
    {
        double rating = staticItemRating(weights,sc);
        if(champAmount !=0)
            return rating;
        else
            return 0;
    }
    public void PrintCostFunction()
    {
        double currentCalc = currentCostValue / currentCostAmount;
        //System.out.println("Current Batch: " + currentCalc);
        totalCost += currentCalc;
        totalAmount++;
        currentCostAmount = 0;
        currentCostValue = 0.0;
    }
    public double ResetBatchParameter()
    {
        double totalCalc = totalCost / totalAmount;
        System.out.print("Current Total: " +  totalCalc);
        double checkCalc = prevTotalCalc - totalCalc;
        System.out.println(" Current Difference: " + (checkCalc));
        prevTotalCalc = totalCalc;
        totalCost = 0.0;
        totalAmount = 0.0;
        return checkCalc;
    }
    private boolean checkNaN(double checkvalue)
    {
        return checkvalue == checkvalue;
    }
    public Map<Long, ItemFeatureVector> getItemVectors() {
        return itemVectors;
    }
    public double getAlpha() {
        return alpha;
    }

    public void setAlpha(double alpha) {
        this.alpha = alpha;
    }

    public Map<Long, ThetaWeights> getTempItemRatings() {
        return tempItemRatings;
    }

    public Map<Long, Integer> getItemFreqMap() {
        return itemFreqMap;
    }
}