package program;

import java.io.*;

import com.sun.org.apache.xerces.internal.impl.xpath.regex.Match;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import program.classes.MatchDetail;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.channels.FileLock;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Ulf on 14/12/15.
 */
public class DataReader {
    List<MatchDetail> currentDataBlock;
    int numOfFiles;
    ArrayList<Integer> brFileNumbers;
    ArrayList<Integer> euneFileNumbers;
    ArrayList<Integer> euwFileNumbers;
    ArrayList<Integer> krFileNumbers;
    ArrayList<Integer> lasFileNumbers;
    ArrayList<Integer> naFileNumbers;
    ArrayList<Integer> oceFileNumbers;
    ArrayList<Integer> ruFileNumbers;

    public DataReader(int numOfFiles)
    {
        this.numOfFiles = numOfFiles;
        brFileNumbers = new ArrayList<>();
        euneFileNumbers = new ArrayList<>();
        euwFileNumbers = new ArrayList<>();
        krFileNumbers = new ArrayList<>();
        lasFileNumbers = new ArrayList<>();
        naFileNumbers = new ArrayList<>();
        oceFileNumbers = new ArrayList<>();
        ruFileNumbers = new ArrayList<>();
        for(int i = 0; i < numOfFiles; i++)
        {
            brFileNumbers.add(i);
            euneFileNumbers.add(i);
            euwFileNumbers.add(i);
            krFileNumbers.add(i);
            lasFileNumbers.add(i);
            naFileNumbers.add(i);
            oceFileNumbers.add(i);
            ruFileNumbers.add(i);
        }
    }

    public MatchDetail getNextEntry()
    {
        MatchDetail returnMd;
        String region = "";
        Random rand = new Random();
        try{
            if (currentDataBlock.isEmpty())
            {
                int rnd = rand.nextInt(8) + 1;
                switch (rnd)
                {
                    case 1:
                        region = "br";
                        if(!brFileNumbers.isEmpty())
                            break;
                    case 2:
                        region = "eune";
                        if(!euneFileNumbers.isEmpty())
                            break;
                    case 3:
                        region = "euw";
                        if(!euwFileNumbers.isEmpty())
                            break;

                    case 4:
                        region = "kr";
                        if(!krFileNumbers.isEmpty())
                            break;

                    case 5:
                        region = "las";
                        if (!lasFileNumbers.isEmpty())
                            break;

                    case 6:
                        region = "na";
                        if (!naFileNumbers.isEmpty())
                            break;

                    case 7:
                        region = "oce";
                        if  (!oceFileNumbers.isEmpty())
                            break;

                    case 8:
                        region = "ru";
                        if (!ruFileNumbers.isEmpty())
                            break;
                    default:
                        System.err.println("Something went horribly wrong!");
                        System.exit(0);
                }
                currentDataBlock = readDataBlock(this.getFileLocation(region));
            }

        }
        catch(NullPointerException e)
        {
            int rnd = rand.nextInt(8) + 1;
            switch (rnd)
            {
                case 1:
                    region = "br";
                    break;
                case 2:
                    region = "eune";
                    break;

                case 3:
                    region = "euw";
                    break;

                case 4:
                    region = "kr";
                    break;

                case 5:
                    region = "las";
                    break;

                case 6:
                    region = "na";
                    break;

                case 7:
                    region = "oce";
                    break;

                case 8:
                    region = "ru";
                    break;
                default:
                    System.err.println("Something went horribly wrong!");
                    System.exit(0);
            }
            currentDataBlock = readDataBlock(this.getFileLocation(region));
        }
        int index = currentDataBlock.size()-1;
        int rnd;
        if (index > 0)
            rnd = rand.nextInt(index);
        else
            rnd = 0;
        returnMd = currentDataBlock.get(rnd);
        currentDataBlock.remove(rnd);
        return returnMd;

    }
    private String getFileLocation(String region)
    {
        Random rand = new Random();
        int rnd = rand.nextInt(this.getRegionSize(region)) + 1;
        String fileLocation = "/Users/Ulf/data/" + region + "/" + region + rnd;
        File file = new File(fileLocation);
        if(file.exists())
            return fileLocation;
        else {
            if (!brFileNumbers.isEmpty() || !euneFileNumbers.isEmpty()
                    || !euwFileNumbers.isEmpty() || !krFileNumbers.isEmpty()
                    || !lasFileNumbers.isEmpty() || !naFileNumbers.isEmpty()
                    || !oceFileNumbers.isEmpty() || !ruFileNumbers.isEmpty())
            {
                switch (region) {
                    case "br":
                        brFileNumbers.remove(rnd);
                        if (!brFileNumbers.isEmpty()) {
                            fileLocation = getFileLocation(region);
                            break;
                        } else {
                            fileLocation = getFileLocation("eune");
                            break;
                        }
                    case "eune":
                        euneFileNumbers.remove(rnd);
                        if (!euneFileNumbers.isEmpty()) {
                            fileLocation = getFileLocation(region);
                            break;
                        } else {
                            fileLocation = getFileLocation("euw");
                            break;
                        }
                    case "euw":
                        euwFileNumbers.remove(rnd);
                        if (!euwFileNumbers.isEmpty()) {
                            fileLocation = getFileLocation(region);
                            break;
                        } else {
                            fileLocation = getFileLocation("kr");
                            break;
                        }
                    case "kr":
                        krFileNumbers.remove(rnd);
                        if (!krFileNumbers.isEmpty()) {
                            fileLocation = getFileLocation(region);
                            break;
                        } else {
                            fileLocation = getFileLocation("las");
                            break;
                        }
                    case "las":
                        lasFileNumbers.remove(rnd);
                        if (!lasFileNumbers.isEmpty()) {
                            fileLocation = getFileLocation(region);
                            break;
                        } else {
                            fileLocation = getFileLocation("na");
                            break;
                        }
                    case "na":
                        naFileNumbers.remove(rnd);
                        if (!naFileNumbers.isEmpty()) {
                            fileLocation = getFileLocation(region);
                            break;
                        }
                        else {
                            fileLocation = getFileLocation("oce");
                            break;
                        }
                    case "oce":
                        oceFileNumbers.remove(rnd);
                        if(!oceFileNumbers.isEmpty()) {
                            fileLocation = getFileLocation(region);
                            break;
                        }
                        else{
                            fileLocation = getFileLocation("ru");
                        }
                    case "ru":
                        ruFileNumbers.remove(rnd);
                        if(!ruFileNumbers.isEmpty()) {
                            fileLocation = getFileLocation(region);
                            break;
                        }
                        else{
                            fileLocation = getFileLocation("br");
                        }
                    default:
                        System.err.println("Region Not Valid");
                }
            }
            else{
                return null;
            }
        }
        return fileLocation;
    }
    private int getRegionSize(String region)
    {
        int size = 0;
        switch(region)
        {
            case"br":
                size = brFileNumbers.size();
                break;
            case"eune":
                size = euneFileNumbers.size();
                break;
            case"euw":
                size = euwFileNumbers.size();
                break;
            case"kr":
                size = krFileNumbers.size();
                break;
            case"las":
                size = lasFileNumbers.size();
                break;
            case"na":
                size = naFileNumbers.size();
                break;
            case"oce":
                size = oceFileNumbers.size();
                break;
            case"ru":
                size = ruFileNumbers.size();
                break;
            default:
                System.err.println("Region Not Valid!");
        }
        return size;
    }
    private List<MatchDetail> readDataBlock(String fileLocation)
    {
        List<MatchDetail> returnList= new ArrayList<>();
        try{
            FileReader fr = new FileReader(fileLocation);
            BufferedReader br = new BufferedReader(fr);


            String line;
            while ((line = br.readLine())!= null)
            {
                returnList.add(new MatchDetail(line));
            }
        }
        catch (FileNotFoundException FNFE)
        {
            FNFE.printStackTrace();
            return null;
        }
        catch (IOException IOE)
        {
            IOE.printStackTrace();
            return null;
        }
        return returnList;
    }

}
