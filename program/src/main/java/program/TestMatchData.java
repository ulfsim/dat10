package program;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.PriorityQueue;

/**
 * Created by Ulf on 06/02/16.
 */
public class TestMatchData {
    Map<Long, ItemFeatureVector> itemMap;
    boolean isRecommender;

    public TestMatchData(Recommender rm)
    {
        itemMap = rm.getItemVectors();
        isRecommender = true;
    }
    public TestMatchData()
    {

    }
    public TestMatchData(Classifier cm)
    {
        itemMap = cm.getItemVectors();
        isRecommender = false;
    }

    public double getBestItemBuild(Match match, Recommender rm, int itemGets) {
        double totalScore = 0.0;
        int totalAmount = 0;
        //int currentAmount = 0;
        itemMap = rm.getItemVectors();
        //Get the staticComponent of each player
        for (StaticComponent staticComponent : match.getComponentsMap().keySet()) {
            if (staticComponent.isUserWinner())
            {
                StaticComponent sc = new StaticComponent(staticComponent);
                double currentScore = 0.0;
                int count = 0;
                List<Long> itemCheckList = sc.getItemList();
                List<Long> itemList = new ArrayList<>();
                for (int i = 0; i < 6; i++) {
                    Map<Long, Double> itemRatings = new HashMap<>();
                    sc.setItemList(itemList);
                    //Calculate the rating of each item, given the current StaticComponent
                    for (Map.Entry<Long, ItemFeatureVector> entry : itemMap.entrySet()) {
                        //double champUsage = entry.getValue().getItemUsageMap().get(sc.getUserChampionId());
                        //Without Normalization
                        itemRatings.put(entry.getKey(),rm.staticItemRating(entry.getValue().getThetaWeights(), sc));
                        //With Normalization
                        //itemRatings.put(entry.getKey(), rm.normalizedStaticItemRating(entry.getValue().getThetaWeights(), sc, champUsage, entry.getValue().getTotalChampAmount()));
                    }

                    List<Entry<Long, Double>> greatest = findGreatest(itemRatings, itemGets + 6);
                    int extralives = 0;
                    for (int j = greatest.size() - 1; j >= 0; j--) {
                        if (!itemList.contains(greatest.get(j).getKey())) {
                            count++;
                            itemList.add(greatest.get(j).getKey());

                            List<Entry<Long,Double>> littleGreater = new ArrayList<>();
                            for(int l = greatest.size() - 1; l > greatest.size() - itemGets - extralives - 1; l--)
                            {
                                littleGreater.add(greatest.get(l));
                            }

                            outerLoop:
                            for (Entry<Long, Double> checkEntry : littleGreater) {
                                for (int item = 0; item < itemCheckList.size(); item++) {
                                    if (checkEntry.getKey().longValue() == itemCheckList.get(item)) {
                                        currentScore += 1;
                                        itemCheckList.remove(item);
                                        break outerLoop;
                                    }
                                    if(itemCheckList.get(item) == 0L)
                                    {
                                        itemCheckList.remove(item);
                                        item--;
                                    }
                                }
                            }
                            break;
                        }
                    }
                }
                //System.out.println("The current score is:" + currentScore / currentAmount);
                double addScore = currentScore / count;
                if(!Double.isNaN(addScore)) {
                    totalScore += addScore;
                }
                totalAmount++;
            }
        }
        return totalScore/totalAmount;
    }
    public double getRandomItemBuild(Match match,Recommender rm, int itemGets){
        double totalScore = 0.0;
        int totalAmount = 0;
        //int currentAmount = 0;
        itemMap = rm.getItemVectors();
        //Get the staticComponent of each player
        for (StaticComponent staticComponent : match.getComponentsMap().keySet()) {
            if (staticComponent.isUserWinner())
            {
                StaticComponent sc = new StaticComponent(staticComponent);
                double currentScore = 0.0;
                int count = 0;
                List<Long> itemCheckList = sc.getItemList();
                List<Long> itemList = new ArrayList<>();
                for (int i = 0; i < 6; i++) {
                    Map<Long, Double> itemRatings = new HashMap<>();
                    sc.setItemList(itemList);
                    //Calculate the rating of each item, given the current StaticComponent
                    for (Map.Entry<Long, ItemFeatureVector> entry : itemMap.entrySet()) {
                        //double champUsage = entry.getValue().getItemUsageMap().get(sc.getUserChampionId());
                        //Without Normalization
                        itemRatings.put(entry.getKey(),Utility.GetActualRandomDouble());
                        //With Normalization
                        //itemRatings.put(entry.getKey(), rm.normalizedStaticItemRating(entry.getValue().getThetaWeights(), sc, champUsage, entry.getValue().getTotalChampAmount()));
                    }

                    List<Entry<Long, Double>> greatest = findGreatest(itemRatings, itemGets + 6);
                    int extralives = 0;
                    for (int j = greatest.size() - 1; j >= 0; j--) {
                        if (!itemList.contains(greatest.get(j).getKey())) {
                            count++;
                            itemList.add(greatest.get(j).getKey());

                            List<Entry<Long,Double>> littleGreater = new ArrayList<>();
                            for(int l = greatest.size() - 1; l > greatest.size() - itemGets - extralives - 1; l--)
                            {
                                littleGreater.add(greatest.get(l));
                            }
                            outerLoop:
                            for (Entry<Long, Double> checkEntry : littleGreater) {
                                for (int item = 0; item < itemCheckList.size(); item++) {
                                    if (checkEntry.getKey().longValue() == itemCheckList.get(item)) {
                                        currentScore += 1;
                                        itemCheckList.remove(item);
                                        break outerLoop;
                                    }
                                    if(itemCheckList.get(item) == 0L)
                                    {
                                        itemCheckList.remove(item);
                                        item--;
                                    }
                                }
                            }
                            break;
                        }
                        extralives++;
                    }
                }
                //System.out.println("The current score is:" + currentScore / currentAmount);
                double addScore = currentScore / count;
                if(!Double.isNaN(addScore)) {
                    totalScore += addScore;
                }
                totalAmount++;
            }
        }
        return totalScore/totalAmount;
    }

    private static <K, V extends Comparable<? super V>> List<Entry<K, V>>
    findGreatest(Map<K, V> map, int n)
    {
        Comparator<? super Entry<K, V>> comparator =
                new Comparator<Entry<K, V>>()
                {
                    @Override
                    public int compare(Entry<K, V> e0, Entry<K, V> e1)
                    {
                        V v0 = e0.getValue();
                        V v1 = e1.getValue();
                        return v0.compareTo(v1);
                    }
                };
        PriorityQueue<Entry<K, V>> highest =
                new PriorityQueue<Entry<K,V>>(n, comparator);
        for (Entry<K, V> entry : map.entrySet())
        {
            highest.offer(entry);
            while (highest.size() > n)
            {
                highest.poll();
            }
        }

        List<Entry<K, V>> result = new ArrayList<Map.Entry<K,V>>();
        while (highest.size() > 0)
        {
            result.add(highest.poll());
        }
        return result;
    }
}
