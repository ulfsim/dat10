package program;

import program.classes.MatchDetail;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.*;

/**
 * Created by Ulf on 11/02/16.
 */
public class Ranking {
    Map<Long,Double> winItemMap;
    Map<Long,Double> lossItemMap;
    Long championId;
    Long timesPresent;
    Long timesWon;

    public static void main(String[] args) throws Exception {
        WordCount wc = new WordCount();
        Map<Long,Double> champMap = Utility.getChampionIdMap(true);
        Map<Long,Ranking> rankMap = new HashMap<>();
        for (Long champId : champMap.keySet())
        {
            rankMap.put(champId,new Ranking(champId));
        }
        for(int i = 0; i < 36; i++) {
            List<MatchDetail> matchDetails = wc.getMatches("/Users/Ulf/data/eune/eune" + i);
            for (MatchDetail md : matchDetails) {
                Match m = new Match(md);
                for (StaticComponent sc : m.getComponentsMap().keySet())
                {
                    Long userChampId = sc.getUserChampionId();
                    rankMap.get(userChampId).Rank(sc);
                }
            }
            System.out.println(i);
        }
        for (Ranking rank : rankMap.values())
        {
            rank.printRankToFile();
        }
    }
    public Ranking(Long championId)
    {
        this.championId = championId;
        winItemMap = Utility.getItemIdMap(true);
        lossItemMap = Utility.getItemIdMap(true);
        Nullify(winItemMap);
        Nullify(lossItemMap);
        timesPresent = 0L;
        timesWon = 0L;
    }
    private void Rank(StaticComponent sc)
    {
        timesPresent++;
        if(sc.isUserWinner())
        {
            timesWon++;
            for (Long entry : sc.getItemList())
            {
                if (entry != 0)
                    winItemMap.put(entry,winItemMap.get(entry) + 1.0);
            }
        }
        else
        {
            for (Long entry : sc.getItemList())
            {
                if (entry != 0)
                    lossItemMap.put(entry,lossItemMap.get(entry) + 1.0);
            }
        }
    }
    private void Nullify(Map<Long,Double> md)
    {
        for(Map.Entry<Long,Double> entry : md.entrySet())
        {
            entry.setValue(0.0);
        }
    }
    private void printRank()
    {
            System.out.println("Printing for champion: " + this.championId);
            for(Map.Entry<Long,Double> winMapEntry : this.winItemMap.entrySet())
            {
                System.out.println(winMapEntry.getKey() + " " + winMapEntry.getValue() / (winMapEntry.getValue() + lossItemMap.get(winMapEntry.getKey())) + " " + winMapEntry.getValue() + lossItemMap.get(winMapEntry.getKey()));
            }
    }
    private void printRankToFile()
    {
        try {
            File file = new File("/Users/Ulf/dat9/flink-java-project/champions/eune/" + this.championId + ".txt");
            if (!file.exists()) {
                file.createNewFile();
            }
            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            Map<Long,Double> winrateMap = new HashMap<>();
            for (Map.Entry<Long,Double> entry : winItemMap.entrySet())
            {
                winrateMap.put(entry.getKey(),entry.getValue()/(entry.getValue()+lossItemMap.get(entry.getKey())));
            }
            Map<Long,Double> sortedWinrateMap = sortByValue(winrateMap);
            for(Map.Entry<Long,Double> winMapEntry : sortedWinrateMap.entrySet())
            {
                bw.write(winMapEntry.getKey() + " " + (winItemMap.get(winMapEntry.getKey()) + lossItemMap.get(winMapEntry.getKey())) + " " + winItemMap.get(winMapEntry.getKey()) / (winItemMap.get(winMapEntry.getKey()) + lossItemMap.get(winMapEntry.getKey())));
                bw.newLine();
            }
            bw.write("Champion present: " + timesPresent + " times");
            bw.newLine();
            bw.write("Champion winrate: " + (double)timesWon/(double)timesPresent);
            bw.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }
    public static <K, V extends Comparable<? super V>> Map<K, V>
    sortByValue( Map<K, V> map )
    {
        List<Map.Entry<K, V>> list =
                new LinkedList<Map.Entry<K, V>>( map.entrySet() );
        Collections.sort( list, new Comparator<Map.Entry<K, V>>()
        {
            public int compare( Map.Entry<K, V> o1, Map.Entry<K, V> o2 )
            {
                return (o1.getValue()).compareTo( o2.getValue() );
            }
        } );

        Map<K, V> result = new LinkedHashMap<K, V>();
        for (Map.Entry<K, V> entry : list)
        {
            result.put( entry.getKey(), entry.getValue() );
        }
        return result;
    }
}
