package program;

/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import program.classes.MatchDetail;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Implements the "WordCount" program that computes a simple word occurrence histogram
 * over some sample data
 *
 * <p>
 * This example shows how to:
 * <ul>
 * <li>write a simple Flink program.
 * <li>use Tuple data types.
 * <li>write and use user-defined functions.
 * </ul>
 *
 */
public class WordCount {

	//
	//	Program
	//

	public static void main(String[] args) throws Exception {

		WordCount wc = new WordCount();
		List<MatchDetail> matchDetails;
		Recommender rm = new Recommender();

		for(int i = 0; i < 10; i++)
		{
			matchDetails = wc.getMatches("/Users/Ulf/data/ru/ru" + i);
			for (MatchDetail md : matchDetails)
			{
				rm.staticMinimize(new Match(md));
				//System.out.println(currentCost/(double)matchDetails.size());
			}
			System.out.println(i);
		}
		matchDetails = wc.getMatches("/Users/Ulf/data/ru/ru" + 0);

		TestMatchData TMD = new TestMatchData(rm);
		// execute and print result
		//WordCount wc = new WordCount();
		//wc.testGetData();

	}


	public List<MatchDetail> getMatches(String fileLocation)
	{
		List<MatchDetail> returnList= new ArrayList<>();
		try{
			FileReader fr = new FileReader(fileLocation);
			BufferedReader br = new BufferedReader(fr);


			String line;
			while ((line = br.readLine())!= null)
			{
				returnList.add(new MatchDetail(line));
			}
			br.close();
		}
		catch (FileNotFoundException FNFE)
		{
			FNFE.printStackTrace();
			return null;
		}
		catch (IOException IOE)
		{
			IOE.printStackTrace();
			return null;
		}
		return returnList;
	}
	//
	// 	User Functions
	//

}
