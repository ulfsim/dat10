package program.classes;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.ArrayList;

/**
 * Created by Nichlas on 30-11-2015.
 */
public class Frame {
    ArrayList<Event> events;
    long timestamp;

    public Frame(JSONObject frame) {
        timestamp = (long) frame.get("timestamp");
        JSONArray jsonEvents = new JSONArray();

        events = new ArrayList<>();

        jsonEvents = (JSONArray) frame.get("events");

        try {
            for (int i = 0; i < jsonEvents.size(); i++) {
                JSONObject jsonEvent = (JSONObject) jsonEvents.get(i);
                events.add(new Event(jsonEvent));
                //System.out.println(jsonEvent);
            }
        }
        catch (Exception e)
        {

        }
    }

    public ArrayList<Event> getEvents() {
        return events;
    }

    public long getTimestamp() {
        return timestamp;
    }
}
