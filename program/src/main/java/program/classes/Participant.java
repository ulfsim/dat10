package program.classes;

import org.json.simple.JSONObject;

/**
 * Created by Nichlas on 30-11-2015.
 */
public class Participant {
    long championId;
    String highestAchievedSeasonTier;
    long participantId;
    long teamId;
    ParticipantStats stats;


    public long getChampionId() {
        return championId;
    }

    public String getHighestAchievedSeasonTier() {
        return highestAchievedSeasonTier;
    }


    public ParticipantStats getStats() {
        return stats;
    }

    public long getTeamId() {
        return teamId;
    }

    public long getParticipantId() {
        return participantId;
    }

    public Participant(JSONObject participant)
    {
            championId = (long) participant.get("championId");
            highestAchievedSeasonTier = (String) participant.get("highestAchievedSeasonTier");
            participantId = (long) participant.get("participantId");
            teamId = (long) participant.get("teamId");

            //System.out.println("Champion ID:" + championId + " ParticipantID:" + participantId + " TeamID:" + teamId);

            JSONObject pstats = (JSONObject)participant.get("stats");
            stats = new ParticipantStats(pstats);
    }
}
