package program.classes;

import org.json.simple.JSONObject;

/**
 * Created by Nichlas on 30-11-2015.
 */
public class Team {
    long baronKills;
    long dragonKills;
    long inhibitorKills;
    long riftHeraldKills;
    long teamId;
    long towerKills;
    boolean winner;
    public Team(JSONObject team)
    {
        baronKills = (long)team.get("baronKills");
        dragonKills = (long)team.get("dragonKills");
        inhibitorKills = (long)team.get("inhibitorKills");
        riftHeraldKills = (long)team.get("riftHeraldKills");
        teamId = (long)team.get("teamId");
        towerKills = (long)team.get("towerKills");
        winner = (boolean)team.get("winner");
    }

    public long getBaronKills() {
        return baronKills;
    }

    public long getDragonKills() {
        return dragonKills;
    }

    public long getInhibitorKills() {
        return inhibitorKills;
    }

    public long getRiftHeraldKills() {
        return riftHeraldKills;
    }

    public long getTeamId() {
        return teamId;
    }

    public long getTowerKills() {
        return towerKills;
    }

    public boolean isWinner() {
        return winner;
    }
}
