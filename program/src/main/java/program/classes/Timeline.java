package program.classes;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.ArrayList;

/**
 * Created by Nichlas on 30-11-2015.
 */
public class Timeline {
    long frameInterval;
    ArrayList<Frame> frames;
    public Timeline(JSONObject timeline)
    {
        frameInterval = (long)timeline.get("frameInterval");
        frames = new ArrayList<>();
        JSONArray jsonFrames= (JSONArray)timeline.get("frames");
        for (int i = 0; i < jsonFrames.size(); i++)
        {
            JSONObject jsonFrame = (JSONObject)jsonFrames.get(i);
            frames.add(new Frame(jsonFrame));
        }
    }

    public long getFrameInterval() {
        return frameInterval;
    }

    public ArrayList<Frame> getFrames() {
        return frames;
    }
}
