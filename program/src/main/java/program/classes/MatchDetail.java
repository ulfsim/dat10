package program.classes;

import java.util.ArrayList;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 * Created by Nichlas on 30-11-2015.
 */
public class MatchDetail {
    long matchDuration;
    long matchId;
    String matchVersion;
    String queueType;
    String region;
    Timeline timeline;
    ArrayList<Participant> participants;

    public long getMatchDuration() {
        return matchDuration;
    }


    public long getMatchId() {
        return matchId;
    }


    public String getMatchVersion() {
        return matchVersion;
    }


    public String getQueueType() {
        return queueType;
    }

    public String getRegion() {
        return region;
    }


    public Timeline getTimeline() {
        return timeline;
    }


    public ArrayList<Participant> getParticipants() {
        return participants;
    }


    public ArrayList<Team> getTeams() {
        return teams;
    }


    ArrayList<Team> teams;


    public MatchDetail(String JSONObject){
        try {
            JSONParser jsonParser = new JSONParser();
            JSONObject jsonObject = (JSONObject) jsonParser.parse(JSONObject);

            matchDuration = (long) jsonObject.get("matchDuration");
            matchId = (long) jsonObject.get("matchId");
            matchVersion = (String) jsonObject.get("matchVersion");
            queueType = (String) jsonObject.get("queueType");
            region = (String) jsonObject.get("region");



            JSONObject jsonTimeline = (JSONObject)jsonObject.get("timeline");
            timeline = new Timeline(jsonTimeline);

            participants = new ArrayList<>();
            JSONArray jsonParticipants = (JSONArray) jsonObject.get("participants");
            for (int i = 0; i < jsonParticipants.size(); i++)
            {
                JSONObject participant = (JSONObject) jsonParticipants.get(i);
                participants.add(new Participant(participant));
            }

            teams = new ArrayList<>();
            JSONArray jsonTeams = (JSONArray) jsonObject.get("teams");
            for (int i = 0; i < jsonTeams.size(); i++)
            {
                JSONObject team = (JSONObject) jsonTeams.get(i);
                teams.add(new Team(team));
            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

}
