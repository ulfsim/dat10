package program.classes;

import org.json.simple.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nichlas on 30-11-2015.
 */
public class ParticipantStats {
    long item0;
    long item1;
    long item2;
    long item3;
    long item4;
    long item5;
    long item6;
    boolean winner;
    public ParticipantStats(JSONObject stats)
    {
        item0 = (long)stats.get("item0");
        item1 = (long)stats.get("item1");
        item2 = (long)stats.get("item2");
        item3 = (long)stats.get("item3");
        item4 = (long)stats.get("item4");
        item5 = (long)stats.get("item5");
        item6 = (long)stats.get("item6");
        winner = (boolean)stats.get("winner");
        //System.out.println("Item0:" + item0 + " Item1: " + item1 + " Item2:" + item2 + " Item3:" + item3 + " Item4:"
        //        + item4 + " Item5:" + item5 + " Item6:" + item6 + " Winner:" + winner);
    }
    public List<Long> getItems()
    {
        ArrayList<Long> itemList = new ArrayList<>();
        itemList.add(item0);
        itemList.add(item1);
        itemList.add(item2);
        itemList.add(item3);
        itemList.add(item4);
        itemList.add(item5);
        itemList.add(item6);
        return itemList;
    }
    public boolean isWinner()
    {
        return winner;
    }
}
