package program.classes;

import org.json.simple.JSONObject;

/**
 * Created by Nichlas on 30-11-2015.
 */
public class Event {
    String buildingType;
    String eventType;
    String laneType;
    long itemId;
    long itemAfter;
    long itemBefore;
    long timestamp;
    long participantId;
    long killerId;
    long teamId;
    String towerType;
    String monsterType;
    public Event(JSONObject event)
    {
        try {buildingType = (String)event.get("buildingType");}
        catch (Exception e) {buildingType = null;}
        try{eventType = (String)event.get("eventType");}
        catch(Exception e){eventType = null;}
        try{laneType = (String)event.get("laneType");}
        catch(Exception e){laneType = null;}
        try{itemId = (long)event.get("itemId");}
        catch(Exception e){itemId = -1;}
        try{itemAfter = (long)event.get("itemAfter");}
        catch(Exception e){itemAfter = -1;}
        try{itemBefore = (long)event.get("itemBefore");}
        catch(Exception e){itemBefore = -1;}
        try{participantId = (long)event.get("participantId");}
        catch(Exception e){itemBefore = -1;}
        try{killerId = (long)event.get("killerId");}
        catch(Exception e){killerId = -1;}
        try{teamId = (long)event.get("teamId");}
        catch(Exception e){teamId = -1;}
        try{towerType = (String)event.get("towerType");}
        catch(Exception e){towerType = null;}
        try{monsterType = (String)event.get("monsterType");}
        catch(Exception e){monsterType = null;}

        timestamp = (long)event.get("timestamp");
    }

    public long getItemBefore() {
        return itemBefore;
    }

    public long getItemAfter() {
        return itemAfter;
    }

    public long getItemId() {
        return itemId;
    }

    public String getEventType() {
        return eventType;
    }

    public long getParticipantId() {
        return participantId;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public String getBuildingType() {
        return buildingType;
    }

    public String getTowerType() {
        return towerType;
    }

    public String getMonsterType() {
        return monsterType;
    }

    public long getKillerId() {
        return killerId;
    }

    public long getTeamId() {
        return teamId;
    }
}
