package program;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * Created by Ulf on 22/12/15.
 */
public class ThetaWeights {
    Map<Long,Double> userChampionWeights;
    Map<Long,Double> enemyChampionWeights;
    Map<Long,Double> userItemWeights;
    Map<Long,Map<Long,Double>> EnemyItemWeights;

    double userRankWeight;
    double enemyRankWeight;
    double userBaronWeight;
    double enemyBaronWeight;
    double userHeraldWeight;
    double enemyHeraldWeight;
    double userDragonWeight;
    double enemyDragonWeight;
    double userTurretWeight;
    double enemyTurretWeight;
    double userInhibitorWeight;
    double enemyInhibitorWeight;

    public ThetaWeights()
    {
        userChampionWeights = Utility.getChampionIdMap(true);
        enemyChampionWeights = Utility.getChampionIdMap(true);
        userItemWeights = Utility.getItemIdMap(true);
        EnemyItemWeights = new HashMap<>();

        for (Map.Entry<Long,Double> entry : enemyChampionWeights.entrySet())
        {
            EnemyItemWeights.put(entry.getKey(),Utility.getItemIdMap(true));
        }

        userRankWeight = Utility.GetRandomDouble();
        enemyRankWeight = Utility.GetRandomDouble();
        userBaronWeight = Utility.GetRandomDouble();
        enemyBaronWeight = Utility.GetRandomDouble();
        userHeraldWeight = Utility.GetRandomDouble();
        enemyHeraldWeight = Utility.GetRandomDouble();
        userDragonWeight = Utility.GetRandomDouble();
        enemyDragonWeight = Utility.GetRandomDouble();
        userTurretWeight = Utility.GetRandomDouble();
        enemyTurretWeight = Utility.GetRandomDouble();
        userInhibitorWeight = Utility.GetRandomDouble();
        enemyInhibitorWeight = Utility.GetRandomDouble();
    }
    public ThetaWeights(boolean isForGradient)
    {
        userChampionWeights = Utility.getChampionIdMap(false);
        enemyChampionWeights = Utility.getChampionIdMap(false);
        userItemWeights = Utility.getItemIdMap(false);
        EnemyItemWeights = new HashMap<>();

        for (Map.Entry<Long,Double> entry : enemyChampionWeights.entrySet())
        {
            EnemyItemWeights.put(entry.getKey(),Utility.getItemIdMap(false));
        }

        userRankWeight = 0.0;
        enemyRankWeight = 0.0;
        userBaronWeight = 0.0;
        enemyBaronWeight = 0.0;
        userHeraldWeight = 0.0;
        enemyHeraldWeight = 0.0;
        userDragonWeight = 0.0;
        enemyDragonWeight = 0.0;
        userTurretWeight = 0.0;
        enemyTurretWeight = 0.0;
        userInhibitorWeight = 0.0;
        enemyInhibitorWeight = 0.0;
    }
    public void print()
    {
        //System.out.println("RankWeight:"+ userRankWeight);
        System.out.println("BaronWeight:"+ userBaronWeight);
        /*System.out.println("HeraldWeight:"+ userHeraldWeight);
        System.out.println("DragonWeight:"+ userDragonWeight);
        System.out.println("TurretWeight:"+ userTurretWeight);
        System.out.println("InhibitorWeight:"+ userInhibitorWeight);
        System.out.println("===================================");*/
    }

    public double getEnemyRankWeight() {
        return enemyRankWeight;
    }

    public void setEnemyRankWeight(double enemyRankWeight) {
        this.enemyRankWeight = enemyRankWeight;
    }

    public double getEnemyBaronWeight() {
        return enemyBaronWeight;
    }

    public void setEnemyBaronWeight(double enemyBaronWeight) {
        this.enemyBaronWeight = enemyBaronWeight;
    }

    public double getEnemyHeraldWeight() {
        return enemyHeraldWeight;
    }

    public void setEnemyHeraldWeight(double enemyHeraldWeight) {
        this.enemyHeraldWeight = enemyHeraldWeight;
    }

    public double getEnemyDragonWeight() {
        return enemyDragonWeight;
    }

    public void setEnemyDragonWeight(double enemyDragonWeight) {
        this.enemyDragonWeight = enemyDragonWeight;
    }

    public double getEnemyTurretWeight() {
        return enemyTurretWeight;
    }

    public void setEnemyTurretWeight(double enemyTurretWeight) {
        this.enemyTurretWeight = enemyTurretWeight;
    }

    public double getEnemyInhibitorWeight() {
        return enemyInhibitorWeight;
    }

    public void setEnemyInhibitorWeight(double enemyInhibitorWeight) {
        this.enemyInhibitorWeight = enemyInhibitorWeight;
    }

    public Map<Long, Double> getUserChampionWeights() {
        return userChampionWeights;
    }

    public void setUserChampionWeights(Map<Long, Double> userChampionWeights) {
        this.userChampionWeights = userChampionWeights;
    }

    public Map<Long, Double> getEnemyChampionWeights() {
        return enemyChampionWeights;
    }

    public void setEnemyChampionWeights(Map<Long, Double> enemyChampionWeights) {
        this.enemyChampionWeights = enemyChampionWeights;
    }

    public Map<Long, Double> getUserItemWeights() {
        return userItemWeights;
    }

    public void setUserItemWeights(Map<Long, Double> userItemWeights) {
        this.userItemWeights = userItemWeights;
    }

    public Map<Long, Map<Long,Double>> getEnemyItemWeights() {
        return EnemyItemWeights;
    }

    public void setEnemyItemWeights(Map<Long, Map<Long,Double>> enemyItemWeights) {
        EnemyItemWeights = enemyItemWeights;
    }

    public double getUserRankWeight() {
        return userRankWeight;
    }

    public void setUserRankWeight(double userRankWeight) {
        this.userRankWeight = userRankWeight;
    }

    public double getUserBaronWeight() {
        return userBaronWeight;
    }

    public void setUserBaronWeight(double userBaronWeight) {
        this.userBaronWeight = userBaronWeight;
    }

    public double getUserHeraldWeight() {
        return userHeraldWeight;
    }

    public void setUserHeraldWeight(double userHeraldWeight) {
        this.userHeraldWeight = userHeraldWeight;
    }

    public double getUserDragonWeight() {
        return userDragonWeight;
    }

    public void setUserDragonWeight(double userDragonWeight) {
        this.userDragonWeight = userDragonWeight;
    }

    public double getUserTurretWeight() {
        return userTurretWeight;
    }

    public void setUserTurretWeight(double userTurretWeight) {
        this.userTurretWeight = userTurretWeight;
    }

    public double getUserInhibitorWeight() {
        return userInhibitorWeight;
    }

    public void setUserInhibitorWeight(double userInhibitorWeight) {
        this.userInhibitorWeight = userInhibitorWeight;
    }

    public double getMagnitude (){
        double magnitude = 0;
        for (Map.Entry<Long, Double> entry : userItemWeights.entrySet()){
            magnitude += Math.pow(entry.getValue(),2);
        }

        for (Map.Entry<Long,Map<Long,Double>> entry : EnemyItemWeights.entrySet()){
            for (Map.Entry<Long,Double> enemyItems : entry.getValue().entrySet()){
                magnitude += Math.pow(enemyItems.getValue(),2);
            }
        }

        for (Map.Entry<Long,Double> entry : userChampionWeights.entrySet()){
            magnitude += Math.pow(entry.getValue(),2);
        }

        for (Map.Entry<Long,Double> entry : enemyChampionWeights.entrySet()){
            magnitude += Math.pow(entry.getValue(),2);
        }

        return Math.sqrt(Math.pow(userBaronWeight, 2) +Math.pow(userDragonWeight, 2) + Math.pow(userHeraldWeight, 2) +
                Math.pow(userInhibitorWeight, 2) +Math.pow(userTurretWeight, 2) +
                Math.pow(enemyBaronWeight, 2) +Math.pow(enemyDragonWeight, 2) + Math.pow(enemyHeraldWeight, 2) +
                Math.pow(enemyInhibitorWeight, 2) +Math.pow(enemyTurretWeight, 2) + magnitude);

    }
    public float[] getChampionWeightsAsFloatArray()
    {
        float[] champArray = new float[userChampionWeights.size()];
        int i = 0;
        for(Map.Entry<Long,Double> entry : userChampionWeights.entrySet())
        {
            double champWeight = entry.getValue();
            champArray[i] = (float)champWeight;
        }
        return champArray;
    }
}
