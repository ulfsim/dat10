package program;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by nicky on 1/6/16.
 */
public class Classifier {
    double alpha = 0.05; //Variables might not be optimized yet
    double regularization = 0.2;
    int printNow = 0;

    double trainingSetSize = 0;

    Map<Long,ItemFeatureVector> itemVectors;

    public Classifier() {
        itemVectors = new HashMap<>();
        Map<Long,Double> itemMap = Utility.getItemIdMap(true);

        for (Map.Entry<Long, Double> entry : itemMap.entrySet()){
            Long key = entry.getKey();
            int intKey = (int)(long) key;
            itemVectors.put(key, new ItemFeatureVector(intKey));
        }
    }

    private double sigmoid(double z){
        return 1/(1 + Math.exp(-z));
    }

    public void minimize (Match match){
        for (Map.Entry<Long,ItemFeatureVector> entry : itemVectors.entrySet()){
            updateVector(entry.getValue(), match);
        }
    }

    private void updateVector(ItemFeatureVector vector, Match match){
        ThetaWeights weights = vector.getThetaWeights();
        Map<StaticComponent,List<DynamicComponent>> componentListMap = match.getComponentsMap();
        List<Long> itemList;



        for (int i = 0; i < componentListMap.size(); i++)
            for (Map.Entry<StaticComponent,List<DynamicComponent>> entry : componentListMap.entrySet()) {
                StaticComponent sc = entry.getKey();
                itemList = sc.getItemList();

                for (DynamicComponent dc : entry.getValue()) {
                    printNow++;
                    if (printNow % 10000 == 0) {
                        System.out.println((1.0 / trainingSetSize) * calcCostFunction(sc, dc) + " PrintNow: " + printNow);
                    }

                    if (sc.isUserWinner()) {
                        if (itemList.contains((long) vector.getItemId())) {
                            multiUpdate(weights, dc, itemList, sc, 1.0, 1.0);
                        } else {
                            multiUpdate(weights, dc, itemList, sc, 0.0, 1.0);
                        }
                    } else {
                        if (itemList.contains((long) vector.getItemId())) {
                            multiUpdate(weights, dc, itemList, sc, 1.0, 0.0);
                        } else {
                            multiUpdate(weights, dc, itemList, sc, 0.0, 0.0);
                        }
                    }
                }
            }
    }

    private double classify(DynamicComponent dc, StaticComponent sc, ThetaWeights weights){
        double classification = .0;

        classification += weights.getEnemyBaronWeight() * dc.getEnemyBaronKills();
        classification += weights.getEnemyDragonWeight() * dc.getEnemyDragonsKills();
        classification += weights.getEnemyHeraldWeight() * dc.getEnemyHeraldKills();
        classification += weights.getEnemyInhibitorWeight() * dc.getEnemyInhibitorsDestroyed();
        classification += weights.getEnemyTurretWeight() * dc.getEnemyTurretsDestroyed();

        Map<Long,Map<Long,Double>> enemyItemWeights = weights.getEnemyItemWeights();
        for (Map.Entry<Long,List<Long>> entry : dc.getEnemyItemList().entrySet()){
            Map<Long,Double> currentMap = enemyItemWeights.get(entry.getKey());
            for (long item : entry.getValue()){
                if (item != 0){
                    classification += currentMap.get(item) * 1.0;
                }
            }
        }

        Map<Long,Double> enemyChampionWeights = weights.getEnemyChampionWeights();
        for (long champion : sc.getEnemyChampionIds())
        {
            classification += enemyChampionWeights.get(champion) * 1.0;
        }

        for (long item : dc.getUserItemList()){
            if (item != 0){
                classification += weights.getUserItemWeights().get(item) * 1.0;
            }
        }

        classification += weights.getUserBaronWeight() * dc.getUserBaronKills();
        classification += weights.getUserDragonWeight() * dc.getUserDragonsKills();
        classification += weights.getUserHeraldWeight() * dc.getUserHeraldKills();
        classification += weights.getUserInhibitorWeight() * dc.getUserInhibitorsDestroyed();
        classification += weights.getUserTurretWeight() * dc.getUserTurretsDestroyed();

        classification += weights.getUserChampionWeights().get(sc.getUserChampionId())*1.0;

        return sigmoid(classification);
    }

    private void multiUpdate(ThetaWeights weights, DynamicComponent dc, List<Long> itemList, StaticComponent sc, double featureWeights , double actualClass){
        double currentClass = classify(dc, sc, weights);

        weights.setUserBaronWeight(updateParam(weights.getUserBaronWeight(), currentClass, dc.getUserBaronKills(), actualClass));
        weights.setEnemyBaronWeight(updateParam(weights.getEnemyBaronWeight(), currentClass, dc.getEnemyBaronKills(), actualClass));
        weights.setUserHeraldWeight(updateParam(weights.getUserHeraldWeight(), currentClass, dc.getUserHeraldKills(), actualClass));
        weights.setEnemyHeraldWeight(updateParam(weights.getEnemyHeraldWeight(), currentClass, dc.getEnemyHeraldKills(), actualClass));
        weights.setUserDragonWeight(updateParam(weights.getUserDragonWeight(), currentClass, dc.getUserDragonsKills(), actualClass));
        weights.setEnemyDragonWeight(updateParam(weights.getEnemyDragonWeight(), currentClass, dc.getEnemyDragonsKills(), actualClass));
        weights.setUserTurretWeight(updateParam(weights.getUserTurretWeight(), currentClass, dc.getUserTurretsDestroyed(), actualClass));
        weights.setEnemyTurretWeight(updateParam(weights.getEnemyTurretWeight(), currentClass, dc.getEnemyTurretsDestroyed(), actualClass));
        weights.setUserInhibitorWeight(updateParam(weights.getUserInhibitorWeight(), currentClass, dc.getUserInhibitorsDestroyed(), actualClass));
        weights.setEnemyInhibitorWeight(updateParam(weights.getEnemyInhibitorWeight(), currentClass, dc.getEnemyInhibitorsDestroyed(), actualClass));

        for (long item : itemList){
            if (item != 0){
                weights.getUserItemWeights().put(item, updateParam(weights.userItemWeights.get(item), currentClass, featureWeights, actualClass));
            }
        }

        long userChampId = sc.getUserChampionId();
        weights.userChampionWeights.put(userChampId, updateParam(weights.userChampionWeights.get(userChampId),currentClass,featureWeights,actualClass));

        List<Long> enemyChampions = sc.getEnemyChampionIds();
        for (long champ : enemyChampions){
            weights.enemyChampionWeights.put(champ, updateParam(weights.getEnemyChampionWeights().get(champ),currentClass,featureWeights,actualClass));
        }

        Map<Long,Map<Long,Double>> enemyItemWeights = weights.getEnemyItemWeights();
        for (Map.Entry<Long,List<Long>> entry : dc.getEnemyItemList().entrySet()){
            Map<Long,Double> currentMap = enemyItemWeights.get(entry.getKey());
            for (long item : entry.getValue()){
                if (item != 0){
                    currentMap.put(item, updateParam(currentMap.get(item),currentClass,featureWeights,actualClass));
                }
            }
        }
    }

    private double updateParam(double currentTheta, double currentClass , double featureWeight, double actualClass){
        double newWeight = currentTheta - (alpha-(currentClass-actualClass))*featureWeight - regularization*currentTheta;

        return newWeight;
    }

    private double calcCostFunction(StaticComponent sc, DynamicComponent dc){
        double currentCost= 0.0;
        double actualClass;

        for (Map.Entry<Long, ItemFeatureVector> itemVector : itemVectors.entrySet()){
            ItemFeatureVector currentVector = itemVector.getValue();

            if (sc.isUserWinner() && sc.getItemList().contains((long)currentVector.getItemId())){
                actualClass = 1;
                currentCost += costFunction(actualClass, currentVector, itemVector, sc, dc) -actualClass * -Math.log(classify(dc,sc,currentVector.getThetaWeights()))-(1-actualClass) * Math.log(1-classify(dc,sc,currentVector.getThetaWeights())) + (regularization/2)*itemVector.getValue().getThetaWeights().getMagnitude();
            }
            else{
                actualClass = 0;
                currentCost += costFunction(actualClass, currentVector, itemVector, sc, dc) -actualClass * -Math.log(classify(dc,sc,currentVector.getThetaWeights()))-(1-actualClass) * Math.log(1-classify(dc,sc,currentVector.getThetaWeights())) + (regularization/2)*itemVector.getValue().getThetaWeights().getMagnitude();
            }
        }

        return currentCost;
    }

    private double costFunction(double actualClass, ItemFeatureVector currentVector, Map.Entry<Long, ItemFeatureVector> itemVector, StaticComponent sc, DynamicComponent dc){
        double result = (-actualClass) * Math.log(classify(dc,sc,currentVector.getThetaWeights()))-(1-actualClass) * Math.log(1-classify(dc,sc,currentVector.getThetaWeights())) + (regularization/2)*itemVector.getValue().getThetaWeights().getMagnitude();
        return result;
    }

    public Map<Long, ItemFeatureVector> getItemVectors() {
        return itemVectors;
    }

    public void setTrainingSetSize(double trainingSetSize) {
        this.trainingSetSize = trainingSetSize;
    }
}
