package program;

import program.classes.MatchDetail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Ulf on 23/12/15.
 */
public class Match {
    Map<StaticComponent,List<DynamicComponent>> componentsMap;
    public Match(MatchDetail md)
    {
        componentsMap = new HashMap<>();
        for(int i = 0; i < 10; i++)
        {
            StaticComponent sc = new StaticComponent(md,i);
            List<DynamicComponent> dynamicComponents = Utility.getDynamicComponents(md,i,sc);
            componentsMap.put(sc, dynamicComponents);
        }
    }

    public Map<StaticComponent, List<DynamicComponent>> getComponentsMap() {
        return componentsMap;
    }
}
