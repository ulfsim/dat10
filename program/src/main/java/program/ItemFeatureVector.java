package program;

import java.util.Map;

/**
 * Created by Ulf on 16/12/15.
 */
public class ItemFeatureVector {
    int itemId;

    ThetaWeights thetaWeights;
    private Map<Long,Double> itemUsageMap;
    public ItemFeatureVector(int itemId)
    {
        this.itemId = itemId;
        thetaWeights = new ThetaWeights();
        itemUsageMap = Utility.getChampionIdMap(true);
        Utility.Nullify(itemUsageMap);
    }

    public int getItemId() {
        return itemId;
    }

    public ThetaWeights getThetaWeights() {
        return thetaWeights;
    }

    public Map<Long, Double> getItemUsageMap() {
        return itemUsageMap;
    }

    public void setItemUsageMap(Map<Long, Double> itemUsageMap) {
        this.itemUsageMap = itemUsageMap;
    }
    public double getTotalChampAmount()
    {
        double total = 0.0;
        for(Map.Entry<Long,Double> entry : itemUsageMap.entrySet())
        {
            total += entry.getValue();
        }
        return total;
    }
}
