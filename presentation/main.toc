\beamer@endinputifotherversion {3.36pt}
\beamer@sectionintoc {1}{Motivation}{3}{0}{1}
\beamer@sectionintoc {2}{Technical Background}{5}{0}{2}
\beamer@subsectionintoc {2}{1}{Big Data Frameworks}{6}{0}{2}
\beamer@subsectionintoc {2}{2}{CPU/GPU Comparison}{7}{0}{2}
\beamer@subsectionintoc {2}{3}{Machine Learning}{9}{0}{2}
\beamer@sectionintoc {3}{Contribution}{12}{0}{3}
\beamer@subsectionintoc {3}{1}{Model}{14}{0}{3}
\beamer@subsectionintoc {3}{2}{User API}{16}{0}{3}
\beamer@subsectionintoc {3}{3}{Back End}{18}{0}{3}
\beamer@sectionintoc {4}{Future Work}{23}{0}{4}
\beamer@sectionintoc {5}{Update to Current Model}{25}{0}{5}
\beamer@subsectionintoc {5}{1}{Addition to Standard Library}{28}{0}{5}
\beamer@sectionintoc {6}{New Test Results}{29}{0}{6}
\beamer@subsectionintoc {6}{1}{With New User Function}{30}{0}{6}
\beamer@subsectionintoc {6}{2}{Flink Issues}{32}{0}{6}
\beamer@sectionintoc {7}{Conclusion}{33}{0}{7}
