\begin{thebibliography}{10}

\bibitem{statModel}
M.~K.~E. .
\newblock Noise-tolerant learning from statistical queries.
\newblock pages 392–401, 1999.

\bibitem{HadoopAlternative}
M.~Asay.
\newblock Hadoop demand falls as other big data tech rises.
\newblock
  http://www.infoworld.com/article/2922720/big-data/hadoop-demand-falls-as-other-big-data-tech-rises.html,
  May 2015.

\bibitem{winedata}
P.~Cortez.
\newblock Wine quality data set.
\newblock http://archive.ics.uci.edu/ml/datasets/Wine+Quality, 2009.

\bibitem{criteo}
CRITEO.
\newblock Criteo dataset.
\newblock https://www.kaggle.com/c/criteo-display-ad-challenge/data, June 2014.

\bibitem{aibook}
A.~K.~M. David L.~Poole.
\newblock Artificial intelligence - foundations of computational agents.
\newblock Cambridge University Press, 2010.

\bibitem{mapvsspark}
A.~Dawar.
\newblock Apache spark vs. mapreduce.

\bibitem{MapReduce}
J.~Dean and S.~Ghemawat.
\newblock Mapreduce: Simplified data processing on large clusters.
\newblock 2004.

\bibitem{cpugpuimage}
D.~Education.
\newblock Massively parallel programming with gpus.
\newblock https://people.duke.edu/~ccc14/sta-663/CUDAPython.html, Sept. 2016.

\bibitem{Mars}
B.~H. et. al.
\newblock Mars: A mapreduce framework on graphics processors.
\newblock 2008.

\bibitem{MapReduceMulti}
C.-T.~C. et. al.
\newblock Map-reduce for machine learning on multicore, 2007.

\bibitem{CNN}
T.~L. et. al.
\newblock Implementation of training convolutional neural networks.
\newblock https://arxiv.org/ftp/arxiv/papers/1506/1506.01195.pdf, 2015.

\bibitem{openclarchitecture}
M.~Fahmed.
\newblock Opencl.
\newblock https://mohamedfahmed.wordpress.com/2010/05/20/opencl-2/, Sept. 2016.

\bibitem{Flink}
Flink.
\newblock Apache flink.
\newblock https://flink.apache.org/, Mar. 2016.

\bibitem{OpenCL}
K.~Group.
\newblock Opencl.
\newblock https://www.khronos.org/opencl/, Aug. 2016.

\bibitem{Hadoop}
Hadoop.
\newblock Apache hadoop.
\newblock http://hadoop.apache.org/, Mar. 2016.

\bibitem{marsvshadoop}
S.~W. Heshan~Li.
\newblock Benchmark hadoop and mars: Mapreduce on cluster versus on gpu.
\newblock
  http://www.cs.jhu.edu/~heshanli/liheshan/Projects\_files/pp\_final\_project.pdf,
  2009.

\bibitem{SIMD}
S.~C.~E. Inc.
\newblock Basics of simd programming.
\newblock
  http://ftp.cvut.cz/kernel/people/geoff/cell/ps3-linux-docs/CellProgrammingTutorial/BasicsOfSIMDProgramming.html,
  2008.

\bibitem{ark}
Intel.
\newblock Intel® xeon® processor e5-2670 (20m cache, 2.60 ghz, 8.00 gt/s
  intel® qpi).
\newblock
  http://ark.intel.com/products/64595/Intel\-Xeon\-Processor\-E5-2670\-20M\-Cache\-2\_60\-GHz-8\_00\-GTs\-Intel\-QPI.

\bibitem{deepLearning}
D.~C.~R. Itamar~Arel and T.~P. Karnowski.
\newblock Deep machine learning—a new frontier in artificial intelligence
  research.
\newblock Research Frontier, 2010.

\bibitem{JOCL}
JOCL.
\newblock Java bindings for opencl.
\newblock http://www.jocl.org/, Aug. 2016.

\bibitem{terasortsparkflink}
D.~Kim.
\newblock Terasort for spark and flink with range partitioner, June 2015.

\bibitem{LWJGL}
LWJGL.
\newblock Lightweight java game library 3.
\newblock http://www.lwjgl.org, May 2016.

\bibitem{mikkel}
e.~a. Mikkel A.~Madsen.
\newblock Predicting the outcome of league of legends matches using machine
  learning on big data, 2015.

\bibitem{bigdataproblem}
V.~.~C. News.
\newblock Every day big data statistics \- 2.5 quintillion bytes of data
  created daily.
\newblock
  http://www.vcloudnews.com/every-day-big-data-statistics-2-5-quintillion-bytes-of-data-created-daily/,
  Apr. 2015.

\bibitem{andrewcost}
A.~Ng.
\newblock Logistic regression: Cost function.
\newblock
  https://www.coursera.org/learn/machine\-learning/lecture/1XG8G/cost\-function,
  Oct. 2015.

\bibitem{andrewhypothesis}
A.~Ng.
\newblock Logistic regression: Hypothesis representation.
\newblock
  https://www.coursera.org/learn/machine\-learning/lecture/RJXfB/hypothesis\-representation,
  Oct. 2015.

\bibitem{dat9}
U.~G.~S. Nichlas Bo~Nielsen.
\newblock Optimizing item shopping for league of legends using machine
  learning.
\newblock Technical report, Aalborg University, 2016.

\bibitem{introCuda}
Nvidia.
\newblock An easy introduction to cuda c and c++.
\newblock
  https://devblogs.nvidia.com/parallelforall/easy\-introduction\-cuda\-c\-and\-c/,
  Oct. 2012.

\bibitem{CUDA}
NVIDIA.
\newblock Cuda.
\newblock http://www.nvidia.com/object/cuda\_home\\\_new.html, Aug. 2016.

\bibitem{quadro}
Nvidia.
\newblock Quadro quick specs.
\newblock http://www.nvidia.com/object/quadro-desktop-gpus.html, Sept. 2016.

\bibitem{RMI}
Oracle.
\newblock Trail: Rmi.
\newblock https://docs.oracle.com/javase/tutorial/rmi/, Nov. 2016.

\bibitem{HeteroSpark}
N.~Z. Y.~C. Peilong~Li, Yan~Luo.
\newblock Heterospark: A heterogeneous cpu/gpu spark platform for machine
  learning algorithms.
\newblock 2015.

\bibitem{infosparkvsflink}
I.~Pointer.
\newblock Apache flink: New hadoop contender squares off against spark.
\newblock
  http://www.infoworld.com/article/2919602/hadoop/flink\-hadoops\-new-contender\-for\-mapreduce\-spark.html.

\bibitem{yarn}
M.~Rouse.
\newblock Apache hadoop yarn(yet another resource negotiator).
\newblock
  http://searchdatamanagement.techtarget.com/definition/Apache\-Hadoop\-YARN\-Yet\-Another\-Resource\-Negotiator.

\bibitem{SAS}
SAS.
\newblock Machine learning - what it is and why it matters.
\newblock http://www.sas.com/en\_us/insights/analytics/machine-learning.html,
  2016.

\bibitem{Spark}
Spark.
\newblock Apache hadoop.
\newblock http://spark.apache.org/, Mar. 2016.

\bibitem{sutterlarus}
H.~Sutter and J.~Larus.
\newblock Software and the concurrency revolution.
\newblock Queue, 3(7):54–62,, 2005.

\end{thebibliography}
