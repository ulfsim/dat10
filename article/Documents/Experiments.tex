\section{Experiments}\label{sec:experiments}
In this section, the framework will be tested and results given. In Section \ref{sec:setup}, the experimental setup will be given. In Section \ref{sec:results}, the results of the experiments will be presented and evaluated. Finally in Section \ref{sec:flinkhypo}, an estimate of how the framework would work on a full Flink setup will be given.

\subsection{Experimental Setup}\label{sec:setup}
The framework is evaluated with the two built-in machine learning functions logistic regression and K-means. The impact of the GPU is tested by benchmarking the performance of the framework implementation to a similar CPU implementation. Two datasets have been chosen for testing. The standard library implementation of Logistic regression is tested using the Criteo dataset\cite{criteo}. The standard library implementation of K-Means is tested using the wine data set \cite{winedata}.

The framework is tested locally on two machines. The first machine has one CPU; i7-2600k 3.4 GHz with 8 gb of main memory and one GPU; Nvidia Geforce GTX 770 with 4gb memory. The second machine has one CPU; i5-6600k and one GPU; Nvidia Geforce GTX 1060 with 3gb memory.

Due to time constraints, a full implementation of the framework in Flink has not been made. Instead the framework will be tested to show how scalable and versatile it is by presenting results from different types of hardware.

\subsection{Experimental Results}\label{sec:results}
\begin{figure}[H]
    \centering
    \begin{minipage}{.45\textwidth}
		\begin{tikzpicture}[scale=0.6]
		\begin{axis}[
		    ybar,
		    enlargelimits=0.15,
		    legend style={at={(0.5,1.2)},
		      anchor=north,legend columns=-1},
		    ylabel={Average Performance in seconds},
		    symbolic x coords={i7 2600k, i5 6600k, GTX 770, GTX 1060},
		    xtick=data,
		    nodes near coords,
		    nodes near coords align={vertical},
		    x tick label style={rotate=45, anchor=east, align=right,text width=2cm},
		    ]
		\addplot coordinates {(i7 2600k,171.84)(i5 6600k, 164.23)(GTX 770,81.97)(GTX 1060,62.37)};

		\end{axis}
		\end{tikzpicture}
		\caption{Logistic Regression CPU/GPU, 1.000.000 samples, 100 iterations}

		\label{fig:logistictest1}
    \end{minipage}%
    \begin{minipage}{0.45\textwidth}
		\begin{tikzpicture}[scale=0.6]
		\begin{axis}[
		    ybar,
		    enlargelimits=0.15,
		    legend style={at={(0.5,1.2)},
		      anchor=north,legend columns=-1},
		    ylabel={Average Performance in seconds},
		    symbolic x coords={i7 2600k, i5 6600k, GTX 770, GTX 1060},
		    xtick=data,
		    nodes near coords,
		    nodes near coords align={vertical},
		    x tick label style={rotate=45, anchor=east, align=right,text width=2cm},
		    ]
		\addplot coordinates {(i7 2600k,170.64)(i5 6600k, 164.58)(GTX 770,96.96)(GTX 1060,73.57)};

		\end{axis}
		\end{tikzpicture}
		\caption{Logistic Regression CPU/GPU, 10.000 samples, 10.000 iterations}

		\label{fig:logistictest2}
    \end{minipage}
\end{figure}

The framework has been tested with the two build in functions in the standard library. The results shown on Figure \ref{fig:logistictest1} and Figure \ref{fig:logistictest2}, shows that using a GPU for solving problems such as logistic regression can be beneficial. The speedup is however not nearly as much as the initial tests showed, and this might be due to a few factors. With the introduction of the automated memory management, the framework has an increased overhead when using a GPU implementation. The framework first needs to calculate and allocate memory on the GPU. Another major factor is the need to copy the data to and from the GPU. To test this, another test with the same two setups has been run. Instead of 1 million samples run over 100 iterations, only 10.0000 were run, but over 10.000 iterations. This means that the same amount of data is processed, but due to the increase in iterations, the framework has to do a lot more of copying back and forth from the CPU to the GPU. This has a very noticeable increase on the performance of the GPU of about 18\%. 

\begin{figure}[h]
\centering
\begin{tikzpicture}[scale=0.6]
\begin{axis}[
    ybar,
    enlargelimits=0.15,
    legend style={at={(0.5,1.2)},
      anchor=north,legend columns=-1},
    ylabel={Average Performance in seconds},
    symbolic x coords={i7 2600k, i5 6600k, GTX 770, GTX 1060},
    xtick=data,
    nodes near coords,
    nodes near coords align={vertical},
    x tick label style={rotate=45, anchor=east, align=right,text width=2cm},
    ]
\addplot coordinates {(i7 2600k,7.42)(i5 6600k, 6.56)(GTX 770,11.408)(GTX 1060,9.34)};

\end{axis}
\end{tikzpicture}
\caption{K-Means CPU/GPU, 4968 samples, 400 iterations, 50 centroids}

\label{fig:kmeanstest}
\end{figure}
The framework was also tested on the second standard library implementation. A kernel with K-Means was run with 50 centroids over 400 iterations and shows some interesting results.
Figure \ref{fig:kmeanstest} shows the results of the test with K-means, and in this case, the automated GPU implementation is slower than the CPU implementation. This might be caused by the fact that there are only 4968 samples of data, so the overhead of the data transfer causes the framework to be slower than a CPU implementation. This shows that it is not always right to use the framework, not even for problems that are easily run concurrently.

\subsection{Flink Test Approximation}\label{sec:flinkhypo}
As mentioned in the start of this section, due to time constraints, the framework was not tested on a fully integrated Flink cluster. An approximation on how a possible test on a such a cluster would have been can be made based on how tests on similar frameworks have been done. When Flink is integrated into to the framework, a few new causes of overhead is introduced. The first is the Flink framework itself. The second is the underlying file system used for the cluster. A file system such as Hadoop Distributed File System can be used in this case.

As for overhead, the Flink framework itself should only cause a minor impact on computational time. The underlying file system and communication between nodes on the cluster on the other hand causes some noticeable overhead. The question is how much this affects performance as we introduce more workers to the cluster.

If we look at the results from HeteroSpark, as seen in Figure \ref{fig:heterospark2}, we can see that performance per worker slows down when new workers are introduced. If the two GPU results are used as an example, we go from a relative average performance of $2.096/GPU$ to $2.201/GPU$. If each new GPU is treated as a new worker, the spark loses about $0.83\%$ performance for each new worker introduces. This is even more apparent with the CPU workers, where an increase from 4 to 16 workers introduces a loss of performance of $31.1\%$ or $2.591\%$ for each new CPU worker introduced.
\begin{figure}[H]
\centering
\begin{tikzpicture}[scale=0.7]
\begin{axis}[
    ybar,
    enlargelimits=0.15,
    legend style={at={(0.5,1.2)},
      anchor=north,legend columns=-1},
    ylabel={Normalized Performance},
    symbolic x coords={Spark 32 Cores,Spark 64 Cores,Spark 128 Cores,HeteroSpark 8 cores/2 GPU,HeteroSpark 32 cores/8 GPU},
    xtick=data,
    nodes near coords,
    nodes near coords align={vertical},
    x tick label style={rotate=45, anchor=east, align=right,text width=3.5cm},
    ]
\addplot coordinates {(Spark 32 Cores,1.00) (Spark 64 Cores,2.09) (Spark 128 Cores,4.60)(HeteroSpark 8 cores/2 GPU,4.33)(HeteroSpark 32 cores/8 GPU,18.57)};
\addplot coordinates {(Spark 32 Cores,1.00) (Spark 64 Cores,2.71) (Spark 128 Cores,6.25)(HeteroSpark 8 cores/2 GPU,3.66)(HeteroSpark 32 cores/8 GPU,16.41)};
\addplot coordinates {(Spark 32 Cores,1.00) (Spark 64 Cores,2.42) (Spark 128 Cores,4.88)(HeteroSpark 8 cores/2 GPU,4.59)(HeteroSpark 32 cores/8 GPU,18.00)};
\legend{Logistic Regression,K-Means,Word2Vec}
\end{axis}
\end{tikzpicture}
\caption{Performance of HeteroSpark v.s. Spark\cite{HeteroSpark}}
\label{fig:heterospark2}
\end{figure}

With this in mind, an expected overhead of $2\%$ for each new worker seems reasonable, this gives us these new theoretical results. These are as seen in Figure \ref{fig:logistictest3} and Figure \ref{fig:logistictest4}. This is however not a completely correct representation, as the increase in workers also increases the effect of the copy overhead mentioned before. With the introduction of more GPUs the overhead of copying from each CPU to each GPU increases and performance is lost.
\begin{figure}
\centering
\begin{minipage}{0.45\textwidth}
\begin{tikzpicture}[scale=0.6]
\begin{axis}[
    ybar,
    enlargelimits=0.15,
    legend style={at={(0.5,1.2)},
      anchor=north,legend columns=-1},
    ylabel={Average Performance in seconds},
    symbolic x coords={GTX 770x1, GTX 770x2,GTX 770x4, GTX 770x8, GTX 770x16},
    xtick=data,
    nodes near coords,
    nodes near coords align={vertical},
    x tick label style={rotate=45, anchor=east, align=right,text width=2cm},
    ]
\addplot coordinates {(GTX 770x1,81.97)(GTX 770x2,41.80)(GTX 770x4,21.72205)(GTX 770x8,11.680)(GTX 770x16,6.660)};
\end{axis}
\end{tikzpicture}
\caption{Logistic Regression on Flink CPU/GPU, 1.000.000 samples, 100 iterations}
\label{fig:logistictest3}
\end{minipage}%
\begin{minipage}{0.45\textwidth}
\begin{tikzpicture}[scale=0.6]
\begin{axis}[
    ybar,
    enlargelimits=0.15,
    legend style={at={(0.5,1.2)},
      anchor=north,legend columns=-1},
    ylabel={Average Performance in seconds},
    symbolic x coords={GTX 1060x1, GTX 1060x2, GTX 1060x4, GTX 1060x8, GTX 1060x16},
    xtick=data,
    nodes near coords,
    nodes near coords align={vertical},
    x tick label style={rotate=45, anchor=east, align=right,text width=2cm},
    ]
\addplot coordinates {(GTX 1060x1,73.57)(GTX 1060x2,37.520)(GTX 1060x4,19.496)(GTX 1060x8,10.483)(GTX 1060x16,5.977)};
\end{axis}
\end{tikzpicture}
\caption{Logistic Regression on Flink CPU/GPU, 1.000.000 samples, 100 iterations}
\label{fig:logistictest4}
\end{minipage}
\end{figure}