\subsection{Logistic Regression}\label{app:logistic}
This is a section taken from a previous semester project\cite{dat9}.

Logistic regression classifies by forming a hypothesis, which can then be used to give a probability of a datapoint belonging to a given class. This hypothesis can be seen as a line that divides the data into two parts. Depending on the location of a new datapoint compared to the hypothesis, logistic regression will then determine the probability of that point belonging to one of the classes.

An example of how a hypothesis could look can be seen in Figure \ref{fig:exlogis}. In this figure there are two classes, the $red$ and $blue$ class and the hypothesis as the blue line. Logistic regression would guess that all points above the blue line belongs to the red class, and above, to the blue class.

\begin{figure}[!htb]
  \centering
  \begin{tikzpicture}
    \begin{axis}
      [
      height = 4cm,
      width = 6cm,
      xlabel = $x_1$,
      ylabel = $x_2$,
      axis lines = left
      ]
      \addplot[domain=0:3, blue] {-1*x+3};
      \addplot[mark=triangle*,red] coordinates {(3,3)};
      \addplot[mark=triangle*,red] coordinates {(2,3)};
      \addplot[mark=triangle*,red] coordinates {(3,2)};
      \addplot[mark=triangle*,red] coordinates {(3,1)};
      \addplot[mark=triangle*,red] coordinates {(1,3)};
      \addplot[mark=triangle*,red] coordinates {(3,1)};
      \addplot[mark=triangle*,red] coordinates {(2,2)};
      \addplot[mark=square*, blue] coordinates {(0.1,0.1)};
      \addplot[mark=square*, blue] coordinates {(1,0.1)};
      \addplot[mark=square*, blue] coordinates {(2,0.1)};
      \addplot[mark=square*, blue] coordinates {(0.1,1)};
      \addplot[mark=square*, blue] coordinates {(0.1,2)};
      \addplot[mark=square*, blue] coordinates {(1,1)};
    \end{axis}
  \end{tikzpicture}
  \caption{Logistic regression example with the hypothesis being the line dividing the data}
  \label{fig:exlogis}
\end{figure}

Logistic regression finds a hypothesis $h_{\theta}(x)$, where $x$ is the coordinates of the point that we want to find the probability for and $\theta$ is a parameter for defining the function of the hypothesis line. The hypothesis will then return the probability of the point belonging to one of the classes. For probabilities we would like them to be between $0$ and $1$: 

\begin{equation}
  \label{eq:hypo}
  0 \leq h_{\theta}(x)\leq 1
\end{equation}

When predicting $y$ it is commonly done with a threshold as seen below:

\[ \text{If } h_{\theta}(x) \geq 0.5, \text{ predict }  y = 1\]
\[ \text{If } h_{\theta}(x) < 0.5, \text{ predict }  y = 0  \]

The general hypothesis in logistic regression can be written as:
\[h_{\theta}(x) = g(\theta^Tx) \]
The function $g$ is defined as:
\[g(z) = \frac{1}{1+e^{-z}} \]
This is called the \emph{logistic function}. Figure \ref{fig:logfunc} shows a plot of the basic logistic function. As the parameter $z$ approaches $\infty$, the line $g(x)$ approaches $1$ and $0$ for $-z$ and $-\infty$. This ensures that the rules for the hypothesis seen in equation \ref{eq:hypo} will always be true, no matter how big $x$ is.

\begin{figure}[!htb] %Husk lige at flytte rundt på labels på denne plot
  \centering
  \begin{tikzpicture}
    \begin{axis}[
      height = 4cm,
      width = 6cm,
      xlabel = $z$,
      ylabel = $g(x)$,
      axis lines = middle]
      \addplot[domain=-1:1, blue] {1/(1+exp(-10*x))};
    \end{axis}
  \end{tikzpicture}
  \caption{Plot of the logistic function}
  \label{fig:logfunc}
\end{figure}

The hypothesis can now be used to predict which class a newly inserted data point has the highest probability of belonging to. Looking back at the basic logistic function shown on figure \ref{fig:logfunc}, the hypothesis would predict all variables with a $z$ above $0$ to be $1$, as they have a probability higher than $0.5$ \cite{andrewhypothesis}.

The output of a hypothesis $h_{\theta}(x)$ can be interpreted as the probability that $y = 1$ given $x$, parameterized by $\theta$. This can be written as:
\[h_{\theta}(x) = \text{P}(y=1 | x, \theta) \]
This would only give the probability of $y = 1$, since $y$ can only be either $1$ or $0$. We know how to find the probability of $y = 0$ as both probabilities must sum up to $1$.

No hypothesis will fit all data distributions however, thus making it necessary to determine the optimal $\theta$.

An example of why this is important could be if a point was added to the example in figure \ref{fig:exlogis} resulting in figure \ref{fig:exlogis2}, another hypothesis would fit the data better. In Figure \ref{fig:exlogis3} an example of a new hypothesis that captures the new point is given.

\begin{figure}[!htb]
  \centering
  \begin{tikzpicture}
    \begin{axis}
      [
      height = 4cm,
      width = 6cm,
      xlabel = $x_1$,
      ylabel = $x_2$,
      axis lines = left
      ]
      \addplot[domain=0:3, blue] {-1*x+3};
      \addplot[mark=triangle*,red] coordinates {(3,3)};
      \addplot[mark=triangle*,red] coordinates {(2,3)};
      \addplot[mark=triangle*,red] coordinates {(3,2)};
      \addplot[mark=triangle*,red] coordinates {(3,1)};
      \addplot[mark=triangle*,red] coordinates {(1,3)};
      \addplot[mark=triangle*,red] coordinates {(3,1)};
      \addplot[mark=triangle*,red] coordinates {(2,2)};
      \addplot[mark=triangle*,red] coordinates {(1.25,1.25)};
      \addplot[mark=square*, blue] coordinates {(0.1,0.1)};
      \addplot[mark=square*, blue] coordinates {(1,0.1)};
      \addplot[mark=square*, blue] coordinates {(2,0.1)};
      \addplot[mark=square*, blue] coordinates {(0.1,1)};
      \addplot[mark=square*, blue] coordinates {(0.1,2)};
      \addplot[mark=square*, blue] coordinates {(1,1)};
    \end{axis}
  \end{tikzpicture}
  \caption{Logistic regression example with a new added point}
  \label{fig:exlogis2}
\end{figure}

\begin{figure}[!htb]
  \centering
  \begin{tikzpicture}
    \begin{axis}
      [
      height = 4cm,
      width = 6cm,
      xlabel = $x_1$,
      ylabel = $x_2$,
      axis lines = left
      ]
      \addplot[domain=0:3, blue] {0.3556*x^2-2.067*x+3};
      \addplot[mark=triangle*,red] coordinates {(3,3)};
      \addplot[mark=triangle*,red] coordinates {(2,3)};
      \addplot[mark=triangle*,red] coordinates {(3,2)};
      \addplot[mark=triangle*,red] coordinates {(3,1)};
      \addplot[mark=triangle*,red] coordinates {(1,3)};
      \addplot[mark=triangle*,red] coordinates {(3,1)};
      \addplot[mark=triangle*,red] coordinates {(2,2)};
      \addplot[mark=triangle*,red] coordinates {(1.25,1.25)};
      \addplot[mark=square*, blue] coordinates {(0.1,0.1)};
      \addplot[mark=square*, blue] coordinates {(1,0.1)};
      \addplot[mark=square*, blue] coordinates {(2,0.1)};
      \addplot[mark=square*, blue] coordinates {(0.1,1)};
      \addplot[mark=square*, blue] coordinates {(0.1,2)};
      \addplot[mark=square*, blue] coordinates {(1,1)};
    \end{axis}
  \end{tikzpicture}
  \caption{Logistic regression example with a new hypothesis}
  \label{fig:exlogis3}
\end{figure}

To fit the parameter $\theta$ one would use a cost function together with a learning algorithm that uses it. This learning algorithm could for example be one of the gradient descend algorithms, described in Section \ref{app:gradientdesent}

The cost function in general can be written as $Cost(h_{\theta},y)$. This is read as the cost that the learning algorithm has to pay if it outputs $h_{\theta}(x)$ and the actual value is $y$. Given $y \in \{1,0\}$, the cost function is as seen in Equation \ref{eq:cost}.

\begin{equation}
  \label{eq:cost}
  \text{Cost}(h_\theta(x),y) = \left\{
    \begin{array}{ll}
      - \text{log}(h_\theta(x)) & \text{if } y = 1 \\
      - \text{log}(1 - h_\theta(x)) & \text{if } y = 0
    \end{array}
  \right.
\end{equation}

This cost function have some desirable properties. First of all it keeps the prediction value between 0 and 1, which important as we these are the only values that y can be predicted as. Secondly it gives an infinite cost if $h_{\theta}(x)$ returns 0 and the actual $y$ value was 1, as this would be the same as it saying that outcome is impossible, which should never happen \cite{andrewcost}.

\begin{figure}[!htb]
  \begin{minipage}[!h]{0.45\linewidth}
    \centering
    \begin{tikzpicture}
      \begin{axis}[height = 5cm, 
        width = 5cm,
        ymax = 6,
        xlabel = $h_{\theta}(x)$,
        ylabel = Cost]
        \addplot[domain=0:1, blue]{-log2(x)};
      \end{axis}
    \end{tikzpicture}
    \captionof{figure}{Plot of $- \text{log}(h_\theta(x))$}
    \label{fig:yeq1}
  \end{minipage}\begin{minipage}[!h]{0.45\linewidth}
    \centering
    \begin{tikzpicture}
      \begin{axis}[height = 5cm,
        width = 5cm,
        ymax = 6,
        xlabel = $h_{\theta}(x)$,
        ylabel = Cost]
        \addplot[domain=0:0.955, blue]{-log2(1-x)};
      \end{axis}
    \end{tikzpicture}
    \captionof{figure}{Plot of $- \text{log}(h_\theta(1-x))$}
    \label{fig:yeq0}
  \end{minipage}
\end{figure}