\section{Framework: Implementation}
This section explains the implementation and technical design decisions made during implementation. In Section \ref{sec:implexisting}, existing frameworks needed for the implementation will be explored. In Section \ref{sec:userapi}, an overview of the user API part of the implementation is presented and design decisions explained. In Section \ref{sec:backend}, an overview of the back end of the framework is given, and the most essential functions presented. Finally in Section \ref{sec:stdlib}, a standard library for the framework will be presented.

\subsection{Existing Frameworks} \label{sec:implexisting}
Since Flink only supports Java, Scala and Python, a solution to combine Flink and OpenCL must be found or created. There exists a handful of solutions for using OpenCL in a higher level language such as Java, mainly LWJGL \cite{LWJGL} and jocl\cite{JOCL}. Using one of these libraries, one can combine Flink and OpenCL without the need for implementation of OpenCL in Flink. Since the only function of such a library in this project is binding OpenCL to Java, LWJGL has been chosen as it is the framework with the most extensive documentation.

\subsection{Framework: User API} \label{sec:userapi}
The implementation of the framework is done through using the Java implementation of Flink as well as the LWJGL library. It has to be noted that even though LWJGL is used for the OpenCL bindings to Java, each kernel must still be written, or generated, in native OpenCL.

The implementation is based on Figure \ref{fig:propFigure}, and implements each step either in Flink, LWJGL or OpenCL.

As the framework is used as an extension to a standard Flink program, the user will simply start the framework by importing it into an existing Flink project and specifying which part of the code is needed to run on the GPU. This is step 1 on Figure \ref{fig:propFigure}.
\begin{figure}[h]
\begin{lstlisting}
OpenCLObject object = new OpenCLObject(dataArray,
	instances,weightSize);
object.runLogistic("cl/logistic.txt", "logistic",iterations);
\end{lstlisting}
\caption{Creating an OpenCLObject}
\label{fig:openCLObject}
\end{figure}
As seen in the code at Figure \ref{fig:openCLObject}, to use the framework the user must create an OpenCLObject which takes three arguments. The first is the dataArray which is an array of doubles or floats containing the data. instances is the amount of floats or doubles a the user data consists of. Finally, weightSize is the amount of floats or doubles the users weights contains, given that the user wants to use an algorithm that requires this e.g. logistic regression. When a user defines a new OpenCLObject, the constructor fetches info about possible OpenCL compatible hardware in the system, this is step 2 and 3 on Figure \ref{fig:propFigure}. If the system has a GPU, it will use this as the target for the program, otherwise it will default to use the CPU. After the target hardware has been chosen, the constructor will call the memory allocator, which is one of the most essential parts of the framework. The memory allocator will be explained in detail in Section \ref{sec:backend}.

When the user has defined an OpenCLObject, they can then use the run method on it as seen in Figure \ref{fig:openCLObject}. In this example the standard implementation of logistic regression is run. The run method takes three arguments. The first is a string with the name of the path to the kernel which is to be compiled. The second is a string with the name of the function that is to be called in the kernel on the GPU. Lastly, the third argument is the number of iterations the user wishes to run the kernel on the GPU. 

\subsection{Framework: Back end}\label{sec:backend}
Once the user has called the run method, nothing more needs to be done by the user. The back end of the framework will automatically calculate and allocate the memory needed and launch the kernel on the GPU. As said previously, the memory allocator is one of the most essential part of the framework, as it allows the framework to easily allocate memory on the GPU, regardless of what kind of hardware is used. The code for the memory allocator is as seen in Appendix \ref{app:codeexamples}. LWJGL has built in functionality to check for the amount of memory and compute units the target hardware has. This information is used to compute how much memory there will be left for data, and how many data instances that fit with each compute unit. If there is more data than the target hardware can handle in a single iteration, the memory allocator will calculate how many iterations are needed to go through all the data. This is not to be confused with the user defined iterations, as the user defined iterations is the number of times the user expects the whole dataset to be used on the GPU.

When the run method is called, the framework tries to compile the kernel and launch it. This is done in a few steps. First, even before the kernel is compiled, any dynamic variables, such as the size of a data instance, needs to be pre-processed in the kernel. OpenCL does not allow for dynamically allocated arrays, so a way to work around this is to load the kernel into the framework as a string, and change any dynamic allocated arrays to their fixed size. The kernel is then compiled.

\begin{figure}[htb]
\begin{lstlisting}
dataBuffer = UtilCL.toDoubleBuffer(dataArray);
weightBuffer = UtilCL.toDoubleBuffer(weightsArray);
answerBuffer = UtilCL.toDoubleBuffer(answerArray);

dataMemory = clCreateBuffer(context, CL_MEM_READ_ONLY | 
CL_MEM_COPY_HOST_PTR,dataBuffer, null);
weightMemory = clCreateBuffer(context, CL_MEM_READ_ONLY | 
CL_MEM_COPY_HOST_PTR, weightBuffer, null);
answerMemory = clCreateBuffer(context, CL_MEM_WRITE_ONLY | 
CL_MEM_COPY_HOST_PTR, answerBuffer, null);
clEnqueueWriteBuffer(queue, dataMemory, 1, 0, dataBuffer, null, null);
clEnqueueWriteBuffer(queue, weightMemory, 1, 0, weightBuffer, null, null);
clFinish(queue);
\end{lstlisting}

\caption{Memory allocation of a kernel using logistic regression}
\label{fig:memoryAllocation}
\end{figure}

Allocating the memory of the GPU is the first part of step 8 in Figure \ref{fig:propFigure}. An example of this is as seen in figure \ref{fig:memoryAllocation}. If you need to copy any data, LWJGL requires it to be of the type Buffer. In this case, the data, weight and answer arrays are converted to buffers. The type of memory wanted must then be specified. In this example, the data and weight memory are used only for computations and are not to be changed, so they are copied as read only. The answer memory is where we want the kernel to store the returned values, and such needs only to be write only. Once specified, the buffers are enqueued into the command queue and await execution. Enqueuing the buffers states that we want to copy the data contained in the buffer to the specified place. This is the reason the answerMemory is not enqueued yet, as we want to copy the buffer back to the CPU memory once the kernel has finished. after enqueuing the write buffers the copy is executed by calling the clFinish function on the command queue.


\begin{figure}[h]
\begin{lstlisting}
kernel1DGlobalWorkSize.put(0, kernelAmount);
kernel.setArg(0,dataMemory);
kernel.setArg(1,weightMemory);
kernel.setArg(2,answerMemory);
kernel.setArg(3,instancesPrKernel);
clEnqueueNDRangeKernel(queue, kernel, 1, null, kernel1DGlobalWorkSize,
 null, null, null);
clEnqueueReadBuffer(queue,answerMemory,1,0,answerBuffer,null,null);
clFinish(queue);
\end{lstlisting}
\caption{Settings the arguments and launching kernels}
\label{fig:kernelLaunch}
\end{figure}

Now that the memory has been specified, the arguments of the kernel must be set and a kernel for each compute unit must be launched. This is the last two steps of step 8 in Figure \ref{fig:propFigure}. As seen in Figure \ref{fig:kernelLaunch}, the arguments for the kernel are set, in this case the implementation of logistic regression. Once the arguments are set, the an amount of kernels are launched based on the data from the memory allocator. This is done by using the function clEnqueueNDRangeKernel. To use this we need to specify which queue and kernel are used as well as how many kernels we want to launch. Other arguments can be set for special cases such as wanting the kernel to be finished before a specific event occurs.

After the arguments for the kernel has been set and it has been specified how many kernels are to be launched, the answerMemory is enqueued to copy the data back into the answerArray. Finally all the commands enqueued to the command queue are executed by calling clFinish on the queue.

Step 9 in Figure \ref{fig:propFigure} is done on the target OpenCL hardware. Once the kernel is finished, the GPU is automatically set as ready and the framework can once again return to step 1.

Some steps of Figure \ref{fig:propFigure} were skipped during the implementation. These are step 5, 7 and part of 10. As to use the framework, the user has to give the data as a single dimension array, the need for the serialize function to be inside of the framework has been omitted. This removes the need for a check of the existence of the function as well as the serialize and de-serialize function. It still remains as a core part of the conceptual model, as it is still needed for the framework to function.

\subsection{Standard Library} \label{sec:stdlib}
The framework includes a standard library of pre-written OpenCL kernels. This includes a variety of widely used machine learning techniques, and can be used without the need of writing ones own kernel, these require no modification to be used. The standard library includes standard solutions to model training and clustering with the methods of logistic regression and K-Means currently.