\section{Related Work} \label{sec:related}
In this section related work to the article will be explored, in Section \ref{sec:gpumachinelearning} the general work of using GPUs in machine learning will be discussed. In Section \ref{sec:heteroSpark}, an implementation of a big data GPU framework, HeteroSpark, is explored.

\subsection{GPU and Big Data Machine Learning}\label{sec:gpumachinelearning}
One of the main uses of GPUs at the time of writing this article is in deep machine learning \cite{deepLearning}. Deep learning focuses on extracting features from data using computational models instead of using a human to define them by hand. A way to do this is to use Convolutional Neural Networks(\textit{CNNs})\cite{CNN}, which use several layers to learn features. CNNs are typically used in image recognition where they can learn features from images e.g. if you feed some images of faces to a CNN it will find recognizable features such as eyes, nose and a mouth. The reason GPUs are used for deep learning is that it is very computationally expensive to do this, but the problem of e.g. image recognition is very easy to split up and divide into many similar sub-problems. The architecture of GPUs fit perfectly, because it utilizes the SIMD format as explained in Section \ref{sec:cpugpu}.

In the area of big data GPUs have been used since 2008 where a framework called MARS was developed\cite{Mars}. The focus of MARS is to combine the MapReduce model with GPUs. It does this by generating the required code for the GPU to run both the Map and Reduce steps on the GPU itself. For its time it showed a great speed up compared to cluster setups such as Hadoop \cite{marsvshadoop}, but MARS is less flexible when it comes to the dataset size selection. The gap in performance between Hadoop and Mars becomes smaller as the size of the data increases as shown by prior research \cite{marsvshadoop}.

\subsection{HeteroSpark}\label{sec:heteroSpark}
In recent times a newer implementation of a GPU big data framework has been made, in the form of HeteroSpark\cite{HeteroSpark}. A look at their test results, as seen in Figure \ref{fig:heterospark} reveals that a single computer with 8 CPU cores and 2 GPUs performs on par with a cluster of 16 computers with 8 CPU cores, when working on machine learning problems such as logistic regression, K-Means and Word2Vec. As seen in Figure \ref{fig:heterospark}, GPUs can solve problems much faster than CPUs. 
\begin{figure}[h]
\centering
\begin{tikzpicture}[scale=0.7]
\begin{axis}[
    ybar,
    enlargelimits=0.15,
    legend style={at={(0.5,1.2)},
      anchor=north,legend columns=-1},
    ylabel={Normalized Performance},
    symbolic x coords={Spark 32 Cores,Spark 64 Cores,Spark 128 Cores,HeteroSpark 8 cores/2 GPU,HeteroSpark 32 cores/8 GPU},
    xtick=data,
    nodes near coords,
    nodes near coords align={vertical},
    x tick label style={rotate=45, anchor=east, align=right,text width=3.5cm},
    ]
\addplot coordinates {(Spark 32 Cores,1.00) (Spark 64 Cores,2.09) (Spark 128 Cores,4.60)(HeteroSpark 8 cores/2 GPU,4.33)(HeteroSpark 32 cores/8 GPU,18.57)};
\addplot coordinates {(Spark 32 Cores,1.00) (Spark 64 Cores,2.71) (Spark 128 Cores,6.25)(HeteroSpark 8 cores/2 GPU,3.66)(HeteroSpark 32 cores/8 GPU,16.41)};
\addplot coordinates {(Spark 32 Cores,1.00) (Spark 64 Cores,2.42) (Spark 128 Cores,4.88)(HeteroSpark 8 cores/2 GPU,4.59)(HeteroSpark 32 cores/8 GPU,18.00)};
\legend{Logistic Regression,K-Means,Word2Vec}
\end{axis}
\end{tikzpicture}
\caption{Performance of HeteroSpark v.s. Spark\cite{HeteroSpark}}
\label{fig:heterospark}
\end{figure}


As seen in Figure \ref{fig:SparkCPUGPUcom}, HeteroSparks worker consists of two parts, the CPU worker and the GPU worker.
These two workers are connected through a Java Remote Method Invocation(\textit{RMI})\cite{RMI} layer that serializes and de-serializes the data. The CPU is treated as a RMI client, while the GPU is treated as a RMI server. This is done such that the GPU can use existing libraries through its dynamically linked library (\*.so), and is linked through the Java Native Interface(\textit{JNI}). This ensures easy access to existing libraries at the cost of a more complex model, and more overhead with the client-server communication.

\begin{figure}[H]
\centering
\includegraphics[scale=0.4]{./Images/HeteroSpark.png}
\caption{HeteroSpark CPU-GPU Communication\cite{HeteroSpark}}
\label{fig:SparkCPUGPUcom}
\end{figure}


Based on this RMI client-server model, HeteroSpark proposes a model on how to use GPUs in conjunction with Spark. The proposed model is as seen in Figure \ref{fig:heteroglue}.

\begin{figure}[h]
\centering
\includegraphics[scale=0.25]{./Images/sparkGlueLogic.png}
\caption{Glue logic of HeteroSpark \cite{HeteroSpark}}
\label{fig:heteroglue}
\end{figure}

When calling a function that needs to use the GPU on a given Spark worker, the worker then checks if it actually has a GPU and it is currently not in use. It then checks if the function the users tries to invoke exists in the RMI register, a simple check to make sure the GPU can use the function. If this function exists in the RMI registry, the GPU is set as busy and data is serialized through the RMI layer. Once the data is on the GPU, the RMI server component deserializes it and runs the GPU implementation of the function the user wants to execute. The results are then serialized and sent back to the Spark worker, as if the problem had been solved on a regular Spark worker