\section{Technical Background}
In this section, existing big data solutions and GPU based scalable machine learning will be explained. In Section \ref{sec:bigdata}, the theory needed from Big Data frameworks will be explained. In Section \ref{sec:cpugpu} a comparison of the CPU and GPU will be made. In Section \ref{sec:extendedgradient}, how to scale algorithms and what class of machine learning algorithms that fit in a GPU context will be explained. In Section \ref{sec:existingframeworks}, existing GPU frameworks are explored. Lastly in Section \ref{sec:initialtests}, initial tests comparing CPU and GPU performance will be presented.


\subsection{Big Data Frameworks} \label{sec:bigdata}
To understand the fundamentals of Big Data frameworks, the MapReduce programming model must be explained.
\subsubsection{MapReduce}
For big datasets, a proposed model from Google is MapReduce\cite{MapReduce}. MapReduce is a programming model which is a combination of three steps, the map step, the shuffle step and the reduce step. This model is very useful when working with multiple workers, such as multiple CPUs or even just multiple cores. Each worker has a chunk of data, and performs computations only on this chunk. The user needs to provide a map and a reduce function. Key-value pairs are used to know what the map function will map to.

\begin{Figure}
  \centering
  \scalebox{0.7}{
    \begin{tikzpicture}[->,>=stealth',bend angle=45,auto]
      % Input data
      \path node at (-6,0) [draw,shape=rectangle, style=rounded corners, minimum width=1cm, minimum height=4cm, label={[rotate=90,yshift=-0.35cm,xshift=-2cm]Input Data}] (ID) {};

      % Output data
      \path node at (6,0) [draw,shape=rectangle, style=rounded corners, minimum width=1cm, minimum height=4cm, label={[rotate=90,yshift=-0.35cm,xshift=-2cm]Output Data}] (OD) {};

      % Mappers
      \path node at (-2.5,1.5) [draw,shape=ellipse, style=rounded corners, minimum width=3cm, minimum height=1cm, label={[yshift=-0.8cm]Map}] (M1) {};
      \path node at (-2.5,0) [draw,shape=ellipse, style=rounded corners, minimum width=3cm, minimum height=1cm, label={[yshift=-0.8cm]Map}] (M2) {};
      \path node at (-2.5,-1.5) [draw,shape=ellipse, style=rounded corners, minimum width=3cm, minimum height=1cm, label={[yshift=-0.8cm]Map}] (M3) {};

      % Reducers
      \path node at (2.5,1) [draw,shape=ellipse, style=rounded corners, minimum width=3cm, minimum height=1cm, label={[yshift=-0.75cm]Reduce}] (R1) {};
      \path node at (2.5,-1) [draw,shape=ellipse, style=rounded corners, minimum width=3cm, minimum height=1cm, label={[yshift=-0.75cm]Reduce}] (R2) {};
      
      % Text nodes
      \path node at (-4.75,-2.5) [] (T1) {Split};
      \path node at (-2.5,-2.5) [] (T2) {Map};
      \path node at (0,-2.5) [] (T4) {Shuffle};
      \path node at (2.5,-2.5) [] (T3) {Reduce};
      \path node at (4.75,-2.5) [] (T5) {Merge};

      %Edges
      \path ([yshift=1.465cm]ID.east) edge (M1)
            (ID) edge (M2)
            ([yshift=-1.465cm]ID.east) edge (M3)
            (M1) edge (R1)
            (M1) edge (R2)
            (M2) edge (R1)
            (M2) edge (R2)
            (M3) edge (R1)
            (M3) edge (R2)
            (R1) edge ([yshift=1cm]OD.west)
            (R2) edge ([yshift=-1cm]OD.west);
%       ([xshift=1cm]E2.south) edge ([xshift=1cm]E1.north)
%       (CM) edge (W1)
%       (CM) edge (W2)
%       (W1) edge (CM)
%       (W2) edge (CM)
%       (SC) edge ([yshift=-0.25cm]CM.west)
%       ([yshift=-0.25cm]CM.west) edge (SC)
%       (SC.south east) edge [bend right] (E1.west)
%       (E2.west) edge [bend right] (SC.north east)
%       (SC.north east) edge [bend left] (E2.west)
%       (E1.west) edge [bend left] (SC.south east);

    \end{tikzpicture}
  }
  \captionof{figure}{The MapReduce programming model \cite{mikkel}}\label{fig:mapreduce}
\end{Figure}

Map reduce will do the three steps as follows:
\begin{itemize}[leftmargin=*]
\item Map Step: The data is split up into even chunks and the map function is applied to each chunk of data, mapping the data to a set of key-value pairs.
\item Shuffle Step: The data is shuffled between the workers such that only data mapped to the same key is on one given worker.
\item Reduce Step: The reduce function is applied on the mapped data and an aggregated result is achieved.
\end{itemize}

This model allows for great scalability, as two simple functions will allow any amount of data to be split up and then combined again.

An simple example of how this would work can be explained using a non-standard deck of cards. Let us assume we have a deck of cards where there are no aces or face cards, such that we are left with only numbered cards, but it still has all four suits(spades, clubs, diamonds and hearts). The deck may contain multiple numbered cards of the same suit, e.g. three times the seven of clubs is allowed. Any number of cards in the deck is allowed, as long as they follow all the rules. The goal is to figure out the total value of the numbered cards in each suit.

\begin{itemize}[leftmargin=*]
	\item Key-value pair: The key is the suit of a given card and the value is the number on said card.
\end{itemize}

Let us assume we have two workers, each having some of the non-standard deck of cards divided between them. With this setup the use of the MapReduce model would go through the following steps:

\begin{itemize}[leftmargin=*]
	\item Map Step: Each worker sorts the deck of cards into piles, each pile consisting only of cards with the same key. In this example, four piles, each one with a different suit.
	\item Shuffle Step: The data is now shuffled around, such that the first worker receives the piles with the hearts and diamonds, and the second worker receives the piles with spades and clubs.
	\item Reduce Step: The data is reduced to the aggregated results we wanted to achieve, which is the total value on each of the piles with a given key. In this case it is the total value on each card with a given suit.
\end{itemize}

\subsubsection{Apache Spark}\label{sec:spark}
Apache Spark is a new big data framework made for big data analytics. The framework requires a cluster manager, e.g. Hadoop Yarn \cite{yarn}. Spark also requires a distributed file system such as Hadoop Distributed File System(\textit{HDFS}). Spark has become popular because of its speed up compared to Hadoop,  as Spark is up to 100 times faster than Hadoop MapReduce in processing data in memory, and up to 10 times faster on the disk. This is the case for algorithms such as logistic regression \cite{Spark}.

The reason Spark is faster is due to its batch processing framework. It uses small parts of the data for each iterations instead of the whole data at once. This is called \textit{micro batching}. This however comes at at cost of latency, as Spark needs to schedule a new set of tasks after each iteration on the data. Data used in Spark is stored in Sparks own form of dataset, called Resilient Distributed Dataset(\textit{RDD}). RDDs are resilient due to the way they are created. Each time an operation is done on a RDD a new RDD is created, and this improves safety of the system, as Spark can easily convert back to a prior version of an RDD if there is an error in the task or hardware failure \cite{mapvsspark}.

RDDs come with a few weaknesses. As a new RDD has to be created each time an operation on an RDD is performed, some functions will not perform well on Spark. An example of this is sorting. As a new RDD has to be created before a new task can be made, the dataset has to be read entirely before it can be partitioning can be done. As seen in Figure \ref{fig:terasortsparkresult}, this makes it so the execution of the terasort is very linear.

\begin{figure}[H]
  \centering 
\includegraphics[scale = 0.55]{Images/TerasortSparkResult.png}
  \caption{Overview of the terasorting process with Spark\cite{dat9} \cite{terasortsparkflink}}
  \label{fig:terasortsparkresult}
\end{figure}

\subsection{Apache Flink}\label{sec:flink}
Apache Flink is another new big data framework from Apache, and is lesser known and used than Apache Spark. Flinks main application is in big data analytics and machine learning. Flink introduces a new model by using pure streaming in addition to working in batches. Flink only schedules tasks once, and this causes Flink to not have the latency problems that Spark has. Flink has some additional functionality in that it allows for each iteration of an iterative process to be run on a single machine instead of the entire cluster if this functionality is wished for in an application\cite{infosparkvsflink}.

Comparing Spark and Flink we can take a look at the sorting example from Section \ref{sec:spark}. Since flink does not utilize RDDs, it does not have the same problem with the limit of scheduling a new task on the data before a task is complete. This means that flink can partition the data at the same time as it is read in and makes it so the task runs more concurrently than linear as seen in Figure \ref{fig:terasortflinkresult}.

\begin{figure}[H]
  \centering
  \includegraphics[scale = 0.55]{Images/TerasortFlinkResult.png}
  \caption{Overview of the TeraSorting process with Flink\cite{dat9} \cite{terasortsparkflink}}
  \label{fig:terasortflinkresult}
\end{figure}

Based on Flink being a big data framework that already has the focus on machine learning, and it supports both streaming and batch programming, it has been chosen as the big data framework for this project.

\subsection{GPU/CPU Comparison} \label{sec:cpugpu}
To understand why one would use a GPU instead of a CPU for machine learning problems, we have to take a look at the physical architecture of the CPU and GPU. Figure 2 is an overview of how much space is utilized by the different hardware components in a CPU and GPU. In a CPU, a lot of space is used on the cache, and an equal amount is used on control and ALUs (\textit{Arithmetic Computational Units}). This means that a CPU has a lot of hardware to control and compute special tasks, as well as a big cache to utilize. GPUs on the other hand uses almost all of their physical space on ALUs, and very little on control and cache. This means that a GPU does not have the ability to perform many hardware specific tasks and use the cache to quickly switch out data. A GPU however, does have the ability to make simple computations on many ALUs in parallel. A simple comparison of the physical usage in hardware is as seen in Figure \ref{fig:CPUGPUarch}.

When programming a CPU, you can explicitly manage and schedule threads and usually run a few heavyweight threads to handle a given process. Usually each of these threads are individually programmed.
When programming a GPU you use single instruction multiple data\cite{SIMD}, \textit{SIMD} for short, which performs the same operation on multiple points of data in parallel. This allows many threads to be programmed for the same task on different data points. Threads are more light weight and are programmed in batches rather than individually. A set of SIMD instructions and what data it is to be used on is called a \textit{kernel}\cite{introCuda}.

\begin{Figure}
\centering
\includegraphics[scale=0.4]{./Images/cpugpuarch.jpg}
\captionof{figure}{CPU \& GPU architecture simplified\cite{cpugpuimage}}
\label{fig:CPUGPUarch}
\end{Figure}


\subsubsection{Scalable Algorithms}\label{sec:extendedgradient}
The GPU architecture is not suited for all kinds of machine learning problems, but it is great at a specific class of problems. Sutter and Larus \cite{sutterlarus} points out that systems with multiple cores benefit from applications that run concurrently. This means that there should be little to no communication between the cores. Chang-Tao Chu et. al.\cite{MapReduceMulti} has looked at how this is best achieved and come across Kearns' Statistical Query Model\cite{statModel}.

Many algorithms that calculate sufficient statistics gradients fit this model, because their calculations can be batched up into sub calculations. This means that we can easily distribute the calculations over multiple cores and do partial computations where the results in the end are aggregated. Chan-Tao Chu et. al. calls this for of algorithms the \textit{"summation form"
}
One way to show how the "summation form" can be applied in practice is to make a simple modification to the well-known gradient descent algorithm, as seen in Appendix \ref{app:gradientdesent}.

\begin{equation}{}
w:= w-\alpha\nabla Q(w) = w-\alpha\sum_{j=1}^{c}\sum_{i=1}^{n_{j}}\nabla Q_{i}(w)
\label{eq:gpugradient}
\end{equation}

This extension splits the sum of entries into smaller sums. Using partial sums rather than a sum over the whole dataset, the data is split up into $c$ different sub datasets, and the results is the sum over all $c$ sub datasets. This algorithm can now scale with the number of cores on a given piece of hardware, e.g. a GPU with 1000 cores will do the computation on 1000 smaller sub datasets rather than on one big dataset.

This can be applied to machine learning models such as logistic regression, as shown by Chan-Tao Chu et al\cite{MapReduceMulti}. The definition of logistic regression can be seen in Appendix \ref{app:logistic}. To fit the parameters of logistic regression, it must be combined with a model such as gradient descent. If using gradient descent, logistic regression would fit the summation form and be able to be run concurrently. This would be done by calculating the update of the parameters concurrently and summing them afterwards.

Clustering algorithms can also benefit be scaled up even though it is not obvious. An example of a clustering algorithm could be K-means (Appendix \ref{app:kmeans}). K-means require a lot of communication each iteration, as centroids are continuously calculated based on the new members of its cluster. This requires an update of what centroid each data point is a part of each iteration. As this requires communication between each core, it is not obvious the it fits the summation form. However, it is clear that the computation of e.g. euclidean distance can be sampled into sub computations and done concurrently, and when done the results aggregated such that one can compute the position of the centroids\cite{statModel}.

\subsubsection{Existing GPU Frameworks}\label{sec:existingframeworks}
As for GPU programming frameworks, two major ones exist, Nvidia's CUDA\cite{CUDA} and Khronos Group's OpenCL\cite{OpenCL}.
They both work in a similar fashion, and uses a high level API such as Java or C\# to write an application using the framework, and a low level language such as CUDA c or OpenCL can be used to write the program for the GPU, called a \textit{kernel}. To use these frameworks, the user must handle all the interaction between the CPU and the GPU. This includes compilation of the kernel, allocation of memory on the GPU and copy of data to and from the GPU from the CPU. 
% On the application side this includes several steps. First the user must serialize the data that needs to be used on the GPU. This serialization must transform the data into a simple format that the GPU can handle, typically a single or multidimensional array. Then the user must allocate the memory needed for the data that needs to be sent to the GPU, as well as the memory needed to store any answers of computations done on the GPU if needed. Lastly, the user needs to specify which kernel is to be run and with what data and parameters.

Both frameworks work when used solely for GPU computation, but OpenCL has the upside that it also runs on CPUs. This might be relevant if one is to use a mix of CPU and GPU workers, since the same openCL implementation can be used for both CPU and GPU. Due to the similarities between CUDA and OpenCL, only OpenCL will be used as an example for this section.

If used for the GPU, the user needs to implement a few steps. This includes \textit{serializing the data}, which is the process of transforming the data into an easy to work with format for the GPU. This is usually done by transforming ones data into a simple data structure such as a single or multidimensional array. Furthermore, the user has to specify and allocate memory for the data that needs to be copied to the GPU, as well as allocate and specify where the answers of the computation is to be put. Lastly, which user written GPU program, the \textit{kernel}, that is to be run on the GPU must be specified, and arguments set.

\begin{Figure}
\centering
\includegraphics[scale=0.5]{./Images/openclhostprocess}
\captionof{figure}{Host of process in OpenCL\cite{openclarchitecture}}
\label{fig:openclhostprocess}
\end{Figure}

To get a better understanding of how OpenCL handles a kernel, we need to look at how OpenCL handles a kernel on an architectural basis. As seen in Figure \ref{fig:openclhostprocess}, the host CPU communicates and sends out work to the different compute devices. A compute device is in this case either may consist of a CPU core or a collection of GPU cores.

As seen in Figure \ref{fig:openclarchitecture}, each compute unit is a part of a block of compute unites which share some local memory. This needs to be taken into account when programming the kernel, as each worker might or might not have the ability to share local variables and data, depending on if they are in the same block or not. The only way for multiple workers to access the same information is either through the global memory. It is good practice to make sure that only compute units in the same block share information.


\begin{Figure}
\centering
\includegraphics[scale=0.38]{./Images/openclArchitecture.jpeg}
\captionof{figure}{Architecture of OpenCL\cite{openclarchitecture}}
\label{fig:openclarchitecture}
\end{Figure}

As of now CUDA only supports Nvidia graphics cards, while OpenCL supports both Nvidia and AMD graphics cards, as well as CPUs. Based on this, OpenCL will be used for this project.

\subsection{Initial Test}\label{sec:initialtests}
A simple way of checking if the GPU is better at handling parallel problems than the CPU is to conduct a basic test. A test was set up to run a logistic regression model, using the extended gradient descent from Equation \ref{eq:gpugradient}, on the criteo \cite{criteo} dataset with 100.000 samples over 100 iterations. The test was conducted on an i7-2600k CPU, only using 1-4 cores, and on a Nvidia GTX 770 running 8 kernels. The average of the test results can be seen in Figure \ref{fig:initialtest}. It is clear that the GPU performs the task much faster, even when the CPU runs all of its 4 hardware threads, the GPU is almost 6 times faster.


\begin{figure}[h]
\centering
\begin{tikzpicture}[scale=0.6]
\begin{axis}[
    ybar,
    enlargelimits=0.15,
    legend style={at={(0.5,1.2)},
      anchor=north,legend columns=-1},
    ylabel={Average Performance in seconds},
    symbolic x coords={i7 1 thread, i7 2 threads, i7 4 threads, GTX 770},
    xtick=data,
    nodes near coords,
    nodes near coords align={vertical},
    x tick label style={rotate=45, anchor=east, align=right,text width=2cm},
    ]
\addplot coordinates {(i7 1 thread,184.84) (i7 2 threads, 96.25) (i7 4 threads, 49.64) (GTX 770,8.46)};

\end{axis}
\end{tikzpicture}
\caption{Logistic Regression CPU/GPU, 100.000 samples, 100 iterations}
\label{fig:initialtest}
\end{figure}
