\section{Framework: Architectural Overview}\label{sec:concept}
The idea is to create a general framework to speed up machine learning algorithms by using the GPU. Such a model needs to be simple for the user to apply, yet flexible enough to allow custom solutions. An architectural model of such as framework can be see in Figure \ref{fig:propFigure}.


\tikzset{
decision/.style = {diamond, draw, fill=blue!20, 
  text width=6em, text badly centered, node distance=3cm, inner sep=0pt,aspect=2},
block/.style = {rectangle, draw, fill=green!20, 
  text width=4em, text centered, rounded corners, minimum height=4em},
flatblock/.style = {rectangle, draw, fill=blue!20, 
  text width=6em, text centered, rounded corners, minimum height=4em},  
line/.style = {draw, -latex'},
cloud/.style = {draw, ellipse,fill=red!20, node distance=3cm,
  minimum height=2em},
subroutine/.style = {draw,rectangle split, rectangle split horizontal,
  rectangle split parts=3,minimum height=1cm,
  rectangle split part fill={red!50, green!50, blue!20, yellow!50}},
connector/.style = {draw,circle,node distance=3cm,fill=yellow!20},
data/.style = {draw, trapezium,node distance=3cm,fill=olive!20}
}

\begin{figure}[H]
\centering
\begin{tikzpicture}[node distance = 3cm, auto,scale=0.6, every node/.style={scale=0.6}]
%Draw nodes
\node [label={[xshift=-0.6cm]$1$},block] (call) {Call myFunc on worker};
\node [label={[xshift=-0.9cm,yshift=-0.45cm]$2$},decision, above right = 0.5cm and 1cm of call] (gpustatus) {GPU live and ready?};
\node [label={$3$},flatblock, right = 0.7cm of gpustatus] (runcpu) {Run CPU implementation};
\node [label={[xshift=-0.9cm,yshift=-0.45cm]$4$},decision,below = 0.25cm of gpustatus] (checkimpl) {KernelImpl exist?};
\node [label={[xshift=-0.9cm,yshift=-0.45cm]$5$},decision,below= 0.4cm of runcpu] (checkserial) {myFuncS exist?};
\node [label={$6$},flatblock,right = 0.7cm of runcpu] (setbusy) {Set GPU busy};
\node [label={[xshift=-1cm]$7$},flatblock, below = 0.5cm of setbusy] (serialize) {Serialize data};
\node [label={[xshift=-1cm]$8$},flatblock, below = 0.5cm of serialize] (gpumemory) {Allocate Memory, Send KernelImpl, Send Data};
\node [cloud, below = 0.2 of gpumemory] (bubble1) {myFuncS};
\node [label={$9$},flatblock, left = 0.7cm of gpumemory] (gpucomp) {GPU Compute};
\node [cloud, below = 0.45 of gpucomp] (bubble2) {KernelImpl};
\node [label={$10$},flatblock, left = 1.1cm of gpucomp] (deserialize) {De-serialize to CPU, Set GPU ready};
\node [cloud, below = 0.45 of deserialize] (bubble3) {myFuncS};

%Draw arrows
\path [line] (call) |- (gpustatus);
\path [line] (gpustatus) -- node {no} (runcpu);
\path [line] (gpustatus) -- node {yes} (checkimpl);
\path [line] (checkimpl) -- node {no} (runcpu);
\path [line] (checkimpl) -- node {yes} (checkserial);
\path [line] (checkserial) -- node {no} (runcpu);
\path [line] (checkserial) -- node {yes} (setbusy);
\path [line] (setbusy) -- (serialize);
\path [line, dashed] (bubble1.east) -- ++ (0.5,0) -- ++ (0.15,0) |- (serialize.east);
%\path [line, dashed] (bubble1.west) -- (serialize.east);
\path [line] (serialize) -- (gpumemory);
\path [line] (gpumemory) -- (gpucomp);
\path [line,dashed] (bubble2) -- (gpucomp);
\path [line] (gpucomp) -- (deserialize);
\path [line,dashed] (bubble3) -- (deserialize);
\path [line] (deserialize) -| (call);


\end{tikzpicture}
\caption{GPU Framework}
\label{fig:propFigure}
\end{figure}
\noindent To understand the model we need to define two functions first.
\noindent
\begin{itemize}[leftmargin=*]
\item Serialize Function: The serialize function(\textit{myFuncS}) is a user defined function that takes the user data as input and outputs a single dimensional array that is needed for it to be used on the GPU. The user should define the transformation from the user data to the single dimensional array. The size of this array is then one of the determining factors for the allocation of the memory on the GPU.
\item Kernel Implementation: The kernel implementation(\textit{KernelImpl}) is a user defined kernel. This is the program for the GPU. This kernel handles all the computation on the GPU, and it has the serialized data as input and will output data in a standard or user defined format.
\end{itemize}

The framework start in step one by being called by the user in step 1. The user needs to import and declare that he or she intends to use the framework. When the user has declared the intent to use the framework, the framework goes to step 2 and checks whether or not there is a GPU, and if there is, if it is busy. This either results in the CPU or GPU implementation being used. If KernelImpl and myFuncS exist(step 4 and 5), there is a GPU present(step 2), and the GPU is ready(step 2), the framework will continue with the GPU implementation. If not, the framework will default to the CPU implementation(step 3). The next step is to set the GPU as busy(step 6), such that further calls to the same GPU will not result in the same GPU being used for multiple kernels. Once the GPU has been set as busy, the data will be serialized according the the user defined function myFuncS(step 7). When the data has been serialized, the memory on the GPU will be allocated(step 8). This is done based on the data given by the users myFuncS. The framework also needs some information about the underlying hardware to allocate the memory, e.g. how much memory there is on the target GPU. Once the memory has been allocated, the user defined KernelImpl and serialized data will be sent to the GPU(step 8). The KernelImpl will then run on the GPU and execute the user defined kernel(step 9). Once the kernel is finished, the kernel will return the data to the CPU and the data will be de-serialized based on the users myFuncS(step 10). Finally, the GPU is set as ready again (step 10).

On the frontend, the framework will act as an easy to use API for the user. The API enabled the user to easily implement a GPU implementation if the user thinks their problem would benefit form the use of a GPU. On the backend the framework will automate some functions for the user. Memory management is a big part of implementing a GPU into a system, and with this process being automated gives the user the ability to implement a GPU solution without the knowledge of how to handle or instantiate memory on a GPU. The framework also omits all the necessary information gathering that needs to be done to be able to use a GPU on a given system.

% \begin{enumerate}
% \item The user calls a function on a given worker
% \item The framework checks if there is a GPU present on the given worker. If a GPU is present, it checks if it is currently running or available for work.
% \item If no GPU is present, or if a GPU is present but busy with another job, the CPU functionality is called instead.
% \item If the GPU is present and ready, the framework checks if the user has implemented the KernelImpl function. The KernelImpl function is the user specified implementation function that runs on the GPU. If the KernelImpl function is not present, the CPU implementation is called instead. 
% \item If KernelImpl is present, the framework checks if the user has implemented the myFuncS function. The myFuncS function is the user specified serialization function used to serialize and de-serialize selected data from the CPU memory to the GPU memory. If the myFuncS function is not present, the CPU implementation is called instead.
% \item The GPU is set as busy and can no longer get new work.
% \item The data is serialized to a format readable by the GPU, this is done by using the myFuncS which the user has defined.
% \item The user defined KernelImpl is sent to the GPU and memory is allocated on the GPU based on the serialized data from $7$. The data is sent to the GPU.
% \item The GPU computes output on the serialized data based on the user defined KernelImpl.
% \item The data is de-serialized to the original format by using the myFuncS. The GPU is then set to ready and can receive new work.
% \end{enumerate} 

\section{Proposed Framework}\label{sec:proposed}
This section explores how a combination of Flink and OpenCL would look like, and explain the goals behind such a combination.

The main goal of the framework is to make the GPU easily available to the user, and such should be easy to use in a MapReduce context such as Flink. The user of the GPU in a MapReduce context could be beneficial in several steps of the process. In this project, the focus will be to enable the speed up of machine learning, and such will not focus on e.g. speed up of map and reduce functions by using the GPU.

When using a cluster setup such as the one seen in Figure \ref{fig:architecture}, the user should easily be able to implement the same implementation of the GPU across all the different hardware there might be. The user will define the two functions myFuncS and KernelImpl on the master node and this will be sent out to each worker. The serialize function will then be run on the data on each individual worker and used as a basis for the \textit{memory allocator} on that worker. The job of the memory allocator is to gather all the information needed to allocate memory on the GPU on that specific worker. This includes gathering some information about the hardware;

\begin{itemize}
	\item The amount of main memory on the GPU on the given worker.
	\item The amount of compute units on the given worker.
	\item The size of a data instance of the users data.
	\item How many instances of data the user has given.
\end{itemize}

This is then used to calculate many data instances each compute unit on the given worker can handle and if needed, how many iterations the framework needs to make to handle all the data on the given worker.

\begin{figure}[H]
\centering
\begin{tikzpicture}[thick,scale=0.6, every node/.style={transform shape}]

% root of the the initial tree, level 1
\node[root] (root) {Master};
\begin{scope}[every node/.style={level 2}]
\node[below left=0.5cm and 0.2cm of root] (c1) {$Worker_1$};
\node[below right =0.5cm and 0.2cm of root] (c2) {$Worker_n$};
\end{scope}
% The second level, relatively positioned nodes
\begin{scope}[every node/.style={level 3}]
\node [below of = c1] (c11) {$CPU_1$};
\node [left = 1.6cm of c11] (user1) {User};
\node [below of = c11] (c12) {$GPU_1$};
\node [left = 1.6cm of c12] (user2) {User};

\node [below of = c2] (c21) {$CPU_n$};
\node [below of = c21] (c22) {$GPU_n$};
\end{scope}


% lines from each level 1 node to every one of its "children"

\def\myshift#1{\raisebox{0.5ex}}
\draw [->,thick,postaction={decorate,decoration={text along path,text align=center,text={|\sffamily\myshift|Program}}}] (root) to [bend right =45] (c1);
\draw [->,thick,postaction={decorate,decoration={text along path,text align=center,text={|\sffamily\myshift|Result}}}] (c1) to [bend right =45] (root);
\draw [->,thick,postaction={decorate,decoration={text along path,text align=center,text={|\sffamily\myshift|Program}}}] (root) to [bend right =45] (c2);
\draw [->,thick,postaction={decorate,decoration={text along path,text align=center,text={|\sffamily\myshift|Result}}}] (c2) to [bend right =45] (root);
\draw [->] (c1) to [bend right=45] (c11);
\draw [->] (c11) to [bend right=45] (c1);
\def\myshift#1{\raisebox{1ex}}
\draw [->,thick,postaction={decorate,decoration={text along path,text align=center,text={|\sffamily\myshift|Serialize}}}] (user1) to (c11);
\draw [->,thick,postaction={decorate,decoration={text along path,text align=center,text={|\sffamily\myshift|Kernel}}}] (user2) to (c12);
\draw[->] (c11) to [bend right=45] (c12);
\draw[->] (c12) to [bend right=45] (c11);
\draw[dashed] (c11) -- (c21);
\draw[dashed] (c12) -- (c22);
\draw[->] (c2) to [bend right=45] (c21);
\draw[->] (c21) to [bend right=45] (c2);
\draw[->] (c21) to [bend right=45] (c22);
\draw[->] (c22) to [bend right=45] (c21);
\end{tikzpicture}
\caption{Architecture overview}
\label{fig:architecture}
\end{figure}



\begin{itemize}[leftmargin=*]
	\item Master: The master sends the program used on each of the workers and is responsible for gathering all the results send back from each worker.
	\item User: The user contributes with two functions, the serialize function and the kernel function.
	\item Worker: The worker has a chunk of the data which needs to be worked on, a CPU and sometimes a GPU. It is responsible for getting the intermediate results, and sending it back to the master if needed.
	\item CPU: The CPU on a worker executes the program sent by the master. It checks if there is a GPU present and if, then generates the memory management based on the data and serialize function given by the user.
	\item GPU: The GPU runs the kernel function given by the user and returns the results to the CPU.
\end{itemize}




